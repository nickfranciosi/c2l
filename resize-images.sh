#!/bin/bash

# Sizes the images should be after conversion
thumbnailsSize="198x167"
secondarySize="400x400"
herosSize="500x670"

# Path to the images of this project
imageDir="$(cd $(dirname "$0") && pwd)/public/images/products"
thumbnailsDir="$imageDir/thumbnails"
thumbnailsOrigDir="$imageDir/thumbnails-orig"
secondaryDir="$imageDir/secondary"
secondaryOrigDir="$imageDir/secondary-orig"
herosDir="$imageDir/heros"
herosOrigDir="$imageDir/heros-orig"

# Print informative text (e.g. in blue)
pinfo() {
	printf "\e[0;34m$@\e[0m\n"
}

# Print error text (e.g. in red)
perror() {
	printf "\e[0;31m$@\e[0m\n"
}

print_help() {
	echo "usage: $(basename $0) image-file [hero | secondary | thumb]"
}

# Resizes all images in a folder
resize_image() {
	if [ "$1" == 'hero' ]; then
		newType='heros'
	elif [ "$1" == 'secondary' ]; then
		newType='secondary'
	elif [ "$1" == 'thumb' ]; then
		newType='thumbnails'
	else
		perror "Skipping unknown option: $1"
		return
	fi

	baseName=$(basename $imageFile)
	newSize=$(eval "echo \$${newType}Size")
	newPath=$(eval "echo \$${newType}Dir")/$baseName
	origPath=$(eval "echo \$${newType}OrigDir")/$baseName
	tempFile=$(mktemp /tmp/img-resize.XXXXX)

	# Add original image to the backup directory in the project
	cp -i "$imageFile" "$origPath"

	# Converto the image to the given size
	pinfo "Making $newType for $baseName"
	convert $imageFile \
	  -resize $newSize \
	  -background White \
	  -gravity center \
	  -extent $newSize \
	  $tempFile

	# Copy the temporary file to the new location, asking before overwriting
	cp -i "$tempFile" "$newPath"
	rm "$tempFile"
}

# Check if `convert` from ImageMagick is installed
if ! [ $(type -p convert 2> /dev/null) ]; then
	perror 'Install ImageMagick to manipulate images (e.g. `brew install imagemagick`)'
	exit 1
fi

# We need at least the path to the image and the size it needs to be
if [ "$#" -lt "2" ]; then
	print_help
	exit 0
fi

imageFile=$1
shift

# The image file must exist
if ! [ -e $imageFile ]; then
	perror "image file not found: $imageFile"
	exit 1
fi

# Run for each size argument given
for arg in $@; do
	resize_image $arg
done
