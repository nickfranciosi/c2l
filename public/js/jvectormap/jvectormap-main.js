$(function() {

  //fills in hidden form value that is then submited to show results
  function showResults(value){
    $('#address').val(value);
    $('#instructions').slideUp();
    $('#loc-list').slideUp().delay(200).slideDown();
    $('#user-location').submit();
  }

  //handles svg map set up and click event for when user selects
  $('#caMap').vectorMap({
    map: 'ca_lcc',
    backgroundColor: '#fff',
    zoomOnScroll: false,
    zoomAnimate: false,
    onRegionClick: function(event, data){
      showResults(data);
    },
    regionStyle: {
      initial: {
        fill: '#2877b1',
        stroke: '#000000',
        "stroke-width": 1
      },
      hover: {
        fill: '#ebebeb'
      }
    }
  });

  $('#usMap').vectorMap({
    map: 'us_lcc',
    backgroundColor: '#fff',
    zoomOnScroll: false,
    zoomAnimate: false,
    onRegionClick: function(event, data){
      showResults(data);
    },
    regionStyle: {
      initial: {
        fill: '#2877b1',
        stroke: '#000000',
        "stroke-width": 1
      },
      hover: {
        fill: '#ebebeb'
      }
    }
  });

  //OLD US LIB map: handles svg map set up and click event for when user selects
  // $('#usMap').usmap({
  //   stateStyles: {fill: '#2877b1'},
  //   stateHoverStyles: {fill: '#eee'},
  //   labelBackingStyles: {fill: '#eee'},
  //   labelBackingHoverStyles: {fill: '#eee'},
  //   labelTextStyles: {fill:'#2877b1'},
  //   click: function(event, data) {
  //        showResults(data.name);
  //     }
  // });

  //handles when user choes state from dropdown
  $('#state_picked').on('change',function(){
    var item= $(this);
    showResults(item.val());
  });

  //handles if user hits enter on keyboard
  $('#manualEntry').on('submit', function(e){
    e.preventDefault();
    $('#textSearch button').click();
  });

  function objectValues(ob) {
    return Object.keys(ob).map(function(id) {
      return ob[id];
    });
  }

  function renderResults(results) {
    var locationBlock = objectValues(results).map(function(result) {
      return "\n      <li>\n      <div class=\"list-details\">\n      <div class=\"list-content\">\n      <div class=\"loc-name\">" + result.locname + "</div>\n      <div class=\"loc-addr\">" + result.address + "</div>\n      <div class=\"loc-addr2\">" + result.address2 + "</div>\n      <div class=\"loc-addr3\">" + result.city + ", " + result.view_state + " " + result.postal + "</div>\n      <div class=\"loc-phone\"><i class=\"fa fa-phone\"></i>" + result.phone + "</div>\n      <div class=\"loc-email\"><i class=\"fa fa-envelope-o\"></i><a href=\"mailto:" + result.email + "\">" + result.email + "</a></div>\n      <div class=\"loc-web\"><i class=\"fa fa-globe\"></i><a href=\"" + result.web + "\" target=\"_blank\">" + result.web + "</a></div>\n      <div class=\"loc-directions\"><i class=\"fa fa-map-marker\"></i><a href=\"http://maps.google.com/maps?daddr=" + result.address + " " + result.address2 + " " + result.city + ", " + result.view_state + " " + result.postal + "\" target=\"_blank\">Directions</a></div>\n      </div>\n      </div>\n      </li>\n      ";
    });

    $('#list').html(locationBlock.join(''));


    $('#map-container').show();


    $('#loc-list').slideDown();

  }

  //handles when user clicks the search button
  $('#textSearch button').on('click', function(){
    var userSearch = $('#addressEntry').val();
    // showResults(userSearch);

    $('#instructions').slideUp();
    $('#loc-list').slideUp();

    $.get('/getRepjson', {search: $('#textSearch input').val()}, renderResults)
  });
});
