$('select').change(function(){
  //when a select item changes in the oneline form
  //get its name attribute
  var selectedItemName = $(this).attr('name');

  //create a string that matches the name of the function below
  var fnString = 'on' + selectedItemName + 'change'; 

  //find the function on the current window object
  var fn = window[fnString];

  //check whether the function string is avaiable as an object in the window
  (typeof fn=== "function") ? fn() : console.log('Online function string isnt available on this window');
})

function fillsystem()
{
//document.onelinegen.SubCat.disabled=true;
addOption(document.onelinegen.system, "Lite", "LitePak", "");
addOption(document.onelinegen.system, "Basic", "Basic: TimeClock/Photo", "");
addOption(document.onelinegen.system, "Self", "Networked: Self-Config", "");
addOption(document.onelinegen.system, "WebServer", "Networked: WebServer", "");
addOption(document.onelinegen.system, "MC6000", "MC6000", "");
addOption(document.onelinegen.system, "Dialog", "DIALOG", "");
	document.onelinegen.model.disabled=true;
	document.onelinegen.riserstyle.disabled=true;
	document.onelinegen.floors.disabled=true;
	document.onelinegen.risers.disabled=true;
	document.onelinegen.panels.disabled=true;
	document.onelinegen.total.value=" ";
}

function onsystemchange()
{
	
	document.onelinegen.total.value=" ";
// ON selection of system this function will work
if (document.onelinegen.system.options.selectedIndex=='0')
{
	document.onelinegen.model.disabled=true;
	document.onelinegen.riserstyle.disabled=true;
	document.onelinegen.floors.disabled=true;
	document.onelinegen.risers.disabled=true;
	document.onelinegen.panels.disabled=true;
	removeAllOptions(document.onelinegen.model);
	removeAllOptions(document.onelinegen.riserstyle);
	removeAllOptions(document.onelinegen.risers);
	removeAllOptions(document.onelinegen.floors);
	removeAllOptions(document.onelinegen.panels);
	document.onelinegen.total.value=" ";
}
else if (document.onelinegen.system.value=='Lite')
{

	document.getElementById("Preview").src="images/oneline/Lite.jpg";
	
	document.onelinegen.model.disabled=false;
	document.onelinegen.riserstyle.disabled=true;
	document.onelinegen.floors.disabled=true;
	document.onelinegen.risers.disabled=true;
	document.onelinegen.panels.disabled=true;
	removeAllOptions(document.onelinegen.model);
	removeAllOptions(document.onelinegen.riserstyle);
	removeAllOptions(document.onelinegen.risers);
	removeAllOptions(document.onelinegen.floors);
	removeAllOptions(document.onelinegen.panels);
	addOption(document.onelinegen.model,"none", "--Please select model--","");
	addOption(document.onelinegen.model,"LitePak-1P08", "8x 1-pole Relay","");
	addOption(document.onelinegen.model,"LitePak-1P16", "16x 1-pole Relay","");
	addOption(document.onelinegen.model,"LitePak-2P04", "4x 2-pole Relays","");
	addOption(document.onelinegen.model,"LitePak-2P08", "8x 2-pole Relays","");
	document.onelinegen.total.value=" ";
}
else 
{	
	removeAllOptions(document.onelinegen.model);
	removeAllOptions(document.onelinegen.risers);
	removeAllOptions(document.onelinegen.floors);
	removeAllOptions(document.onelinegen.panels);
	document.onelinegen.model.disabled=true;
	document.onelinegen.riserstyle.disabled=false;
	document.onelinegen.floors.disabled=true;
	document.onelinegen.risers.disabled=true;
	document.onelinegen.panels.disabled=true;
	removeAllOptions(document.onelinegen.riserstyle);
	addOption(document.onelinegen.riserstyle,"rs", "--Select Riser Style--","");
	addOption(document.onelinegen.riserstyle,"CommTower", "Commercial Tower","");
	addOption(document.onelinegen.riserstyle,"LRComm", "Low-Rise Commercial","");
	addOption(document.onelinegen.riserstyle,"Edu", "Educational","");
	
	if (document.onelinegen.system.value=='Basic')
	{

		addOption(document.onelinegen.model,"wtp", "WRS-2224/WTP-4408","");

	}
	else if (document.onelinegen.system.value=='Self')
	{
		
		addOption(document.onelinegen.model,"wnx", "WNX-2624","");
	
	}
	else if (document.onelinegen.system.value=='WebServer')
	{
	
		addOption(document.onelinegen.model,"wnp", "WNP-2150","");
	
	}
	else if (document.onelinegen.system.value=='DIALOG')
	{
	
		addOption(document.onelinegen.model,"w3", "WLC-3150","");
	
	}
}
}
////////////////// 
function onmodelchange()
{
  //do nothing
}
function onriserstylechange()
{
	document.onelinegen.total.value=" ";
	removeAllOptions(document.onelinegen.risers);
	removeAllOptions(document.onelinegen.floors);
	removeAllOptions(document.onelinegen.panels);
	document.onelinegen.model.disabled=true;
	document.onelinegen.riserstyle.disabled=false;
	document.onelinegen.floors.disabled=false;
	document.onelinegen.risers.disabled=false;
	document.onelinegen.panels.disabled=true;
	addOption(document.onelinegen.floors,"fl", "--Number of Floors--","");
	addOption(document.onelinegen.risers,"fl", "--Number of Risers--","");
	if(document.onelinegen.riserstyle.selectedIndex=='0')
	{
		removeAllOptions(document.onelinegen.risers);
		removeAllOptions(document.onelinegen.floors);
		removeAllOptions(document.onelinegen.panels);
		document.onelinegen.floors.disabled=true;
		document.onelinegen.risers.disabled=true;
		document.onelinegen.panels.disabled=true;
		document.onelinegen.total.value=" ";		
	}

	if(document.onelinegen.riserstyle.value=='LRComm')
	{
		addOption(document.onelinegen.floors,"FL1", "1","");
		addOption(document.onelinegen.floors,"FL2", "2","");
		addOption(document.onelinegen.floors,"FL3", "3","");
		addOption(document.onelinegen.floors,"FL4", "4","");
		addOption(document.onelinegen.floors,"FL5", "5","");	
		addOption(document.onelinegen.risers,"R1", "1","");
		addOption(document.onelinegen.risers,"R2", "2","");
		addOption(document.onelinegen.risers,"R3", "3","");
		addOption(document.onelinegen.risers,"R4", "4","");
	}
	else if(document.onelinegen.riserstyle.value=='CommTower')
	{
		addOption(document.onelinegen.floors,"FL1", "1","");
		addOption(document.onelinegen.floors,"FL2", "2","");
		addOption(document.onelinegen.floors,"FL3", "3","");
		addOption(document.onelinegen.floors,"FL4", "4","");
		addOption(document.onelinegen.floors,"FL5", "5","");
		addOption(document.onelinegen.floors,"FL6", "6","");
		addOption(document.onelinegen.floors,"FL7", "7","");
		addOption(document.onelinegen.floors,"FL8", "8","");
		addOption(document.onelinegen.floors,"FL9", "9","");
		addOption(document.onelinegen.floors,"FL10", "10","");
		addOption(document.onelinegen.floors,"FL11", "11","");
		addOption(document.onelinegen.floors,"FL12", "12","");
		
		addOption(document.onelinegen.risers,"R1", "1","");
		addOption(document.onelinegen.risers,"R2", "2","");
	
	}
	else if(document.onelinegen.riserstyle.value=='Edu')
	{
		addOption(document.onelinegen.floors,"FL1", "1","");
		addOption(document.onelinegen.floors,"FL2", "2","");
		addOption(document.onelinegen.floors,"FL3", "3","");
		addOption(document.onelinegen.floors,"FL4", "4","");	
		removeAllOptions(document.onelinegen.risers);
		addOption(document.onelinegen.risers,"", "N/A","");
		document.onelinegen.risers.disabled=true;
	}
	
}
function onfloorschange()
{	
		document.onelinegen.total.value=" ";
		if(document.onelinegen.floors.selectedIndex=='0')
		{
			
			document.onelinegen.panels.disabled=true;
			removeAllOptions(document.onelinegen.panels);		
		}
		else //if(document.onelinegen.risers.selectedIndex!='0')
		{
			
			document.onelinegen.panels.disabled=false;
			if(document.onelinegen.riserstyle.value=="CommTower")
			{
				if(document.onelinegen.risers.value=="R1")
				{
					removeAllOptions(document.onelinegen.panels);
					document.onelinegen.panels.disabled=false;
					addOption(document.onelinegen.panels,"n", "--Number of Panels per Floor--","");
					addOption(document.onelinegen.panels,"PPF1", "1","");
					addOption(document.onelinegen.panels,"PPF2", "2","");
				}
				else if(document.onelinegen.risers.value=="R2")
				{
					removeAllOptions(document.onelinegen.panels);
					document.onelinegen.panels.disabled=false;
					addOption(document.onelinegen.panels,"n", "--Number of Panels per Floor--","");
					addOption(document.onelinegen.panels,"PPF2", "2","");
					addOption(document.onelinegen.panels,"PPF4", "4","");
				}
			}
			else if(document.onelinegen.riserstyle.value=="LRComm")
			{
				if(document.onelinegen.risers.value=="R1")
				{
					removeAllOptions(document.onelinegen.panels);
					document.onelinegen.panels.disabled=false;
					addOption(document.onelinegen.panels,"n", "--Number of Panels per Floor--","");
					addOption(document.onelinegen.panels,"PPF1", "1","");
					addOption(document.onelinegen.panels,"PPF2", "2","");
				}
				else if(document.onelinegen.risers.value=="R2")
				{
					removeAllOptions(document.onelinegen.panels);
					document.onelinegen.panels.disabled=false;
					addOption(document.onelinegen.panels,"n", "--Number of Panels per Floor--","");
					addOption(document.onelinegen.panels,"PPF2", "2","");
					addOption(document.onelinegen.panels,"PPF4", "4","");
				}
				else if(document.onelinegen.risers.value=="R3")
				{
					removeAllOptions(document.onelinegen.panels);
					document.onelinegen.panels.disabled=false;
					addOption(document.onelinegen.panels,"n", "--Number of Panels per Floor--","");
					addOption(document.onelinegen.panels,"PPF3", "3","");
					addOption(document.onelinegen.panels,"PPF6", "6","");
				}
				else if(document.onelinegen.risers.value=="R4")
				{
					removeAllOptions(document.onelinegen.panels);
					document.onelinegen.panels.disabled=false;
					addOption(document.onelinegen.panels,"n", "--Number of Panels per Floor--","");
					addOption(document.onelinegen.panels,"PPF4", "4","");
					addOption(document.onelinegen.panels,"PPF8", "8","");
				}
			}	
			
		
			else if(document.onelinegen.riserstyle.value=="Edu")
			{
				removeAllOptions(document.onelinegen.panels);
				document.onelinegen.panels.disabled=false
				addOption(document.onelinegen.panels,"n", "--Number of Panels per Floor--","");;
				addOption(document.onelinegen.panels,"PPF1", "1","");
				addOption(document.onelinegen.panels,"PPF2", "2","");
				addOption(document.onelinegen.panels,"PPF3", "3","");
				addOption(document.onelinegen.panels,"PPF4", "4","");
				addOption(document.onelinegen.panels,"PPF5", "5","");
				addOption(document.onelinegen.panels,"PPF6", "6","");
				addOption(document.onelinegen.panels,"PPF7", "7","");
			
			}
		}
} 
function onriserschange()
{
		document.onelinegen.total.value=" ";
		if(document.onelinegen.risers.selectedIndex=='0')
		{
			document.onelinegen.panels.disabled=true;
			removeAllOptions(document.onelinegen.panels);		
		}
		else if(document.onelinegen.floors.selectedIndex!='0')
		{
			
			document.onelinegen.panels.disabled=false;
			if(document.onelinegen.riserstyle.value=="CommTower")
			{
				if(document.onelinegen.risers.value=="R1")
				{
					removeAllOptions(document.onelinegen.panels);
					document.onelinegen.panels.disabled=false;
					addOption(document.onelinegen.panels,"n", "--Number of Panels per Floor--","");
					addOption(document.onelinegen.panels,"PPF1", "1","");
					addOption(document.onelinegen.panels,"PPF2", "2","");
				}
				else if(document.onelinegen.risers.value=="R2")
				{
					removeAllOptions(document.onelinegen.panels);
					document.onelinegen.panels.disabled=false;
					addOption(document.onelinegen.panels,"n", "--Number of Panels per Floor--","");
					addOption(document.onelinegen.panels,"PPF2", "2","");
					addOption(document.onelinegen.panels,"PPF4", "4","");
				}
			}
			else if(document.onelinegen.riserstyle.value=="LRComm")
			{
				if(document.onelinegen.risers.value=="R1")
				{
					removeAllOptions(document.onelinegen.panels);
					document.onelinegen.panels.disabled=false;
					addOption(document.onelinegen.panels,"n", "--Number of Panels per Floor--","");
					addOption(document.onelinegen.panels,"PPF1", "1","");
					addOption(document.onelinegen.panels,"PPF2", "2","");
				}
				else if(document.onelinegen.risers.value=="R2")
				{
					removeAllOptions(document.onelinegen.panels);
					document.onelinegen.panels.disabled=false;
					addOption(document.onelinegen.panels,"n", "--Number of Panels per Floor--","");
					addOption(document.onelinegen.panels,"PPF2", "2","");
					addOption(document.onelinegen.panels,"PPF4", "4","");
				}
				else if(document.onelinegen.risers.value=="R3")
				{
					removeAllOptions(document.onelinegen.panels);
					document.onelinegen.panels.disabled=false;
					addOption(document.onelinegen.panels,"n", "--Number of Panels per Floor--","");
					addOption(document.onelinegen.panels,"PPF3", "3","");
					addOption(document.onelinegen.panels,"PPF6", "6","");
				}
				else if(document.onelinegen.risers.value=="R4")
				{
					removeAllOptions(document.onelinegen.panels);
					document.onelinegen.panels.disabled=false;
					addOption(document.onelinegen.panels,"n", "--Number of Panels per Floor--","");
					addOption(document.onelinegen.panels,"PPF4", "4","");
					addOption(document.onelinegen.panels,"PPF8", "8","");
				}
			}	
					
		}
		

}
function onpanelschange()
{
	
	
		document.onelinegen.total.value=" ";
	if(document.onelinegen.panels.options.selectedIndex!="0") 
	{
	if(document.onelinegen.riserstyle.value=="Edu")
	 document.onelinegen.total.value=(document.onelinegen.floors.options.selectedIndex)*(document.onelinegen.panels.options.selectedIndex);
	 else
		document.onelinegen.total.value=(document.onelinegen.risers.options.selectedIndex*document.onelinegen.floors.options.selectedIndex)*(document.onelinegen.panels.options.selectedIndex)*document.onelinegen.risers.options.selectedIndex;
	//alert(document.onelinegen.panels.options.selectedIndex);
	
	if(document.onelinegen.riserstyle.value=="Edu")
	document.getElementById("Preview").src="images/oneline/"+document.onelinegen.riserstyle.value+"_"+document.onelinegen.floors.value+".jpg";	
	else
	document.getElementById("Preview").src="images/oneline/"+document.onelinegen.riserstyle.value+"_"+document.onelinegen.risers.value+"_"+document.onelinegen.panels.value+".jpg";
	}	
}
function removeAllOptions(selectbox)
{
	var i;
	for(i=selectbox.options.length-1;i>=0;i--)
	{
		//selectbox.options.remove(i);
		selectbox.remove(i);
	}
}


function addOption(selectbox, value, text )
{
	var optn = document.createElement("OPTION");
	optn.text = text;
	optn.value = value;

	selectbox.options.add(optn);
}

function popit()
{	

	if(document.onelinegen.system.value=='Lite')
	{
		if(document.onelinegen.model.options.selectedIndex=='0')
			alert("Please select a model for LitePak.");	
		else
		window.location="images/oneline/"+document.onelinegen.system.value+"/"+document.onelinegen.model.value+".dwg";
		//alert();
	
	}
	else
	{
	 if(document.onelinegen.total.value==" ")
	 {
	   		if(document.onelinegen.riserstyle.options.selectedIndex=='0')
		 		alert("Please select a riser style.");
		 	else if(document.onelinegen.floors.options.selectedIndex=='0')
		 		alert("Please select number of floors.");
		 	else if(document.onelinegen.risers.options.selectedIndex=='0')
		 		alert("Please select number of risers.");
		 	else if(document.onelinegen.panels.options.selectedIndex=='0')
		 		alert("Please select number of panels per floor.");
	 }
	 else
	 {
	 	if(document.onelinegen.riserstyle.value=="Edu")
			window.location="images/oneline/"+document.onelinegen.system.value+"/"+document.onelinegen.riserstyle.value+"/"+document.onelinegen.system.value+"_"+document.onelinegen.riserstyle.value+"_"+document.onelinegen.floors.value+"_"+document.onelinegen.panels.value+".dwg";	
	 	else 		 
			window.location="images/oneline/"+document.onelinegen.system.value+"/"+document.onelinegen.riserstyle.value+"/"+document.onelinegen.system.value+"_"+document.onelinegen.riserstyle.value+"_"+document.onelinegen.floors.value+"_"+document.onelinegen.risers.value+"_"+document.onelinegen.panels.value+".dwg";
	 }
	}
		
}
	
function enb(){

if (document.onelinegen.system.options.selectedIndex=='1'){
document.onelinegen.SubCat.disabled=true;
//document.onelinegen.Generate.disabled=true;
}
else
{
document.onelinegen.SubCat.disabled=false;
//document.onelinegen.Generate.disabled=false;
}
}
