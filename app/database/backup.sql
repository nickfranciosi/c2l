# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.12-0ubuntu1.1)
# Database: douglas
# Generation Time: 2016-10-24 17:54:18 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table application_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `application_type`;

CREATE TABLE `application_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tagline` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hero` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pdf_grid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `application_type` WRITE;
/*!40000 ALTER TABLE `application_type` DISABLE KEYS */;

INSERT INTO `application_type` (`id`, `name`, `tagline`, `description`, `image`, `created_at`, `updated_at`, `hero`, `thumbnail`, `url`, `pdf_grid`)
VALUES
	(1,'Stadiums & Arenas','The Trusted Lighting Control Solution Provider for Stadiums and Arenas.','<p>For more than 20 years, Douglas Lighting Controls has been called upon to provide lighting control systems for Stadiums and Arenas whether the need is for a new build or facility upgrade. These large scale projects are a specialty of ours. Knowing how to support the sophisticated lighting control requirements of a sports complex takes years to understand. The systems required to operate the lighting throughout the facility require an unparalleled level of reliability. Douglas is recognized for its system reliability and ability to develop the best solution for these challenging projects.</p>',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00','rogers_centre.jpg','rogers_centre.jpg','stadiums_arenas','Stadiums-Arenas.pdf'),
	(3,'Office Buildings','Scalable Systems for Office Buildings','<p>Multi-floor commercial buildings require multiple relay panels and a variety of switching and control options including time schedules, daylight sensors, and occupancy sensors. Once connected, these devices create a Dialog lighting control system from Douglas Lighting Controls that is capable of controlling an entire building without the need for an external control device. Additional capabilities include integrating with a Building Management Systems.</p>\n<p><b>Dialog</b> systems are factory built, configured, programmed, and tested to meet the requirements of each specific project. Once on site, the labelled panels and devices can be delivered to the correct locations and connected through our simple non-polarized 18/2 wire data bus.</p>',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00','office-building.jpg','office-building.jpg','office-building.jpg','officebuilding.pdf'),
	(4,'Schools & Campuses','Lighting Controls for Easy Installation and to Maximize Energy Efficiency.','<p>Often accredited with setting the pace for innovative solutions to energy savings, schools of all shapes and sizes throughout North America have looked to Douglas Lighting Controls to create easy to use lighting control systems to reduce energy consumption and to provide for a comfortable environment.</p>\r<p>Traditionally, Douglas Lighting Controls has provided centralized lighting control systems throughout schools; however, with the need for more specific control in certain areas, the standardization of classroom configurations, and the need to reduce long wiring runs, Douglas has the developed in now offers a decentralized stand-alone room control solutions. Now projects can take advantage of the benefits of centralized and decentralized systems to support the growing needs of modern educational facilities.</p>\r',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00','classroom.jpg','classroom.jpg','schools_campuses','Schools-Campuses.pdf'),
	(5,'Small Commercial/Retail','Standardized Systems for Common Applications','<p>Small Commercial and Retail facilities are often designed with common footprints and lighting requirements. For these applications, Douglas Lighting Controls has standardized relay/control panels that are easy to install and operate. These panels feature the same reliability found in our large system panels and also make use of our non-polarized 18/2 wire data network. Take a look at our LitePak systems to meet your small project requirements.</p>',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00','retail-fastfood.jpg','retail-fastfood.jpg','retail-fastfood.jpg','smallcommercial-retail.pdf');

/*!40000 ALTER TABLE `application_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table applications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `applications`;

CREATE TABLE `applications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `info_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `application_type_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `products_product_type_id_index` (`application_type_id`),
  CONSTRAINT `application_application_type_id_foreign` FOREIGN KEY (`application_type_id`) REFERENCES `application_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `applications` WRITE;
/*!40000 ALTER TABLE `applications` DISABLE KEYS */;

INSERT INTO `applications` (`id`, `name`, `info_2`, `application_type_id`, `created_at`, `updated_at`)
VALUES
	(1,'Rogers Centre','Toronto, ON',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(2,'Rogers Arena','Vancouver, BC',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(3,'Bell Centre','Montreal, QC',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(4,'Staples Center','Los Angeles, CA',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(5,'StubHub center','Los Angeles, CA',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(6,'Yankee Stadium','New York, NY',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(7,'Citi Field','New York, NY',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(8,'AT&T Stadium','Arlington, TX',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(9,'Georgia Dome','Atlanta , GA',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(10,'Infocision Stadium - University of Akron','Akron, OH',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(11,'Target Field','Minneapolis, MN',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(12,'Auburn Arena - Auburn University','Auburn, AL',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(13,'Consol Energy Center','Pittsburgh, PA',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(14,'Red Bull Arena','Harrison, NJ',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(15,'BBVA Compass Stadium','Houston, TX',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(16,'McLane Stadium','Waco, TX',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(17,'Reed Arena - Texas A & M University','College Station, TX',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(18,'Ole Miss - The University of Mississippi','Oxford, MS',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(19,'Kyle Field  - Texas A & M University','College Station, TX',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(20,'Vikings Stadium','Minneapolis, MN',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(21,'Roger\'s Place','Edmonton, AB',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(22,'New York Law School','New York, NY',4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(23,'Casey Middle School','Boulder, CO',4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(24,'North College Hill','Cincinnati, OH',4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(25,'Perkins School','Watertown, MS',4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(26,'Notre Dame Regional Secondary School','Vancouver, BC',4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(27,'Crawford Bay School','Crawford Bay, BC',4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(28,'LAUSD','Multiple Locations',4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(29,'Parkhill, Smith & Cooper, Inc','Lubbock, TX',3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(30,'Canada Place','Vancouver, BC',3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(31,'Federal Bureau of Investigation','Louisville, KY',3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(32,'Qualico','Winnipeg, MB',3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(33,'Amazon','Seattle, WA',3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(34,'Telus','Edmonton, AB',3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(35,'Turnberry Towers','Las Vegas, NV',3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(36,'Porsche Vancouver','Vancouver, BC',5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(37,'Starbucks','Various locations',5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(38,'T-Mobile','Various locations',5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(39,'Bass Pro Shops','Various Locations',5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(40,'Safeway','Various Locations',5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(41,'Save-On-Foods','Various Locations',5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(42,'Whole Foods','Various Locations',5,'0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `applications` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table certification_product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `certification_product`;

CREATE TABLE `certification_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `certification_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `certification_product_certification_id_index` (`certification_id`),
  KEY `certification_product_product_id_index` (`product_id`),
  CONSTRAINT `certification_product_certification_id_foreign` FOREIGN KEY (`certification_id`) REFERENCES `certifications` (`id`) ON DELETE CASCADE,
  CONSTRAINT `certification_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products_new` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `certification_product` WRITE;
/*!40000 ALTER TABLE `certification_product` DISABLE KEYS */;

INSERT INTO `certification_product` (`id`, `certification_id`, `product_id`, `created_at`, `updated_at`)
VALUES
	(1,5,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(2,6,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(3,5,39,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(4,6,39,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(5,7,39,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(6,5,40,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(7,6,40,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(8,7,40,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(9,5,41,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(10,6,41,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(11,7,41,'0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `certification_product` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table certification_product_copy
# ------------------------------------------------------------

DROP TABLE IF EXISTS `certification_product_copy`;

CREATE TABLE `certification_product_copy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `certification_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `certification_product_certification_id_index` (`certification_id`),
  KEY `certification_product_product_id_index` (`product_id`),
  CONSTRAINT `certification_product_copy_ibfk_1` FOREIGN KEY (`certification_id`) REFERENCES `certifications` (`id`) ON DELETE CASCADE,
  CONSTRAINT `certification_product_copy_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products_new` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `certification_product_copy` WRITE;
/*!40000 ALTER TABLE `certification_product_copy` DISABLE KEYS */;

INSERT INTO `certification_product_copy` (`id`, `certification_id`, `product_id`, `created_at`, `updated_at`)
VALUES
	(1,1,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(2,2,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(3,5,39,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(4,6,39,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(5,7,39,'0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `certification_product_copy` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table certifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `certifications`;

CREATE TABLE `certifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `certifications` WRITE;
/*!40000 ALTER TABLE `certifications` DISABLE KEYS */;

INSERT INTO `certifications` (`id`, `name`, `thumbnail`, `description`, `created_at`, `updated_at`)
VALUES
	(1,'CSACUS','csacus.png','logo for certifications','2014-07-08 21:04:54','2014-07-08 21:04:54'),
	(2,'Energy Star','energy_star.jpg','logo for certifications','2014-07-08 21:04:54','2014-07-08 21:04:54'),
	(3,'Lighitng Facts','lighting_facts.png','logo for certifications','2014-07-08 21:04:54','2014-07-08 21:04:54'),
	(4,'Damp Location','damp.jpg','logo for certifications','2014-07-08 21:04:54','2014-07-08 21:04:54'),
	(5,'ASHRAE 90.1-2010','','logo for certifications','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(6,'California Title 24 Compliant','t24.png','logo for certifications','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(7,'California Title 20 Listed','','logo for certifications','0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `certifications` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table doctype
# ------------------------------------------------------------

DROP TABLE IF EXISTS `doctype`;

CREATE TABLE `doctype` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `folder` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `doctype` WRITE;
/*!40000 ALTER TABLE `doctype` DISABLE KEYS */;

INSERT INTO `doctype` (`id`, `name`, `created_at`, `updated_at`, `folder`)
VALUES
	(1,'BIM','2014-07-08 20:45:31','2014-07-08 20:45:31','bim'),
	(2,'Installation Instructions','2014-07-08 20:48:04','2014-07-08 20:48:04','instructions'),
	(3,'IES','2014-07-08 20:48:04','2014-07-08 20:48:04','ies'),
	(4,'Spec Sheet','2014-07-08 20:48:04','2014-07-08 20:48:04','specs'),
	(5,'App Note','2014-07-08 20:48:04','2014-07-08 20:48:04','app_notes'),
	(6,'Brochures','2014-07-08 20:48:04','2014-07-08 20:48:04','brochures'),
	(7,'Images','2014-07-08 20:48:04','2014-07-08 20:48:04','images'),
	(8,'Cutsheet','0000-00-00 00:00:00','0000-00-00 00:00:00','cutsheets'),
	(9,'Manual','0000-00-00 00:00:00','0000-00-00 00:00:00','manuals'),
	(10,'Press Release','0000-00-00 00:00:00','0000-00-00 00:00:00','press_release'),
	(11,'Marketing Material','2016-10-21 21:16:14','2016-10-21 21:16:14','marketing_material'),
	(12,'Sales Tool','2016-10-21 21:16:34','2016-10-21 21:16:34','sales_tool');

/*!40000 ALTER TABLE `doctype` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table literature
# ------------------------------------------------------------

DROP TABLE IF EXISTS `literature`;

CREATE TABLE `literature` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `doctype_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `literature_doctype_id_foreign` (`doctype_id`),
  CONSTRAINT `literature_doctype_id_foreign` FOREIGN KEY (`doctype_id`) REFERENCES `doctype` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `literature` WRITE;
/*!40000 ALTER TABLE `literature` DISABLE KEYS */;

INSERT INTO `literature` (`id`, `doctype_id`, `name`, `description`, `path`, `thumbnail`, `created_at`, `updated_at`)
VALUES
	(1,2,'SD4-SD6_Install Instructions.pdf','','SD4-SD6_Install Instructions.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(2,2,'SD8_Install Instructions.pdf','','SD8_Install Instructions.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(3,1,'SD4M10L835CDWU.rfa','','SD4M10L835CDWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(4,1,'SD4M15L835CDWU.rfa','','SD4M15L835CDWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(5,1,'SD4M20L835CDWU.rfa','','SD4M20L835CDWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(6,1,'SD4W15L835WWU.rfa','','SD4W15L835WWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(7,1,'SD6M5K835CDWU.rfa','','SD6M5K835CDWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(8,1,'SD6M15L835CDWU.rfa','','SD6M15L835CDWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(9,1,'SD6M20L835CDWU.rfa','','SD6M20L835CDWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(10,1,'SD6M30L835CDWU.rfa','','SD6M30L835CDWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(11,1,'SD6M40L835CDWU.rfa','','SD6M40L835CDWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(12,1,'SD6SM10L835CDWU.rfa','','SD6SM10L835CDWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(13,1,'SD6SM15L835CDWU.rfa','','SD6SM15L835CDWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(14,1,'SD6SM20L835CDWU.rfa','','SD6SM20L835CDWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(15,1,'SD6W20L835WWU.rfa','','SD6W20L835WWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(16,1,'SD8M5K835CDU.rfa','','SD8M5K835CDU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(17,1,'SD8M5K835CSU.rfa','','SD8M5K835CSU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(18,1,'SD8M7K835CDU.rfa','','SD8M7K835CDU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(19,1,'SD8M7K835CSU.rfa','','SD8M7K835CSU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(20,1,'SD8M10K835CDU.rfa','','SD8M10K835CDU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(21,1,'SD8M10K835CSU.rfa','','SD8M10K835CSU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(22,4,'Spec Sheet _ SD4','','Spec Sheet_SD4.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(23,4,'Spec Sheet _ SD6','','Spec Sheet_SD6.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(24,4,'Spec Sheet _ SD6-30L-40L-5K_HL','','Spec Sheet_SD6-30L-40L-5K_HL.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(25,4,'Spec Sheet _ SD6S','','Spec Sheet_SD6S.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(26,4,'Spec Sheet _ SD8','','Spec Sheet_SD8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(27,7,'SD4-Hero','','SD4-Hero-Clip.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(28,7,'SD6-Hero','','SD6-Hero-DSC00588-Clip.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(29,7,'SD6S-Hero','','SD6S-Hero-DSC00768-Clip.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(30,7,'SD6-5K-Hero','','SD6-5K-Hero-DSC00735-Clip.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(31,7,'SD8-Hero','','SD8-Hero-DSC00892_Clip.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(32,7,'SD6-5K-beauty-shot','','SD6-5K-beauty-shot-casting-DSC09939.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(33,7,'SD6-CloseUp-UpperReflector','','SD6-CloseUp-UpperReflector-DSC00606.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(34,7,'SD6S-reflector-flange','','SD6S-reflector-flange-DSC01550-crop.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(35,7,'SD8-feature-heat-pipes','','SD8-feature-heat-pipes-DSC00879.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(37,7,'SD4-Overhead-DFK4 HERO','','SD4-Overhead-DFK4 HERO only-Clip.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(38,7,'SD4-Fixture','','SD4-Overhead-FixtureOnly-Clip.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(39,7,'SD4-Overhead-SD4andDFK4','','SD4-Overhead-SD4andDFK4-Clip.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(40,1,'Space Player PT 35 Degree Tilt-JW130HB.rfa','','Space Player PT 35 Degree Tilt-JW130HB.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(41,6,'SpacePlayer_Creative Content White Paper.pdf','','SpacePlayer_Creative Content White Paper.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(42,4,'SpacePlayer_Spec Sheet.pdf','','SpacePlayer_Spec Sheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(43,6,'Space Player Manual English.pdf','','Space Player Manual English.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(44,6,'SpacePlayer_FAQs.pdf','','SpacePlayer_FAQs.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(45,7,'PT-JW130(B)','','PT-JW130(B)_image.jpg','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(46,7,'Space Player Rotation','','Space Player Rotation.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(47,7,'SpacePlayer Black HERO','','SpacePlayer Black HERO.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(48,6,'SpacePlayer_Tri-Fold Brochure (low res).pdf','','SpacePlayer_Tri-Fold Brochure (low res).pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(49,3,'SD4M10L835CDWU.IES','','SD4M10L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(50,3,'SD4M15L835CDWU.IES','','SD4M15L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(51,3,'SD4M15L835WWU.IES','','SD4M15L835WWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(52,3,'SD4M20L835CDWU.IES','','SD4M20L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(53,3,'SD4W15L835CDWU.IES','','SD4W15L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(54,3,'SD4W15L835WWU.IES','','SD4W15L835WWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(55,3,'SD4W20L835CDWU.IES','','SD4W20L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(56,3,'SD6M15L835CDWU.IES','','SD6M15L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(57,3,'SD6M20L830CDWU.IES','','SD6M20L830CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(58,3,'SD6M20L835CDWU.IES','','SD6M20L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(59,3,'SD6M20L835WWU.IES','','SD6M20L835WWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(60,3,'SD6M20L840CDWU.IES','','SD6M20L840CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(61,3,'SD6M20L935CDWU.IES','','SD6M20L935CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(62,3,'SD6W20L835CDWU.IES','','SD6W20L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(63,3,'SD6W20L835WWU.IES','','SD6W20L835WWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(64,3,'SD6SM10L835CDWU.IES','','SD6SM10L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(65,3,'SD6SM15L835CDWU.IES','','SD6SM15L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(66,3,'SD6SM20L835CDWU.IES','','SD6SM20L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(67,3,'SD6SW15L835CDWU.IES','','SD6SW15L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(68,3,'SD6M5K835CDWU.IES','','SD6M5K835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(69,3,'SD6M30L835CDWU.IES','','SD6M30L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(70,3,'SD6M40L835CDWU.IES','','SD6M40L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(71,3,'SD6W5K835CDWU.IES','','SD6W5K835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(72,3,'SD6W30L835CDWU.IES','','SD6W30L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(73,3,'SD8-5K835-U_ST8-MCD.IES','','SD8-5K835-U_ST8-MCD.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(74,3,'SD8-5K835-U_ST8-NCD.IES','','SD8-5K835-U_ST8-NCD.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(75,3,'SD8-5K835-U_ST8-WCD.IES','','SD8-5K835-U_ST8-WCD.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(76,3,'SD8-7K835-U_ST8-MCD.IES','','SD8-7K835-U_ST8-MCD.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(77,3,'SD8-7K835-U_ST8-NCD.IES','','SD8-7K835-U_ST8-NCD.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(78,3,'SD8-7K835-U_ST8-WCD.IES','','SD8-7K835-U_ST8-WCD.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(79,3,'SD8-10K835-U_ST8-MCD.IES','','SD8-10K835-U_ST8-MCD.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(80,3,'SD8-10K835-U_ST8-MCS.IES','','SD8-10K835-U_ST8-MCS.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(81,3,'SD8-10K835-U_ST8-NCD.IES','','SD8-10K835-U_ST8-NCD.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(82,3,'SD8-10K835-U_ST8-NCS.IES','','SD8-10K835-U_ST8-NCS.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(83,3,'SD8-10K835-U_ST8-WCD.IES','','SD8-10K835-U_ST8-WCD.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(84,3,'SD8-10K835-U_ST8-WCS.IES','','SD8-10K835-U_ST8-WCS.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(85,8,'3500switches-CutSheet.pdf','','dialog/3500switches-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(86,8,'DialogOccSensor-CutSheet.pdf','','dialog/DialogOccSensor-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(87,8,'DialogWOS-CutSheet.pdf','','dialog/DialogWOS-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(88,8,'DialogWOW-CutSheet.pdf','','dialog/DialogWOW-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(89,8,'DLS-4500-120-CutSheet.pdf','','dialog/DLS-4500-120-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(90,8,'DLS-22000-CutSheet.pdf','','dialog/DLS-22000-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(91,8,'DLS-41000-120-CutSheet.pdf','','dialog/DLS-41000-120-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(92,8,'OPC-Server-CutSheet.pdf','','dialog/OPC-Server-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(93,8,'ValueSeriesCCM-Interface-CutSheet.pdf','','dialog/ValueSeriesCCM-Interface-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(94,8,'WCI-3928-cut-sheet.pdf','','dialog/WCI-3928-cut-sheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(95,8,'WDB-3314-cut-sheet.pdf','','dialog/WDB-3314-cut-sheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(96,8,'WDY-CCMINT-CutSheet.pdf','','legacy/WDY-CCMINT-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(97,8,'WIR-3110-CutSheet.pdf','','dialog/WIR-3110-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(98,8,'WLC-3150-custsheet.pdf','','dialog/WLC-3150-custsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(99,8,'WLCP-CutSheet.pdf','','dialog/WLCP-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(100,8,'WNG-3131-CutSheet.pdf','','dialog/WNG-3131-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(101,8,'WOC-3801-CutSheet.pdf','','dialog/WOC-3801-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(102,8,'GWS-Dialog-Global-Web-Server-cs.pdf','','dialog/GWS-Dialog-Global-Web-Server-cs.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(104,8,'WRD-3408-cut-sheet_0.pdf','','dialog/WRD-3408-cut-sheet_0.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(105,8,'WTI-3101-CutSheet.pdf','','dialog/WTI-3101-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(106,6,'Dialog-Brochure_09_lowres.pdf','','dialog/Dialog-Brochure_09_lowres.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(107,6,'Dialog-Overview-brochure.pdf','','dialog/Dialog-Overview-brochure.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(108,9,'BACnet-CCM-Manual.pdf','','dialog/BACnet-CCM-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(109,9,'CCM-BACnet-Manual.pdf','','dialog/CCM-BACnet-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(110,9,'WIR-3110-Manual.pdf','','dialog/WIR-3110-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(111,9,'WNG-3131-Manual.pdf','','dialog/WNG-3131-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(113,9,'WTI-3101-Manual.pdf','','dialog/WTI-3101-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(114,8,'Diversa-DualTech-WOR-24Vac-cut-sheet.pdf','','diversa/Diversa-DualTech-WOR-24Vac-cut-sheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(115,8,'Diversa-DualTech-WOR-120-277Vac-CutSheet.pdf','','diversa/Diversa-DualTech-WOR-120-277Vac-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(116,8,'Diversa-Dualtech-WOR-347Vac-CutSheet.pdf','','diversa/Diversa-Dualtech-WOR-347Vac-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(117,9,'Diversa-DualTech-WOR-Manual.pdf','','diversa/Diversa-DualTech-WOR-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(118,8,'Diversa-DualTech-WVR-24Vac-cutsheet.pdf','','diversa/Diversa-DualTech-WVR-24Vac-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(119,8,'Diversa-PIR-WOR-24-LowVoltage.pdf','','diversa/Diversa-PIR-WOR-24-LowVoltage.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(120,8,'Diversa-PIR-WOR-347.pdf','','diversa/Diversa-PIR-WOR-347.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(121,8,'Diversa-PIR-WOR-120-277.pdf','','diversa/Diversa-PIR-WOR-120-277.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(122,6,'DiversaBrochure2015-09-lowres.pdf','','diversa/DiversaBrochure2015-09-lowres.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(173,6,'Diversa-brochure.pdf','','diversa/Diversa-brochure.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(174,8,'DiversaDualTechWOS24Vac-cutsheet.pdf','','diversa/DiversaDualTechWOS24Vac-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(175,8,'DiversaDualTechWOS347Vac-cutsheet.pdf','','diversa/DiversaDualTechWOS347Vac-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(176,9,'DiversaDualTechWOSManual-ALL.pdf','','diversa/DiversaDualTechWOSManual-ALL.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(177,8,'DiversaDualTechWVS347Vac-cutsheet.pdf','','diversa/DiversaDualTechWVS347Vac-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(178,2,'DiversaPIRWOS24Vac-InstallGuide.pdf','','diversa/DiversaPIRWOS24Vac-InstallGuide.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(179,2,'DiversaPIRWOS120-277VacInstallGuide.pdf','','diversa/DiversaPIRWOS120-277VacInstallGuide.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(180,2,'DiversaPIRWOS347Vac-InstallGuide.pdf','','diversa/DiversaPIRWOS347Vac-InstallGuide.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(181,8,'DiversaDualTechWOS120-277Vac-cutsheet.pdf','','diversa/DiversaDualTechWOS120-277Vac-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(182,8,'DiversaDualTechWVS120-277Vac-cutsheet.pdf','','diversa/DiversaDualTechWVS120-277Vac-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(183,6,'Diversa-brochure.pdf','','diversa/Diversa-brochure.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(184,9,'WOW-manual.pdf','','diversa/WOW-manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(185,8,'WOW-WVW-cutsheet.pdf','','diversa/WOW-WVW-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(186,6,'Diversa-brochure.pdf','','diversa/Diversa-brochure.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(187,8,'DiversaWP-PP20-2P-D-cutsheet.pdf','','diversa/DiversaWP-PP20-2P-D-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(188,8,'DiversaWP-PP20-347-D-cutsheet.pdf','','diversa/DiversaWP-PP20-347-D-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(189,8,'DiversaWP-PP20-D-cutsheet.pdf','','diversa/DiversaWP-PP20-D-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(190,8,'3500-series-WallSwitches-cutsheet.pdf','','wall_switch/3500-series-WallSwitches-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(191,8,'8700-Series-cutsheet.pdf','','wall_switch/8700-Series-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(192,8,'FacePlate-custsheet.pdf','','wall_switch/FacePlate-custsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(193,8,'WN-7700SwitchEnclosures-cutsheet.pdf','','wall_switch/WN-7700SwitchEnclosures-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(194,8,'WBI-2671-cutsheet.pdf','','tools/WBI-2671-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(195,8,'WNG-2122-cutsheet.pdf','','tools/WNG-2122-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(196,8,'WNG-2133-bacnet-gtwy-cutsheet.pdf','','tools/WNG-2133-bacnet-gtwy-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(197,8,'WNG-3131-cutsheet.pdf','','tools/WNG-3131-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(198,8,'WNX-2624-Lonworks-Node-cutsheet.pdf','','tools/WNX-2624-Lonworks-Node-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(199,8,'WR-6161 WR-6172-cutsheet.pdf','','tools/WR-6161 WR-6172-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(200,8,'WR-6221-cutsheet.pdf','','tools/WR-6221-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(201,8,'WRE-9242Z-cutsheet.pdf','','tools/WRE-9242Z-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(202,8,'WR-RIB240-cutsheet.pdf','','tools/WR-RIB240-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(203,9,'WIR-3110-Diversa-Manual.pdf','','tools/WIR-3110-Diversa-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(204,9,'WNG-2133-Manual.pdf','','tools/WNG-2133-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(205,9,'WNG-3131 - Manual.pdf','','tools/WNG-3131 - Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(206,6,'DialogRoomControllerKitWiringGuide.pdf','','dialog_room_controller/DialogRoomControllerKitWiringGuide.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(207,6,'DRC-ReferenceGuide-ns-2015.pdf','','dialog_room_controller/DRC-ReferenceGuide-ns-2015.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(208,8,'DialogRoomController-cutsheet.pdf','','dialog_room_controller/DialogRoomController-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(209,9,'DialogRoomController-Manual.pdf','','dialog_room_controller/DialogRoomController-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(210,10,'DialogRoomController-pressrelease.pdf','','dialog_room_controller/DialogRoomController-pressrelease.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(211,8,'DRC-DaylightSensor-cutsheet.pdf','','dialog_room_controller/DRC-DaylightSensor-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(212,8,'DRC-OccupancySensor-cutsheet.pdf','','dialog_room_controller/DRC-OccupancySensor-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(213,8,'DRC-SwitchStations-cutsheet.pdf','','dialog_room_controller/DRC-SwitchStations-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(214,6,'DRC-sellsheet.pdf','','dialog_room_controller/DRC-sellsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(215,4,'DRC-SpecificationDoc.doc','','dialog_room_controller/DRC-SpecificationDoc.doc','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(216,4,'DRC-SpecificationDoc.pdf','','dialog_room_controller/DRC-SpecificationDoc.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(217,6,'LitePak-brochure.pdf','','litepak/LitePak-brochure.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(218,8,'LitePak-cutsheet.pdf','','litepak/LitePak-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(219,9,'LitePak-Manual.pdf','','litepak/LitePak-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(220,8,'Litepak-NEMA4-cutsheet.pdf','','litepak/Litepak-NEMA4-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(221,9,'WR3330-801manual.pdf','','full-two-way/WR3330-801manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(222,9,'WR3891-8.pdf','','full-two-way/WR3891-8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(223,9,'WRT1320-8.pdf','','full-two-way/WRT1320-8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(224,9,'WRT3364K-8-WRT3374K-8.pdf','','full-two-way/WRT3364K-8-WRT3374K-8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(225,9,'WRT3365-8-WRT3375-8.pdf','','full-two-way/WRT3365-8-WRT3375-8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(226,9,'WRT3367-8.pdf','','full-two-way/WRT3367-8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(227,9,'WRT3394-8-WRT3395-8.pdf','','full-two-way/WRT3394-8-WRT3395-8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(228,9,'WRT3540K-8-manual.pdf','','full-two-way/WRT3540K-8-manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(229,9,'WRT3657-8.pdf','','full-two-way/WRT3657-8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(230,9,'WRT6024-48-72WK-8-WRT6120-44-68WK-8.pdf','','full-two-way/WRT6024-48-72WK-8-WRT6120-44-68WK-8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(231,9,'WRT9103K-89-manual.pdf','','full-two-way/WRT9103K-89-manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(232,9,'WRT9261-8.pdf','','full-two-way/WRT9261-8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(233,9,'WRT9500-8.pdf','','full-two-way/WRT9500-8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(234,9,'WRT9600-8.pdf','','full-two-way/WRT9600-8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(235,9,'SE-MANUAL.pdf','','full-two-way/SE-MANUAL.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(236,9,'FPX-2007-User-Manual.pdf','','full-two-way/FPX-2007-User-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(237,9,'TB100-Manual.pdf','','full-two-way/TB100-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(238,9,'WR3891-8-manual.pdf','','full-two-way/WR3891-8-manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(239,8,'DMX-1005-cutsheet.pdf','','legacy/DMX-1005-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(240,9,'LON-2624-SelfConfig-Manual.pdf','','legacy/LON-2624-SelfConfig-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(241,9,'MC-6000A-Manual.pdf','','legacy/MC-6000A-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(242,8,'MC-6000-cutsheet.pdf','','legacy/MC-6000-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(243,9,'MC-6000N-Manual.pdf','','legacy/MC-6000N-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(244,8,'MC-6000-OPC-Server-cutsheet.pdf','','legacy/MC-6000-OPC-Server-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(245,8,'MC-6210-cutsheet.pdf','','legacy/MC-6210-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(246,8,'MC-6308-cutsheet.pdf','','legacy/MC-6308-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(247,8,'MC-6416-cutsheet.pdf','','legacy/MC-6416-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(248,8,'W-2000-OPC-Server-cutsheet.pdf','','legacy/W-2000-OPC-Server-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(249,8,'WBC-2512-cutsheet.pdf','','legacy/WBC-2512-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(250,8,'WLB-2910-cutsheet .pdf','','legacy/WLB-2910-cutsheet .pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(251,8,'WLP-2999-cutsheet.pdf','','legacy/WLP-2999-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(252,8,'WNG-2122-cutsheet.pdf','','legacy/WNG-2122-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(253,8,'WNG-2133-cutsheet.pdf','','legacy/WNG-2133-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(254,9,'WNG-2133-Manual.pdf','','legacy/WNG-2133-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(255,8,'WNP-2150-cutsheet.pdf','','legacy/WNP-2150-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(256,9,'WNP-2150-Manual.pdf','','legacy/WNP-2150-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(257,8,'WNR-2112-cutsheet.pdf','','legacy/WNR-2112-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(258,8,'WNS-2300-cutsheet.pdf','','legacy/WNS-2300-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(259,8,'WNS-2308-cutsheet.pdf','','legacy/WNS-2308-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(260,8,'WNX-2624-cutsheet.pdf','','legacy/WNX-2624-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(261,8,'WPN-5821-5822-dimmer-cutsheet.pdf','','legacy/WPN-5821-5822-dimmer-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(262,8,'WR-8600 switches-cutsheet.pdf','','legacy/WR-8600 switches-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(263,8,'WR-8800-cutsheet.pdf','','legacy/WR-8800-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(264,9,'WRS-2224-config.pdf','','legacy/WRS-2224-config.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(265,8,'WRS-2224-cutsheet.pdf','','legacy/WRS-2224-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(266,9,'WRS-2224-Manual.pdf','','legacy/WRS-2224-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(267,8,'WSG-2300-cutsheet.pdf','','legacy/WSG-2300-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(268,8,'WSP-2718-cutsheet.pdf','','legacy/WSP-2718-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(269,8,'WSP-2718-D-cutsheet.pdf','','legacy/WSP-2718-D-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(270,8,'WTI-2332-FT-cutsheet.pdf','','legacy/WTI-2332-FT-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(271,9,'WTI-2332-FT-Manual.pdf','','legacy/WTI-2332-FT-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(272,8,'WTP-4408-cutsheet.pdf','','legacy/WTP-4408-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(273,9,'WTP-4408-Manual.pdf','','legacy/WTP-4408-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(274,8,'WTS-4181-cutsheet.pdf','','legacy/WTS-4181-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(275,8,'WIR-3110-cutsheet.pdf','','tools/WIR-3110-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(276,9,'WIR-3110-Manual-Dialog.pdf','','tools/WIR-3110-Manual-Dialog.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(278,6,'Full-2Way-brochure.pdf','','full-two-way/Full-2Way-brochure.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(279,8,'PWE-cutsheet2015-12-03.pdf','','dialog/PWE-cutsheet2015-12-03.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(280,8,'WRC-LCD9261-cutsheet.pdf','','dialog_room_controller/WRC-LCD9261-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(281,8,'WTP-1000-WTP-2000-TrakPak-cutsheet.pdf','','tools/WTP-1000-WTP-2000-TrakPak-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(282,8,'WPS-3711-cutsheet.pdf','','dialog/WPS-3711-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(283,8,'WPS-ExteriorDaylightSensors-CutSheet.pdf','','dialog/WPS-ExteriorDaylightSensors-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(284,9,'DialogSystemManual-WLC-3150-2015-07.pdf','','dialog/DialogSystemManual-WLC-3150-2015-07.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(285,8,'WR-4075-WR-4040-cutsheet.pdf','','dialog/WR-4075-WR-4040-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(286,8,'Diversa-WOS-PIR-24VacCutsheet.pdf','','diversa/Diversa-WOS-PIR-24VacCutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(287,8,'Diversa-WOS-PIR-120-277VacCutsheet.pdf','','diversa/Diversa-WOS-PIR-120-277VacCutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(288,8,'Diversa-WOS-PIR-347VacCutsheet.pdf','','diversa/Diversa-WOS-PIR-347VacCutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(289,6,'WRC-3160-Kits.dwg','','dialog_room_controller/WRC-3160-Kits.dwg','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(290,6,'WRC-3160-KitsBy Page.dwg','','dialog_room_controller/WRC-3160-KitsBy Page.dwg','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(291,6,'Full-2Way_brochure_Fr.pdf','','full-two-way/160401_F2W_FR.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(292,8,'WORSDD1-N-N-cs.pdf','','diversa/WORSDD1-N-N-cs.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(293,12,'DialogRoomController-SellSheet.pdf','','resource_library/DialogRoomController-SellSheet.pdf','','2016-10-24 17:07:54','2016-10-24 17:07:54');

/*!40000 ALTER TABLE `literature` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table literature_copy_gabrielle_14th_April_2016
# ------------------------------------------------------------

DROP TABLE IF EXISTS `literature_copy_gabrielle_14th_April_2016`;

CREATE TABLE `literature_copy_gabrielle_14th_April_2016` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `doctype_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `literature_doctype_id_foreign` (`doctype_id`),
  CONSTRAINT `literature_copy_gabrielle_14th_April_2016_ibfk_1` FOREIGN KEY (`doctype_id`) REFERENCES `doctype` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `literature_copy_gabrielle_14th_April_2016` WRITE;
/*!40000 ALTER TABLE `literature_copy_gabrielle_14th_April_2016` DISABLE KEYS */;

INSERT INTO `literature_copy_gabrielle_14th_April_2016` (`id`, `doctype_id`, `name`, `description`, `path`, `thumbnail`, `created_at`, `updated_at`)
VALUES
	(1,2,'SD4-SD6_Install Instructions.pdf','','SD4-SD6_Install Instructions.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(2,2,'SD8_Install Instructions.pdf','','SD8_Install Instructions.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(3,1,'SD4M10L835CDWU.rfa','','SD4M10L835CDWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(4,1,'SD4M15L835CDWU.rfa','','SD4M15L835CDWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(5,1,'SD4M20L835CDWU.rfa','','SD4M20L835CDWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(6,1,'SD4W15L835WWU.rfa','','SD4W15L835WWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(7,1,'SD6M5K835CDWU.rfa','','SD6M5K835CDWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(8,1,'SD6M15L835CDWU.rfa','','SD6M15L835CDWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(9,1,'SD6M20L835CDWU.rfa','','SD6M20L835CDWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(10,1,'SD6M30L835CDWU.rfa','','SD6M30L835CDWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(11,1,'SD6M40L835CDWU.rfa','','SD6M40L835CDWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(12,1,'SD6SM10L835CDWU.rfa','','SD6SM10L835CDWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(13,1,'SD6SM15L835CDWU.rfa','','SD6SM15L835CDWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(14,1,'SD6SM20L835CDWU.rfa','','SD6SM20L835CDWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(15,1,'SD6W20L835WWU.rfa','','SD6W20L835WWU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(16,1,'SD8M5K835CDU.rfa','','SD8M5K835CDU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(17,1,'SD8M5K835CSU.rfa','','SD8M5K835CSU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(18,1,'SD8M7K835CDU.rfa','','SD8M7K835CDU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(19,1,'SD8M7K835CSU.rfa','','SD8M7K835CSU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(20,1,'SD8M10K835CDU.rfa','','SD8M10K835CDU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(21,1,'SD8M10K835CSU.rfa','','SD8M10K835CSU.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(22,4,'Spec Sheet _ SD4','','Spec Sheet_SD4.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(23,4,'Spec Sheet _ SD6','','Spec Sheet_SD6.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(24,4,'Spec Sheet _ SD6-30L-40L-5K_HL','','Spec Sheet_SD6-30L-40L-5K_HL.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(25,4,'Spec Sheet _ SD6S','','Spec Sheet_SD6S.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(26,4,'Spec Sheet _ SD8','','Spec Sheet_SD8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(27,7,'SD4-Hero','','SD4-Hero-Clip.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(28,7,'SD6-Hero','','SD6-Hero-DSC00588-Clip.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(29,7,'SD6S-Hero','','SD6S-Hero-DSC00768-Clip.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(30,7,'SD6-5K-Hero','','SD6-5K-Hero-DSC00735-Clip.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(31,7,'SD8-Hero','','SD8-Hero-DSC00892_Clip.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(32,7,'SD6-5K-beauty-shot','','SD6-5K-beauty-shot-casting-DSC09939.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(33,7,'SD6-CloseUp-UpperReflector','','SD6-CloseUp-UpperReflector-DSC00606.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(34,7,'SD6S-reflector-flange','','SD6S-reflector-flange-DSC01550-crop.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(35,7,'SD8-feature-heat-pipes','','SD8-feature-heat-pipes-DSC00879.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(37,7,'SD4-Overhead-DFK4 HERO','','SD4-Overhead-DFK4 HERO only-Clip.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(38,7,'SD4-Fixture','','SD4-Overhead-FixtureOnly-Clip.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(39,7,'SD4-Overhead-SD4andDFK4','','SD4-Overhead-SD4andDFK4-Clip.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(40,1,'Space Player PT 35 Degree Tilt-JW130HB.rfa','','Space Player PT 35 Degree Tilt-JW130HB.rfa','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(41,6,'SpacePlayer_Creative Content White Paper.pdf','','SpacePlayer_Creative Content White Paper.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(42,4,'SpacePlayer_Spec Sheet.pdf','','SpacePlayer_Spec Sheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(43,6,'Space Player Manual English.pdf','','Space Player Manual English.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(44,6,'SpacePlayer_FAQs.pdf','','SpacePlayer_FAQs.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(45,7,'PT-JW130(B)','','PT-JW130(B)_image.jpg','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(46,7,'Space Player Rotation','','Space Player Rotation.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(47,7,'SpacePlayer Black HERO','','SpacePlayer Black HERO.tif','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(48,6,'SpacePlayer_Tri-Fold Brochure (low res).pdf','','SpacePlayer_Tri-Fold Brochure (low res).pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(49,3,'SD4M10L835CDWU.IES','','SD4M10L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(50,3,'SD4M15L835CDWU.IES','','SD4M15L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(51,3,'SD4M15L835WWU.IES','','SD4M15L835WWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(52,3,'SD4M20L835CDWU.IES','','SD4M20L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(53,3,'SD4W15L835CDWU.IES','','SD4W15L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(54,3,'SD4W15L835WWU.IES','','SD4W15L835WWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(55,3,'SD4W20L835CDWU.IES','','SD4W20L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(56,3,'SD6M15L835CDWU.IES','','SD6M15L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(57,3,'SD6M20L830CDWU.IES','','SD6M20L830CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(58,3,'SD6M20L835CDWU.IES','','SD6M20L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(59,3,'SD6M20L835WWU.IES','','SD6M20L835WWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(60,3,'SD6M20L840CDWU.IES','','SD6M20L840CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(61,3,'SD6M20L935CDWU.IES','','SD6M20L935CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(62,3,'SD6W20L835CDWU.IES','','SD6W20L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(63,3,'SD6W20L835WWU.IES','','SD6W20L835WWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(64,3,'SD6SM10L835CDWU.IES','','SD6SM10L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(65,3,'SD6SM15L835CDWU.IES','','SD6SM15L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(66,3,'SD6SM20L835CDWU.IES','','SD6SM20L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(67,3,'SD6SW15L835CDWU.IES','','SD6SW15L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(68,3,'SD6M5K835CDWU.IES','','SD6M5K835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(69,3,'SD6M30L835CDWU.IES','','SD6M30L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(70,3,'SD6M40L835CDWU.IES','','SD6M40L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(71,3,'SD6W5K835CDWU.IES','','SD6W5K835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(72,3,'SD6W30L835CDWU.IES','','SD6W30L835CDWU.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(73,3,'SD8-5K835-U_ST8-MCD.IES','','SD8-5K835-U_ST8-MCD.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(74,3,'SD8-5K835-U_ST8-NCD.IES','','SD8-5K835-U_ST8-NCD.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(75,3,'SD8-5K835-U_ST8-WCD.IES','','SD8-5K835-U_ST8-WCD.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(76,3,'SD8-7K835-U_ST8-MCD.IES','','SD8-7K835-U_ST8-MCD.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(77,3,'SD8-7K835-U_ST8-NCD.IES','','SD8-7K835-U_ST8-NCD.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(78,3,'SD8-7K835-U_ST8-WCD.IES','','SD8-7K835-U_ST8-WCD.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(79,3,'SD8-10K835-U_ST8-MCD.IES','','SD8-10K835-U_ST8-MCD.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(80,3,'SD8-10K835-U_ST8-MCS.IES','','SD8-10K835-U_ST8-MCS.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(81,3,'SD8-10K835-U_ST8-NCD.IES','','SD8-10K835-U_ST8-NCD.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(82,3,'SD8-10K835-U_ST8-NCS.IES','','SD8-10K835-U_ST8-NCS.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(83,3,'SD8-10K835-U_ST8-WCD.IES','','SD8-10K835-U_ST8-WCD.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(84,3,'SD8-10K835-U_ST8-WCS.IES','','SD8-10K835-U_ST8-WCS.ies','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(85,8,'3500switches-CutSheet.pdf','','dialog/3500switches-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(86,8,'DialogOccSensor-CutSheet.pdf','','dialog/DialogOccSensor-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(87,8,'DialogWOS-CutSheet.pdf','','dialog/DialogWOS-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(88,8,'DialogWOW-CutSheet.pdf','','dialog/DialogWOW-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(89,8,'DLS-4500-120-CutSheet.pdf','','dialog/DLS-4500-120-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(90,8,'DLS-22000-CutSheet.pdf','','dialog/DLS-22000-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(91,8,'DLS-41000-120-CutSheet.pdf','','dialog/DLS-41000-120-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(92,8,'OPC-Server-CutSheet.pdf','','dialog/OPC-Server-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(93,8,'ValueSeriesCCM-Interface-CutSheet.pdf','','dialog/ValueSeriesCCM-Interface-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(94,8,'WCI-3928-cut-sheet.pdf','','dialog/WCI-3928-cut-sheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(95,8,'WDB-3314-cut-sheet.pdf','','dialog/WDB-3314-cut-sheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(96,8,'WDY-CCMINT-CutSheet.pdf','','legacy/WDY-CCMINT-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(97,8,'WIR-3110-CutSheet.pdf','','dialog/WIR-3110-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(98,8,'WLC-3150-custsheet.pdf','','dialog/WLC-3150-custsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(99,8,'WLCP-CutSheet.pdf','','dialog/WLCP-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(100,8,'WNG-3131-CutSheet.pdf','','dialog/WNG-3131-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(101,8,'WOC-3801-CutSheet.pdf','','dialog/WOC-3801-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(102,8,'WPC-GlobalWebServer-CutSheet.pdf','','dialog/WPC-GlobalWebServer-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(104,8,'WRD-3408-cut-sheet_0.pdf','','dialog/WRD-3408-cut-sheet_0.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(105,8,'WTI-3101-CutSheet.pdf','','dialog/WTI-3101-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(106,6,'Dialog-Brochure_09_lowres.pdf','','dialog/Dialog-Brochure_09_lowres.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(107,6,'Dialog-Overview-brochure.pdf','','dialog/Dialog-Overview-brochure.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(108,9,'BACnet-CCM-Manual.pdf','','dialog/BACnet-CCM-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(109,9,'CCM-BACnet-Manual.pdf','','dialog/CCM-BACnet-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(110,9,'WIR-3110-Manual.pdf','','dialog/WIR-3110-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(111,9,'WNG-3131-Manual.pdf','','dialog/WNG-3131-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(113,9,'WTI-3101-Manual.pdf','','dialog/WTI-3101-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(114,8,'Diversa-DualTech-WOR-24Vac-cut-sheet.pdf','','diversa/Diversa-DualTech-WOR-24Vac-cut-sheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(115,8,'Diversa-DualTech-WOR-120-277Vac-CutSheet.pdf','','diversa/Diversa-DualTech-WOR-120-277Vac-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(116,8,'Diversa-Dualtech-WOR-347Vac-CutSheet.pdf','','diversa/Diversa-Dualtech-WOR-347Vac-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(117,9,'Diversa-DualTech-WOR-Manual.pdf','','diversa/Diversa-DualTech-WOR-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(118,8,'Diversa-DualTech-WVR-24Vac-cutsheet.pdf','','diversa/Diversa-DualTech-WVR-24Vac-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(119,8,'Diversa-PIR-WOR-24-LowVoltage.pdf','','diversa/Diversa-PIR-WOR-24-LowVoltage.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(120,8,'Diversa-PIR-WOR-347.pdf','','diversa/Diversa-PIR-WOR-347.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(121,8,'Diversa-PIR-WOR-120-277.pdf','','diversa/Diversa-PIR-WOR-120-277.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(122,6,'DiversaBrochure2015-09-lowres.pdf','','diversa/DiversaBrochure2015-09-lowres.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(173,6,'Diversa-brochure.pdf','','diversa/Diversa-brochure.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(174,8,'DiversaDualTechWOS24Vac-cutsheet.pdf','','diversa/DiversaDualTechWOS24Vac-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(175,8,'DiversaDualTechWOS347Vac-cutsheet.pdf','','diversa/DiversaDualTechWOS347Vac-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(176,9,'DiversaDualTechWOSManual-ALL.pdf','','diversa/DiversaDualTechWOSManual-ALL.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(177,8,'DiversaDualTechWVS347Vac-cutsheet.pdf','','diversa/DiversaDualTechWVS347Vac-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(178,2,'DiversaPIRWOS24Vac-InstallGuide.pdf','','diversa/DiversaPIRWOS24Vac-InstallGuide.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(179,2,'DiversaPIRWOS120-277VacInstallGuide.pdf','','diversa/DiversaPIRWOS120-277VacInstallGuide.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(180,2,'DiversaPIRWOS347Vac-InstallGuide.pdf','','diversa/DiversaPIRWOS347Vac-InstallGuide.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(181,8,'DiversaDualTechWOS120-277Vac-cutsheet.pdf','','diversa/DiversaDualTechWOS120-277Vac-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(182,8,'DiversaDualTechWVS120-277Vac-cutsheet.pdf','','diversa/DiversaDualTechWVS120-277Vac-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(183,6,'Diversa-brochure.pdf','','diversa/Diversa-brochure.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(184,9,'WOW-manual.pdf','','diversa/WOW-manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(185,8,'WOW-WVW-cutsheet.pdf','','diversa/WOW-WVW-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(186,6,'Diversa-brochure.pdf','','diversa/Diversa-brochure.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(187,8,'DiversaWP-PP20-2P-D-cutsheet.pdf','','diversa/DiversaWP-PP20-2P-D-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(188,8,'DiversaWP-PP20-347-D-cutsheet.pdf','','diversa/DiversaWP-PP20-347-D-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(189,8,'DiversaWP-PP20-D-cutsheet.pdf','','diversa/DiversaWP-PP20-D-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(190,8,'3500-series-WallSwitches-cutsheet.pdf','','wall_switch/3500-series-WallSwitches-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(191,8,'8700-Series-cutsheet.pdf','','wall_switch/8700-Series-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(192,8,'FacePlate-custsheet.pdf','','wall_switch/FacePlate-custsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(193,8,'WN-7700SwitchEnclosures-cutsheet.pdf','','wall_switch/WN-7700SwitchEnclosures-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(194,8,'WBI-2671-cutsheet.pdf','','tools/WBI-2671-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(195,8,'WNG-2122-cutsheet.pdf','','tools/WNG-2122-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(196,8,'WNG-2133-bacnet-gtwy-cutsheet.pdf','','tools/WNG-2133-bacnet-gtwy-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(197,8,'WNG-3131-cutsheet.pdf','','tools/WNG-3131-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(198,8,'WNX-2624-Lonworks-Node-cutsheet.pdf','','tools/WNX-2624-Lonworks-Node-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(199,8,'WR-6161 WR-6172-cutsheet.pdf','','tools/WR-6161 WR-6172-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(200,8,'WR-6221-cutsheet.pdf','','tools/WR-6221-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(201,8,'WRE-9242Z-cutsheet.pdf','','tools/WRE-9242Z-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(202,8,'WR-RIB240-cutsheet.pdf','','tools/WR-RIB240-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(203,9,'WIR-3110-Diversa-Manual.pdf','','tools/WIR-3110-Diversa-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(204,9,'WNG-2133-Manual.pdf','','tools/WNG-2133-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(205,9,'WNG-3131 - Manual.pdf','','tools/WNG-3131 - Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(206,6,'DialogRoomControllerKitWiringGuide.pdf','','dialog_room_controller/DialogRoomControllerKitWiringGuide.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(207,6,'DRC-ReferenceGuide-ns-2015.pdf','','dialog_room_controller/DRC-ReferenceGuide-ns-2015.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(208,8,'DialogRoomController-cutsheet.pdf','','dialog_room_controller/DialogRoomController-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(209,9,'DialogRoomController-Manual.pdf','','dialog_room_controller/DialogRoomController-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(210,10,'DialogRoomController-pressrelease.pdf','','dialog_room_controller/DialogRoomController-pressrelease.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(211,8,'DRC-DaylightSensor-cutsheet.pdf','','dialog_room_controller/DRC-DaylightSensor-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(212,8,'DRC-OccupancySensor-cutsheet.pdf','','dialog_room_controller/DRC-OccupancySensor-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(213,8,'DRC-SwitchStations-cutsheet.pdf','','dialog_room_controller/DRC-SwitchStations-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(214,6,'DRC-sellsheet.pdf','','dialog_room_controller/DRC-sellsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(215,4,'DRC-SpecificationDoc.doc','','dialog_room_controller/DRC-SpecificationDoc.doc','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(216,4,'DRC-SpecificationDoc.pdf','','dialog_room_controller/DRC-SpecificationDoc.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(217,6,'LitePak-brochure.pdf','','litepak/LitePak-brochure.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(218,8,'LitePak-cutsheet.pdf','','litepak/LitePak-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(219,9,'LitePak-Manual.pdf','','litepak/LitePak-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(220,8,'Litepak-NEMA4-cutsheet.pdf','','litepak/Litepak-NEMA4-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(221,9,'WR3330-801manual.pdf','','full_two_way/WR3330-801manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(222,9,'WR3891-8.pdf','','full_two_way/WR3891-8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(223,9,'WRT1320-8.pdf','','full_two_way/WRT1320-8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(224,9,'WRT3364K-8-WRT3374K-8.pdf','','full_two_way/WRT3364K-8-WRT3374K-8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(225,9,'WRT3365-8-WRT3375-8.pdf','','full_two_way/WRT3365-8-WRT3375-8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(226,9,'WRT3367-8.pdf','','full_two_way/WRT3367-8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(227,9,'WRT3394-8-WRT3395-8.pdf','','full_two_way/WRT3394-8-WRT3395-8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(228,9,'WRT3540K-8-manual.pdf','','full_two_way/WRT3540K-8-manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(229,9,'WRT3657-8.pdf','','full_two_way/WRT3657-8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(230,9,'WRT6024-48-72WK-8-WRT6120-44-68WK-8.pdf','','full_two_way/WRT6024-48-72WK-8-WRT6120-44-68WK-8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(231,9,'WRT9103K-89-manual.pdf','','full_two_way/WRT9103K-89-manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(232,9,'WRT9261-8.pdf','','full_two_way/WRT9261-8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(233,9,'WRT9500-8.pdf','','full_two_way/WRT9500-8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(234,9,'WRT9600-8.pdf','','full_two_way/WRT9600-8.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(235,9,'SE-MANUAL.pdf','','full_two_way/SE-MANUAL.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(236,9,'FPX-2007-User-Manual.pdf','','full_two_way/FPX-2007-User-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(237,9,'TB100-Manual.pdf','','full_two_way/TB100-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(238,9,'WR3891-8-manual.pdf','','full_two_way/WR3891-8-manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(239,8,'DMX-1005-cutsheet.pdf','','legacy/DMX-1005-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(240,9,'LON-2624-SelfConfig-Manual.pdf','','legacy/LON-2624-SelfConfig-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(241,9,'MC-6000A-Manual.pdf','','legacy/MC-6000A-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(242,8,'MC-6000-cutsheet.pdf','','legacy/MC-6000-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(243,9,'MC-6000N-Manual.pdf','','legacy/MC-6000N-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(244,8,'MC-6000-OPC-Server-cutsheet.pdf','','legacy/MC-6000-OPC-Server-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(245,8,'MC-6210-cutsheet.pdf','','legacy/MC-6210-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(246,8,'MC-6308-cutsheet.pdf','','legacy/MC-6308-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(247,8,'MC-6416-cutsheet.pdf','','legacy/MC-6416-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(248,8,'W-2000-OPC-Server-cutsheet.pdf','','legacy/W-2000-OPC-Server-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(249,8,'WBC-2512-cutsheet.pdf','','legacy/WBC-2512-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(250,8,'WLB-2910-cutsheet .pdf','','legacy/WLB-2910-cutsheet .pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(251,8,'WLP-2999-cutsheet.pdf','','legacy/WLP-2999-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(252,8,'WNG-2122-cutsheet.pdf','','legacy/WNG-2122-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(253,8,'WNG-2133-cutsheet.pdf','','legacy/WNG-2133-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(254,9,'WNG-2133-Manual.pdf','','legacy/WNG-2133-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(255,8,'WNP-2150-cutsheet.pdf','','legacy/WNP-2150-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(256,9,'WNP-2150-Manual.pdf','','legacy/WNP-2150-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(257,8,'WNR-2112-cutsheet.pdf','','legacy/WNR-2112-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(258,8,'WNS-2300-cutsheet.pdf','','legacy/WNS-2300-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(259,8,'WNS-2308-cutsheet.pdf','','legacy/WNS-2308-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(260,8,'WNX-2624-cutsheet.pdf','','legacy/WNX-2624-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(261,8,'WPN-5821-5822-dimmer-cutsheet.pdf','','legacy/WPN-5821-5822-dimmer-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(262,8,'WR-8600 switches-cutsheet.pdf','','legacy/WR-8600 switches-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(263,8,'WR-8800-cutsheet.pdf','','legacy/WR-8800-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(264,9,'WRS-2224-config.pdf','','legacy/WRS-2224-config.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(265,8,'WRS-2224-cutsheet.pdf','','legacy/WRS-2224-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(266,9,'WRS-2224-Manual.pdf','','legacy/WRS-2224-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(267,8,'WSG-2300-cutsheet.pdf','','legacy/WSG-2300-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(268,8,'WSP-2718-cutsheet.pdf','','legacy/WSP-2718-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(269,8,'WSP-2718-D-cutsheet.pdf','','legacy/WSP-2718-D-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(270,8,'WTI-2332-FT-cutsheet.pdf','','legacy/WTI-2332-FT-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(271,9,'WTI-2332-FT-Manual.pdf','','legacy/WTI-2332-FT-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(272,8,'WTP-4408-cutsheet.pdf','','legacy/WTP-4408-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(273,9,'WTP-4408-Manual.pdf','','legacy/WTP-4408-Manual.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(274,8,'WTS-4181-cutsheet.pdf','','legacy/WTS-4181-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(275,8,'WIR-3110-cutsheet.pdf','','tools/WIR-3110-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(276,9,'WIR-3110-Manual-Dialog.pdf','','tools/WIR-3110-Manual-Dialog.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(278,6,'Full-2Way-brochure.pdf','','full_two_way/Full-2Way-brochure.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(279,8,'PWE-cutsheet2015-12-03.pdf','','dialog/PWE-cutsheet2015-12-03.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(280,8,'WRC-LCD9261-cutsheet.pdf','','dialog_room_controller/WRC-LCD9261-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(281,8,'WTP-1000-WTP-2000-TrakPak-cutsheet.pdf','','tools/WTP-1000-WTP-2000-TrakPak-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(282,8,'WPS-3711-cutsheet.pdf','','dialog/WPS-3711-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(283,8,'WPS-ExteriorDaylightSensors-CutSheet.pdf','','dialog/WPS-ExteriorDaylightSensors-CutSheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(284,9,'DialogSystemManual-WLC-3150-2015-07.pdf','','dialog/DialogSystemManual-WLC-3150-2015-07.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(285,8,'WR-4075-WR-4040-cutsheet.pdf','','dialog/WR-4075-WR-4040-cutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(286,8,'Diversa-WOS-PIR-24VacCutsheet.pdf','','diversa/Diversa-WOS-PIR-24VacCutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(287,8,'Diversa-WOS-PIR-120-277VacCutsheet.pdf','','diversa/Diversa-WOS-PIR-120-277VacCutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(288,8,'Diversa-WOS-PIR-347VacCutsheet.pdf','','diversa/Diversa-WOS-PIR-347VacCutsheet.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(289,6,'WRC-3160-Kits.dwg','','dialog_room_controller/WRC-3160-Kits.dwg','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(290,6,'WRC-3160-KitsBy Page.dwg','','dialog_room_controller/WRC-3160-KitsBy Page.dwg','','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(291,6,'Full-2Way_brochure_Fr.pdf','','full_two_way/160401_F2W_FR.pdf','','0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `literature_copy_gabrielle_14th_April_2016` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table literature_product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `literature_product`;

CREATE TABLE `literature_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `literature_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `literature_product_literature_id_index` (`literature_id`),
  KEY `literature_product_product_id_index` (`product_id`),
  CONSTRAINT `literature_product_literature_id_foreign` FOREIGN KEY (`literature_id`) REFERENCES `literature` (`id`) ON DELETE CASCADE,
  CONSTRAINT `literature_product_product_id_foreign_new` FOREIGN KEY (`product_id`) REFERENCES `products_new` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `literature_product` WRITE;
/*!40000 ALTER TABLE `literature_product` DISABLE KEYS */;

INSERT INTO `literature_product` (`id`, `literature_id`, `product_id`, `created_at`, `updated_at`)
VALUES
	(1,85,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(2,86,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(3,87,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(4,88,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(5,89,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(6,90,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(7,91,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(8,92,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(9,93,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(10,94,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(11,95,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(12,96,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(13,97,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(14,98,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(15,99,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(16,100,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(17,101,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(18,102,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(20,104,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(21,105,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(43,106,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(44,107,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(85,108,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(86,109,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(87,110,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(88,111,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(90,113,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(106,114,39,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(107,115,39,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(108,116,39,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(109,117,39,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(110,118,39,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(111,119,39,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(112,120,39,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(113,121,39,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(114,122,39,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(141,173,40,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(142,174,40,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(143,175,40,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(144,176,40,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(145,177,40,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(146,178,40,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(147,179,40,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(148,180,40,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(149,181,40,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(150,182,40,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(151,183,41,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(152,184,41,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(153,185,41,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(154,186,42,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(155,187,42,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(156,188,42,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(157,189,42,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(158,190,45,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(159,191,44,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(160,192,47,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(161,193,46,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(174,206,112,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(175,207,112,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(176,208,112,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(177,209,112,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(178,210,112,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(179,211,112,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(180,212,112,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(181,213,112,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(182,214,112,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(183,215,112,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(184,216,112,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(185,217,113,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(186,218,113,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(187,219,113,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(188,220,113,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(189,221,114,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(190,222,114,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(191,223,114,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(192,224,114,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(193,225,114,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(194,226,114,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(195,227,114,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(196,228,114,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(197,229,114,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(198,230,114,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(199,231,114,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(200,232,114,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(201,233,114,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(202,234,114,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(203,235,114,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(204,236,114,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(205,237,114,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(206,238,114,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(207,239,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(208,240,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(209,241,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(210,242,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(211,243,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(212,244,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(213,245,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(214,246,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(215,247,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(216,248,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(217,249,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(218,250,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(219,251,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(220,252,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(221,253,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(222,254,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(223,255,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(224,256,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(225,257,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(226,258,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(227,259,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(228,260,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(229,261,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(230,262,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(231,263,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(232,264,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(233,265,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(234,266,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(235,267,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(236,268,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(237,269,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(238,270,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(239,271,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(240,272,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(241,273,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(242,274,115,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(243,194,60,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(247,197,51,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(248,111,51,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(249,260,61,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(250,202,65,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(251,97,48,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(252,110,48,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(253,203,48,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(254,275,48,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(255,276,48,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(257,278,114,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(258,199,62,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(259,199,63,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(260,200,64,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(261,201,66,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(262,279,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(263,280,112,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(264,281,120,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(265,282,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(267,283,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(269,284,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(270,285,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(271,286,40,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(272,287,40,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(273,288,40,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(275,289,112,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(276,290,112,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(277,291,114,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(279,292,39,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(280,293,121,'2016-10-24 17:23:14','2016-10-24 17:23:14');

/*!40000 ALTER TABLE `literature_product` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mailinglist
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mailinglist`;

CREATE TABLE `mailinglist` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mailinglist` WRITE;
/*!40000 ALTER TABLE `mailinglist` DISABLE KEYS */;

INSERT INTO `mailinglist` (`id`, `name`, `title`, `email`, `company`)
VALUES
	(1,'Nick Franciosi','test','nfranciosi@gsandf.com','test'),
	(2,'Nick Franciosi','Mr.','nfranciosi@gsandf.com','Test Co.'),
	(3,'Norris Elston','Mr.','nfranciosi@gsandf.com','TontoCo'),
	(4,'Bill Foley','President','bfoley@creative-epicenter.com','Creative Epicenter');

/*!40000 ALTER TABLE `mailinglist` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`migration`, `batch`)
VALUES
	('2014_06_26_152523_create_literature_table',1),
	('2014_06_26_152538_create_doctype_table',1),
	('2014_06_26_152620_create_products_table',1),
	('2014_06_26_153114_update_foreign_key_on_literature_table',1),
	('2014_06_26_160235_create_literature_product_table',2),
	('2014_06_26_162615_create_certifications_table',3),
	('2014_06_26_163024_create_certification_product_table',4),
	('2014_06_26_164952_create_product_type_table',5),
	('2014_06_26_165138_add_product_type_id_to_products_table',6),
	('2014_05_30_183117_create_users_table',7),
	('2014_04_07_151339_create_product_sub_category_table',8);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table product_sub_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_sub_category`;

CREATE TABLE `product_sub_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `product_sub_category` WRITE;
/*!40000 ALTER TABLE `product_sub_category` DISABLE KEYS */;

INSERT INTO `product_sub_category` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'4\" Round Downlights','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(2,'6\" Square Downlights','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(3,'6\" Round High Lumen','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(4,'8\" Round High Lumen Downlights','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(5,'Space Player','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(6,'Frame Kit','0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `product_sub_category` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table product_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_type`;

CREATE TABLE `product_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `product_type` WRITE;
/*!40000 ALTER TABLE `product_type` DISABLE KEYS */;

INSERT INTO `product_type` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'Downlights','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(2,'Space Player','0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `product_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table product_type_new
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_type_new`;

CREATE TABLE `product_type_new` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `has_subcategory` int(1) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `product_type_new` WRITE;
/*!40000 ALTER TABLE `product_type_new` DISABLE KEYS */;

INSERT INTO `product_type_new` (`id`, `name`, `created_at`, `updated_at`, `has_subcategory`, `url`)
VALUES
	(1,'Dialog &ndash; Centralized Controls','0000-00-00 00:00:00','0000-00-00 00:00:00',0,'dialog'),
	(2,'Dialog Room Controller &ndash; Decentralized Controls','0000-00-00 00:00:00','0000-00-00 00:00:00',0,'dialog_room_controller'),
	(3,'LitePak &ndash; Standardized Controls','0000-00-00 00:00:00','0000-00-00 00:00:00',0,'litepak'),
	(4,'Diversa &ndash; Sensors &amp; Power Packs','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'diversa'),
	(5,'Wall Switch Stations','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'wall_switch_stations'),
	(6,'Tools & Integration Devices','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'tools_integration_devices'),
	(7,'Legacy','0000-00-00 00:00:00','0000-00-00 00:00:00',0,'legacy'),
	(9,'Full Two Way','0000-00-00 00:00:00','0000-00-00 00:00:00',0,'full-two-way'),
	(10,'Resource Library','2016-10-21 21:58:06','2016-10-21 21:58:06',0,'resource-library');

/*!40000 ALTER TABLE `product_type_new` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table product_type_new_copygabrielle_14th_April_2016
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_type_new_copygabrielle_14th_April_2016`;

CREATE TABLE `product_type_new_copygabrielle_14th_April_2016` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `has_subcategory` int(1) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `product_type_new_copygabrielle_14th_April_2016` WRITE;
/*!40000 ALTER TABLE `product_type_new_copygabrielle_14th_April_2016` DISABLE KEYS */;

INSERT INTO `product_type_new_copygabrielle_14th_April_2016` (`id`, `name`, `created_at`, `updated_at`, `has_subcategory`, `url`)
VALUES
	(1,'Dialog &ndash; Centralized Controls','0000-00-00 00:00:00','0000-00-00 00:00:00',0,'dialog'),
	(2,'Dialog Room Controller &ndash; Decentralized Controls','0000-00-00 00:00:00','0000-00-00 00:00:00',0,'dialog_room_controller'),
	(3,'LitePak &ndash; Standardized Controls','0000-00-00 00:00:00','0000-00-00 00:00:00',0,'litepak'),
	(4,'Diversa &ndash; Sensors &amp; Power Packs','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'diversa'),
	(5,'Wall Switch Stations','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'wall_switch_stations'),
	(6,'Tools & Integration Devices','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'tools_integration_devices'),
	(7,'Legacy','0000-00-00 00:00:00','0000-00-00 00:00:00',0,'legacy'),
	(9,'Full Two Way','0000-00-00 00:00:00','0000-00-00 00:00:00',0,'full_two_way'),
	(10,'Resource Library','2016-10-21 21:51:03','2016-10-21 21:51:03',NULL,'resource-library');

/*!40000 ALTER TABLE `product_type_new_copygabrielle_14th_April_2016` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `blurb` text COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hero` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `product_type_id` int(10) unsigned NOT NULL,
  `overview` blob NOT NULL,
  `product_sub_category_id` int(10) unsigned NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_product_type_id_index` (`product_type_id`),
  CONSTRAINT `products_product_type_id_foreign` FOREIGN KEY (`product_type_id`) REFERENCES `product_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;

INSERT INTO `products` (`id`, `name`, `description`, `blurb`, `thumbnail`, `hero`, `created_at`, `updated_at`, `product_type_id`, `overview`, `product_sub_category_id`, `url`)
VALUES
	(1,'SD4','The SD4 is a unique specification designed downlight. It balances ease of installation and component integration for a high efficiency LED downlight.','4&Prime; Round Specification Grade Downlight','sd4.png','SD4.jpg','0000-00-00 00:00:00','0000-00-00 00:00:00',1,X'3C6C693E3C7370616E3E4F70746963616C3C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E436869702D6F6E2D626F617264204C4544206D6F64756C652072656475636573206D756C7469706C6520736861646F77696E672076732E2064696F646520646576696365732E202053706865726963616C2070686F7370686F7220636F6174696E6720666F7220636F6C6F7220756E69666F726D697479206F76657220616E676C652E2033205343444D20636F6C6F7220636F6E73697374656E63792E3C2F6C693E0A20202020202020203C6C693E4D656469756D206F72205769646520646973747269627574696F6E732E3C2F6C693E0A20202020202020203C6C693E312C3030302C20312C3530302026616D703B20322C303030206E6F6D696E616C206C756D656E206C6967687420656E67696E65732E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A3C6C693E3C7370616E3E4D656368616E6963616C20616E6420546865726D616C3C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E556E6976657273616C2064657369676E20696E7374616C6C7320696E746F20626F7468206E657720636F6E737472756374696F6E2026616D703B2072656D6F64656C206170706C69636174696F6E73207468726F7567682061706572747572652066726F6D2062656C6F77206365696C696E672E20204F7074696F6E616C2044727977616C6C204672616D652D496E204B69742E3C2F6C693E0A20202020202020203C6C693E496E74656772616C20686F7573696E672026616D703B206F7074696320696E206F6E6520756E69742E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A2020200A3C6C693E3C7370616E3E456C656374726963616C3C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E447269766572207072696D617279207369646520656C656374726963616C20717569636B20636F6E6E6563747320666F72206561737920696E7374616C6C6174696F6E2E3C2F6C693E0A20202020202020203C6C693E54776F20C2BD265072696D653B2026616D703B2074776F20636F6D62696E6174696F6E20C2BD265072696D653B202D20C2BE265072696D653B206B6E6F636B6F757473206F6E204A2D626F782E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A202020200A3C6C693E3C7370616E3E436F6E74726F6C3C2F7370616E3E0A202020203C756C3E0A2020202020202020203C6C693E302D3130562064696D6D696E6720746F2031302520617661696C61626C652E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A2020200A3C6C693E3C7370616E3E43657274696669636174696F6E733C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E4353412064616D70206C6F636174696F6E204C697374656420746F2055532026616D703B2043616E616469616E20554C2031353938207374616E646172642E3C2F6C693E0A20202020202020203C6C693E454E455247592053544152207175616C69666965642E20204C69676874696E6746616374733C7375703E267265673B3C2F7375703E20726567697374657265642E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A202020200A3C6C693E3C7370616E3E57617272616E74793C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E466976652D79656172206C696D697465642077617272616E74792E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A202020200A',1,'SD4'),
	(2,'SD6S','The SD6S is a unique specification designed downlight. It balances ease of installation and component integration for a high efficiency LED downlight.','6&Prime; Square Specification Grade Downlight','sd6s.png','SD6S.jpg','0000-00-00 00:00:00','0000-00-00 00:00:00',1,X'3C6C693E3C7370616E3E4F70746963616C3C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E436869702D6F6E2D626F617264204C4544206D6F64756C652072656475636573206D756C7469706C6520736861646F77696E672076732E2064696F646520646576696365732E202053706865726963616C2070686F7370686F7220636F6174696E6720666F7220636F6C6F7220756E69666F726D697479206F76657220616E676C652E2033205343444D20636F6C6F7220636F6E73697374656E63792E3C2F6C693E0A20202020202020203C6C693E4D656469756D206F72205769646520646973747269627574696F6E732E3C2F6C693E0A20202020202020203C6C693E312C3030302C20312C3530302026616D703B20322C303030206E6F6D696E616C206C756D656E206C6967687420656E67696E65732E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A3C6C693E3C7370616E3E4D656368616E6963616C20616E6420546865726D616C3C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E556E6976657273616C2064657369676E20696E7374616C6C7320696E746F20626F7468206E657720636F6E737472756374696F6E2026616D703B2072656D6F64656C206170706C69636174696F6E73207468726F7567682061706572747572652066726F6D2062656C6F77206365696C696E672E20204F7074696F6E616C2044727977616C6C204672616D652D496E204B69742E3C2F6C693E0A20202020202020203C6C693E496E74656772616C20686F7573696E672026616D703B206F7074696320696E206F6E6520756E69742E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A2020200A3C6C693E3C7370616E3E456C656374726963616C3C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E447269766572207072696D617279207369646520656C656374726963616C20717569636B20636F6E6E6563747320666F72206561737920696E7374616C6C6174696F6E2E3C2F6C693E0A20202020202020203C6C693E54776F20C2BD265072696D653B2026616D703B2074776F20636F6D62696E6174696F6E20C2BD265072696D653B202D20C2BE265072696D653B206B6E6F636B6F757473206F6E204A2D626F782E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A202020200A3C6C693E3C7370616E3E436F6E74726F6C3C2F7370616E3E0A202020203C756C3E0A2020202020202020203C6C693E302D3130562064696D6D696E6720746F2031302520617661696C61626C652E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A2020200A3C6C693E3C7370616E3E43657274696669636174696F6E733C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E4353412064616D70206C6F636174696F6E204C697374656420746F2055532026616D703B2043616E616469616E20554C2031353938207374616E646172642E3C2F6C693E0A20202020202020203C6C693E454E455247592053544152207175616C69666965642E20204C69676874696E6746616374733C7375703E267265673B3C2F7375703E20726567697374657265642E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A202020200A3C6C693E3C7370616E3E57617272616E74793C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E466976652D79656172206C696D697465642077617272616E74792E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A202020200A',2,'SD6S'),
	(3,'SD6 High Lumen','The SD6 High Lumen  is a high output unique specification designed downlight. It balances ease of installation and component integration for a high efficiency high light output LED downlight.','6&Prime; Round High Lumen Specification Grade Downlight','sd6_high_lumen.png','SD6-5K.jpg','0000-00-00 00:00:00','0000-00-00 00:00:00',1,X'3C6C693E3C7370616E3E4F70746963616C3C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E436869702D6F6E2D626F617264204C4544206D6F64756C652072656475636573206D756C7469706C6520736861646F77696E672076732E2064696F646520646576696365732E202053706865726963616C2070686F7370686F7220636F6174696E6720666F7220636F6C6F7220756E69666F726D697479206F76657220616E676C652E2033205343444D20636F6C6F7220636F6E73697374656E63792E3C2F6C693E0A20202020202020203C6C693E4D656469756D206F72205769646520646973747269627574696F6E732E3C2F6C693E0A20202020202020203C6C693E332C3030302C20342C3030302026616D703B20352C303030206E6F6D696E616C206C756D656E206C6967687420656E67696E65732E3C2F6C693E0A20202020202020203C6C693E5265666C6563746F722072657461696E6564206279206469652063617374206C6F77657220686F7573696E6720666F722070686F746F6D657472696320636F6E73697374656E63792E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A3C6C693E3C7370616E3E4D656368616E6963616C20616E6420546865726D616C3C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E556E6976657273616C2064657369676E20696E7374616C6C7320696E746F20626F7468206E657720636F6E737472756374696F6E2026616D703B2072656D6F64656C206170706C69636174696F6E73207468726F7567682061706572747572652066726F6D2062656C6F77206365696C696E672E20204F7074696F6E616C2044727977616C6C204672616D652D496E204B69742E3C2F6C693E0A20202020202020203C6C693E496E74656772616C20686F7573696E672026616D703B206F7074696320696E206F6E6520756E69742E3C2F6C693E0A20202020202020203C6C693E446965206361737420616C756D696E756D20686561742073696E6B2026616D703B206F70746963616C20617373656D626C7920666F72206C6F6E672064696F6465206C6966652E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A2020200A3C6C693E3C7370616E3E456C656374726963616C3C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E447269766572207072696D617279207369646520656C656374726963616C20717569636B20636F6E6E6563747320666F72206561737920696E7374616C6C6174696F6E2E3C2F6C693E0A20202020202020203C6C693E54776F20C2BD265072696D653B2026616D703B2074776F20636F6D62696E6174696F6E20C2BD265072696D653B202D20C2BE265072696D653B206B6E6F636B6F757473206F6E204A2D626F782E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A202020200A3C6C693E3C7370616E3E436F6E74726F6C3C2F7370616E3E0A202020203C756C3E0A2020202020202020203C6C693E302D3130562064696D6D696E6720746F2031302520617661696C61626C652E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A2020200A3C6C693E3C7370616E3E43657274696669636174696F6E733C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E4353412064616D70206C6F636174696F6E204C697374656420746F2055532026616D703B2043616E616469616E20554C2031353938207374616E646172642E3C2F6C693E0A20202020202020203C6C693E454E455247592053544152207175616C69666965642E20204C69676874696E6746616374733C7375703E267265673B3C2F7375703E20726567697374657265642E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A202020200A3C6C693E3C7370616E3E57617272616E74793C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E466976652D79656172206C696D697465642077617272616E74792E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A202020200A',3,'SD6-high-lumen'),
	(4,'SD6','The SD6 is a unique specification designed downlight. It balances ease of installation and component integration for a high efficiency LED downlight.','6&Prime; Round Specification Grade Downlight','sd6.png','SD6.jpg','0000-00-00 00:00:00','0000-00-00 00:00:00',1,X'3C6C693E3C7370616E3E4F70746963616C3C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E436869702D6F6E2D626F617264204C4544206D6F64756C652072656475636573206D756C7469706C6520736861646F77696E672076732E2064696F646520646576696365732E202053706865726963616C2070686F7370686F7220636F6174696E6720666F7220636F6C6F7220756E69666F726D697479206F76657220616E676C652E2033205343444D20636F6C6F7220636F6E73697374656E63792E3C2F6C693E0A20202020202020203C6C693E4D656469756D206F72205769646520646973747269627574696F6E732E3C2F6C693E0A20202020202020203C6C693E312C3530302026616D703B20322C303030206E6F6D696E616C206C756D656E206C6967687420656E67696E65732E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A3C6C693E3C7370616E3E4D656368616E6963616C20616E6420546865726D616C3C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E556E6976657273616C2064657369676E20696E7374616C6C7320696E746F20626F7468206E657720636F6E737472756374696F6E2026616D703B2072656D6F64656C206170706C69636174696F6E73207468726F7567682061706572747572652066726F6D2062656C6F77206365696C696E672E20204F7074696F6E616C2044727977616C6C204672616D652D496E204B69742E3C2F6C693E0A20202020202020203C6C693E496E74656772616C20686F7573696E672026616D703B206F7074696320696E206F6E6520756E69742E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A2020200A3C6C693E3C7370616E3E456C656374726963616C3C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E447269766572207072696D617279207369646520656C656374726963616C20717569636B20636F6E6E6563747320666F72206561737920696E7374616C6C6174696F6E2E3C2F6C693E0A20202020202020203C6C693E54776F20C2BD265072696D653B2026616D703B2074776F20636F6D62696E6174696F6E20C2BD265072696D653B202D20C2BE265072696D653B206B6E6F636B6F757473206F6E204A2D626F782E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A202020200A3C6C693E3C7370616E3E436F6E74726F6C3C2F7370616E3E0A202020203C756C3E0A2020202020202020203C6C693E302D3130562064696D6D696E6720746F2031302520617661696C61626C652E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A2020200A3C6C693E3C7370616E3E43657274696669636174696F6E733C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E4353412064616D70206C6F636174696F6E204C697374656420746F2055532026616D703B2043616E616469616E20554C2031353938207374616E646172642E3C2F6C693E0A20202020202020203C6C693E454E455247592053544152207175616C69666965642E20204C69676874696E6746616374733C7375703E267265673B3C2F7375703E20726567697374657265642E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A202020200A3C6C693E3C7370616E3E57617272616E74793C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E466976652D79656172206C696D697465642077617272616E74792E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A202020200A',2,'SD6'),
	(5,'SD8','The SD8 is a unique specification designed high output downlight. It balances ease of installation and component integration for a high efficiency LED downlight.','8&Prime; Round High Lumen Specification Grade Downlight','sd8.png','SD8.jpg','0000-00-00 00:00:00','0000-00-00 00:00:00',1,X'3C6C693E3C7370616E3E4F70746963616C3C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E436869702D6F6E2D626F617264204C4544206D6F64756C652072656475636573206D756C7469706C6520736861646F77696E672076732E2064696F646520646576696365732E202053706865726963616C2070686F7370686F7220636F6174696E6720666F7220636F6C6F7220756E69666F726D697479206F76657220616E676C652E2033205343444D20636F6C6F7220636F6E73697374656E63792E3C2F6C693E0A20202020202020203C6C693E3135266465673B204E6172726F772C203235266465673B204D656469756D206F72203435266465673B205769646520646973747269627574696F6E732E3C2F6C693E0A20202020202020203C6C693E352C3030302C20372C3530302C2031302C3030302026616D703B2031342C303030206E6F6D696E616C206C756D656E206C6967687420656E67696E65732E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A3C6C693E3C7370616E3E4D656368616E6963616C20616E6420546865726D616C3C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E457874727564656420616C756D696E756D20686561742073696E6B207769746820696E74656772616C206865617420706970657320666F72206C6F6E672064696F6465206C6966652E3C2F6C693E0A20202020202020203C6C693E4472697665722026616D703B20656C656374726963616C20636F6E6E656374696F6E732063616E2062652061636365737365642066726F6D2062656C6F77206365696C696E6720776974686F757420746F6F6C732E3C2F6C693E0A20202020202020203C6C693E4261722068616E67657220627261636B6574732061646A7573742033265072696D653B20766572746963616C6C792026616D703B206163636F6D6D6F6461746520337264207061727479206261722068616E6765727320C2BD265072696D653B20454D542C206F72206C617468696E67206368616E6E656C20286279206F7468657273292E3C2F6C693E0A20202020202020203C6C693E48656176792067617567652043525320737465656C20686F7573696E67207769746820736174696E20626C61636B207061696E7465642D61667465722D6661627269636174696F6E2066696E6973682E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A2020200A3C6C693E3C7370616E3E456C656374726963616C3C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E447269766572207072696D617279207369646520656C656374726963616C20717569636B20636F6E6E6563747320666F72206561737920696E7374616C6C6174696F6E2E3C2F6C693E0A20202020202020203C6C693E54776F20C2BD265072696D653B2026616D703B2074776F20636F6D62696E6174696F6E20C2BD265072696D653B202D20C2BE265072696D653B206B6E6F636B6F757473206F6E204A2D626F782E3C2F6C693E0A20202020202020203C6C693E417070726F76656420666F72203820283420696E2F34206F757429202331322041574720636F6E647563746F727320726174656420666F72203930266465673B43207468727520776972696E672E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A202020200A3C6C693E3C7370616E3E436F6E74726F6C3C2F7370616E3E0A202020203C756C3E0A2020202020202020203C6C693E302D3130562064696D6D696E6720746F2031302520617661696C61626C652E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A2020200A3C6C693E3C7370616E3E43657274696669636174696F6E733C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E4353412064616D70206C6F636174696F6E204C697374656420746F2055532026616D703B2043616E616469616E20554C2031353938207374616E646172642E3C2F6C693E0A20202020202020203C6C693E454E455247592053544152207175616C69666965642E20204C69676874696E6746616374733C7375703E267265673B3C2F7375703E20726567697374657265642E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A202020200A3C6C693E3C7370616E3E57617272616E74793C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E466976652D79656172206C696D697465642077617272616E74792E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A',4,'SD8'),
	(6,'Space Player','Lighting has gained new meaning and power. Now with your creativity, light can express itself in an unprecedented way. Space Player is your dynamic marketing lighting necessity.','Lighting has gained new meaning and power. Now with your creativity, light can express itself in an unprecedented way. Space Player is your dynamic marketing lighting necessity.','spaceplayer.png','SpacePlayer.jpg','0000-00-00 00:00:00','0000-00-00 00:00:00',2,X'3C6C693E3C7370616E3E4372656174652064796E616D6963206D6F76696E672070726F647563742073706F746C696768747320616E642070726F6A6563742070726F6475637420696E666F726D6174696F6E207461626C65733C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E4C69676874696E6720616E642070726F6A656374696F6E20636F6D62696E6520696E746F206F6E6520756E69743C2F6C693E0A20202020202020203C6C693E3344206D617070696E672070726F6A656374696F6E206162696C6974793C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A3C6C693E3C7370616E3E517569636B20616E64206561737920746F207570646174652077697468207374616E646172642033726420706172747920736F667477617265202841646F62652043532C206574632E293C2F7370616E3E3C2F6C693E0A3C6C693E3C7370616E3E466C657869626C65206170706C69636174696F6E20757365202872657461696C2C2072657374617572616E74732C206D757365756D732C206578686962697420686F757365732C206574632E29E28094746865206170706C69636174696F6E732061726520656E646C6573733C2F7370616E3E3C2F6C693E0A3C6C693E3C7370616E3E436F6E74726F6C6C61626C652076696120416E64726F696420616E6420694F5320646576696365732C20616C6C6F77696E6720746865207573657220746F20636F6E6E65637420636F6E74656E74206469726563746C7920746F207468652070726F6A6563746F723C2F7370616E3E3C2F6C693E0A3C6C693E3C7370616E3E556E69742053706563696669636174696F6E733C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E436F6D62696E657320746865206C6F6F6B206F66206120747261646974696F6E616C20747261636B206669787475726520746F20666974207365616D6C6573736C7920696E746F206869676820656E64206170706C69636174696F6E732E3C2F6C693E0A20202020202020203C6C693E312C303030204C756D656E733C2F6C693E0A20202020202020203C6C693E4C617365722044696F6465206C6967687420736F75726365207768696368206D65616E73207468652070726F64756374206973206D61696E74656E616E6365206672656520706C757320656E7375726573206F7665722032302C30303020686F757273206F6620757361626C65206C6966653C2F6C693E0A20202020202020203C6C693E5265736F6C7574696F6E20E2809320312C3033392C36383020706978656C73202831363A31302057584741206571756976616C656E74293C2F6C693E0A20202020202020203C6C693E456C65637472696320466F63757320616E6420456C656374726963205A6F6F6D3C2F6C693E0A20202020202020203C6C693E52652D706C617920E280932048444D492C20576972656C6573732C20534420436172643C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A3C6C693E3C7370616E3E506C656173652076696577206F7572203C6120687265663D22687474703A2F2F777777322E70616E61736F6E69632E62697A2F65732F6C69676874696E672F676C6F62616C2F7370616365706C617965722F223E696E7465726E6174696F6E616C20706167653C2F613E3C2F7370616E3E3C2F6C693E',5,'space-player');

/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table products_new
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_new`;

CREATE TABLE `products_new` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_additional` blob,
  `blurb` text COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'sd4.png',
  `hero` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SD4.jpg',
  `secondary` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_type_id` int(10) unsigned NOT NULL,
  `overview` blob,
  `details` blob,
  `product_sub_category_id` int(10) unsigned DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `only_resources` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_product_type_id_index` (`product_type_id`),
  CONSTRAINT `products_new_product_type_id_foreign` FOREIGN KEY (`product_type_id`) REFERENCES `product_type_new` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `products_new` WRITE;
/*!40000 ALTER TABLE `products_new` DISABLE KEYS */;

INSERT INTO `products_new` (`id`, `name`, `description`, `description_additional`, `blurb`, `thumbnail`, `hero`, `secondary`, `product_type_id`, `overview`, `details`, `product_sub_category_id`, `url`, `only_resources`)
VALUES
	(39,'WOR Ceiling Sensors','Line and Low Voltage Recessed Ceiling Occupany Sensors',NULL,'Diversa Sensors are highly configurable Dual Technology and PIR only occupancy/vacancy sensors that provide load control based on room occupancy.  Diversa sensors enable locations to meet AHSRAE 90.1 and California Title 24 requirements.','WOR.jpg','WOR.jpg','WOR.jpg',4,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E4475616C20546563686E6F6C6F677920616E64205049522073656E736F72733C2F6C693E0A20202020202020203C6C693E3132302F3237375661632C2033343756616320616E64204C6F7720566F6C74616765203234566163206D6F64656C733C2F6C693E0A20202020202020203C6C693E4C6F772050726F66696C652044657369676E20666F722061707065616C696E6720696E7374616C6C6174696F6E3C2F6C693E0A20202020202020203C6C693E4F6E2D626F6172642073657474696E6720666F722074696D652D6F75742064656C617920616E64206461796C69676874206C6576656C20286461796C696768742073656E736F7273206D6F64656C73293C2F6C693E0A20202020202020203C6C693E416476616E636520636F6E66696775726174696F6E206F66204475616C20546563686E6F6C6F67792053656E736F727320776974682048616E6468656C6420496E6672617265642053657474696E6720556E69743C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E44697665727361205265636573736564204365696C696E672053656E736F727320696E2050495220287061737369766520696E66726172656429206F72204475616C20546563686E6F6C6F6779202850495220616E642070726F7072696574617279204144492D566F69636529206D6F64656C732070726F76696465206F63637570616E637920646574656374696F6E206F766572206C617267652061726561732E3C2F6C693E0A093C6C693E44697665727361204475616C20546563686E6F6C6F67792073656E736F72732061726520636C617373206C656164696E67207370656369666961626C652073656E736F72732074686174206F7574706572666F726D2074686520636F6D7065746974696F6E206279207265647563696E672066616C73652074726967676572732064756520746F20485641432C206D757369632C20766962726174696F6E2C207472616666696320616E64207265647563696E6720706F77657220636F6E73756D7074696F6E206279207573696E672061207061737369766520736F756E6420646574656374696F6E20746563686E6F6C6F677920726174686572207468616E20616E2061637469766520756C747261736F6E696320746563686E6F6C6F67792E20436F6D62696E696E672050495220616E64204144492D566F69636520746563686E6F6C6F677920616C6C6F7773207468652073656E736F7220746F206D61696E7461696E206F63637570616E6379207468726F7567682069747320766F69636520646574656374696F6E20746563686E6F6C6F677920696E206172656173207468617420646F206E6F742070726F7669646520646972656374206C696E65206F6620736967687420746F206120706572736F6E2E3C2F6C693E0A093C6C693E44697665727361205049522073656E736F72732066696C6C20746865206E65656420666F7220504952206F6E6C79207370656369666965642070726F6A65637473206F722070726F76696465206120636F6D6D65726369616C2067726164652C206561737920746F207573652F696E7374616C6C2073656E736F7220666F7220696E74726F647563746F7279207573616765206F66206C6967687420636F6E74726F6C7320616E6420776F726B2077656C6C20696E2061726561732074686174206861766520646972656374206C696E65206F662073696768742066726F6D2073656E736F7220746F20706572736F6E2E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A3C6C693E3C7370616E3E416476616E74616765733C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E43616E20626520636F6E6669677572656420666F7220566163616E6379206D6F646520286D616E75616C204F4E2F4175746F204F4646293C2F6C693E0A20202020202020203C6C693E5374616E646172642C20457874656E646564206F72204869676820426179206C656E73657320666F72206F7074696D616C206172656120636F7665726167653C2F6C693E0A20202020202020203C6C693E4D6F756E747320666C7573682077697468206365696C696E6720696E746F2061207374616E64617264206F637461676F6E20626F783C2F6C693E0A20202020202020203C6C693E54696C74696E67206C656E7320646972656374732073656E736F7220746F206F7220617761792066726F6D20706172746963756C6172206172656173206F7220666F72206D6F756E74696E67206F6E20736C6F706564206365696C696E67733C2F6C693E0A20202020202020203C6C693E4F7074696F6E7320666F72206461796C696768742073656E736F722C20302D3130762064696D6D696E672C20617578696C696172792072656C61793C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',NULL,'WOR',NULL),
	(40,'WOS Wall Switch Sensors','Line and Low Voltage Wall Switch Occupany Sensors',NULL,'Diversa Sensors are highly configurable Dual Technology and PIR only occupancy/vacancy sensors that provide load control based on room occupancy.  Diversa sensors enable locations to meet AHSRAE 90.1 and California Title 24 requirements.','WOS.jpg','WOS.jpg','WOS.jpg',4,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E4475616C20546563686E6F6C6F677920616E64205049522073656E736F72733C2F6C693E0A20202020202020203C6C693E3132302F3237375661632C2033343756616320616E64204C6F7720566F6C74616765203234566163206D6F64656C733C2F6C693E0A20202020202020203C6C693E4D6F6465726E2064657369676E20666F722061707065616C696E6720696E7374616C6C6174696F6E3C2F6C693E0A20202020202020203C6C693E4F6E2D626F6172642073657474696E6720666F722074696D652D6F75742064656C617920616E64206461796C69676874206C6576656C20286461796C696768742073656E736F7273206D6F64656C73293C2F6C693E0A20202020202020203C6C693E416476616E636520636F6E66696775726174696F6E206F66204475616C20546563686E6F6C6F67792053656E736F727320776974682048616E6468656C6420496E6672617265642053657474696E6720556E69743C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E446976657273612057616C6C205377697463682053656E736F727320696E2050495220287061737369766520696E66726172656429206F72204475616C20546563686E6F6C6F6779202850495220616E642070726F7072696574617279204144492D566F69636529206D6F64656C732063616E206265207573656420696E20656E636C6F73656420726F6F6D73207375636820617320636C617373726F6F6D732C2072657374726F6F6D7320616E64206F666669636573207768656E206D616E75616C6C79207475726E696E67206F6E206F72206F66662061206C69676874206D6179206265656E206E65656465642E3C2F6C693E0A093C6C693E44697665727361204475616C20546563686E6F6C6F67792073656E736F72732061726520636C617373206C656164696E67207370656369666961626C652073656E736F72732074686174206F7574706572666F726D2074686520636F6D7065746974696F6E206279207265647563696E672066616C73652074726967676572732064756520746F20485641432C206D757369632C20766962726174696F6E2C207472616666696320616E64207265647563696E6720706F77657220636F6E73756D7074696F6E206279207573696E672061207061737369766520736F756E6420646574656374696F6E20746563686E6F6C6F677920726174686572207468616E20616E2061637469766520756C747261736F6E696320746563686E6F6C6F67792E20436F6D62696E696E672050495220616E64204144492D566F69636520746563686E6F6C6F677920616C6C6F7773207468652073656E736F7220746F206D61696E7461696E206F63637570616E6379207468726F7567682069747320766F69636520646574656374696F6E20746563686E6F6C6F677920696E206172656173207468617420646F206E6F742070726F7669646520646972656374206C696E65206F6620736967687420746F206120706572736F6E2E3C2F6C693E0A093C6C693E44697665727361205049522073656E736F72732066696C6C20746865206E65656420666F7220504952206F6E6C79207370656369666965642070726F6A65637473206F722070726F76696465206120636F6D6D65726369616C2067726164652C206561737920746F207573652F696E7374616C6C2073656E736F7220666F7220696E74726F647563746F7279207573616765206F66206C6967687420636F6E74726F6C7320616E6420776F726B2077656C6C20696E2061726561732074686174206861766520646972656374206C696E65206F662073696768742066726F6D2073656E736F7220746F20706572736F6E2E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A3C6C693E3C7370616E3E416476616E74616765733C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E43616E20626520636F6E6669677572656420666F7220566163616E6379206D6F646520286D616E75616C204F4E2F4175746F204F4646293C2F6C693E0A20202020202020203C6C693E57616C6C2073776974636820666F72206D616E75616C20636F6E74726F6C3C2F6C693E0A20202020202020203C6C693E313830266465673B20646574656374696F6E202D206D6F756E747320666C75736820746F207468652077616C6C20696E2061207374616E646172642067616E6720626F783C2F6C693E0A20202020202020203C6C693E4F7074696F6E7320666F72206461796C696768742073656E736F722C20302D3130762064696D6D696E672C20617578696C696172792072656C61793C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',NULL,'WOS',NULL),
	(41,'WOW Corner/Hallway Sensors','Line and Low Voltage Corner/Hallway Occupancy Sensors',NULL,'Diversa Sensors are highly configurable Dual Technology and PIR only occupancy/vacancy sensors that provide load control based on room occupancy.  Diversa sensors enable locations to meet AHSRAE 90.1 and California Title 24 requirements.','WOW.jpg','WOW.jpg','WOW.jpg',4,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E4475616C20546563686E6F6C6F67792053656E736F727320666F722061747269756D732C206C6F62626965732C2068616C6C77617973206F7220737461697277656C6C733C2F6C693E0A20202020202020203C6C693E3132302F3237375661632C2033343756616320616E64204C6F7720566F6C74616765203234566163206D6F64656C733C2F6C693E0A20202020202020203C6C693E4D6F6465726E2064657369676E20666F722061707065616C696E6720696E7374616C6C6174696F6E3C2F6C693E0A20202020202020203C6C693E4F6E2D626F6172642073657474696E6720666F722074696D652D6F75742064656C617920616E64206461796C69676874206C6576656C20286461796C696768742073656E736F7273206D6F64656C73293C2F6C693E0A20202020202020203C6C693E416476616E636520636F6E66696775726174696F6E206F66204475616C20546563686E6F6C6F67792053656E736F727320776974682048616E6468656C6420496E6672617265642053657474696E6720556E69743C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E4469766572736120436F726E6572204D6F756E742053656E736F72732077697468204475616C20546563686E6F6C6F67792073656E73696E6720666F722061747269756D732C206C6F62626965732C2068616C6C77617973206F7220737461697277656C6C73206665617475726520612062616C6C20616E6420736F636B65742064657369676E20746F2070726F7669646520666C65786962696C69747920696E206D6F756E74696E67207468652073656E736F7220746F2077616C6C732C206365696C696E6773206F7220736C6F706564206365696C696E677320666F72206F7074696D697A696E672073656E736F7220706F736974696F6E2E3C2F6C693E0A093C6C693E44697665727361204475616C20546563686E6F6C6F67792073656E736F72732061726520636C617373206C656164696E67207370656369666961626C652073656E736F72732074686174206F7574706572666F726D2074686520636F6D7065746974696F6E206279207265647563696E672066616C73652074726967676572732064756520746F20485641432C206D757369632C20766962726174696F6E2C207472616666696320616E64207265647563696E6720706F77657220636F6E73756D7074696F6E206279207573696E672061207061737369766520736F756E6420646574656374696F6E20746563686E6F6C6F677920726174686572207468616E20616E2061637469766520756C747261736F6E696320746563686E6F6C6F67792E20436F6D62696E696E672050495220616E64204144492D566F69636520746563686E6F6C6F677920616C6C6F7773207468652073656E736F7220746F206D61696E7461696E206F63637570616E6379207468726F7567682069747320766F69636520646574656374696F6E20746563686E6F6C6F677920696E206172656173207468617420646F206E6F742070726F7669646520646972656374206C696E65206F6620736967687420746F206120706572736F6E2E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A3C6C693E3C7370616E3E416476616E74616765733C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E43616E20626520636F6E6669677572656420666F7220566163616E6379206D6F646520286D616E75616C204F4E2F4175746F204F4646293C2F6C693E0A20202020202020203C6C693E53776976656C206D6F756E74696E67206865616420616C6C6F777320666F7220696E7374616C6C6174696F6E20696E20612076617269657479206F66206C6F636174696F6E733C2F6C693E0A20202020202020203C6C693E436F726E6572206F72204C617267652061726561206C656E73657320666F7220737065636966696320636F766572616765207061747465726E733C2F6C693E0A20202020202020203C6C693E4F7074696F6E7320666F72206461796C696768742073656E736F722C20302D3130762064696D6D696E672C20617578696C696172792072656C61793C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',NULL,'WOW',NULL),
	(42,'WP-PP Power Packs','Converts line voltage to low voltage to power Diversa low voltage occupancy sensors',NULL,'Diversa Power Packs provide low voltge power to Diversa low voltage occupancy sensors.','WP-PP.jpg','WP-PP.jpg','WP-PP.jpg',4,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E436F6D6D697373696F6E696E6720746F6F6C20666F722044697665727361204F63637570616E63792053656E736F727320616E64204469616C6F67204F63637570616E63792053656E736F72732C204461796C696768742053656E736F722C20616E642057616C6C2053776974636865732053746174696F6E733C2F6C693E0A20202020202020203C6C693E53657420636F6E66696775726174696F6E732C207365742075702061646472657373657320616E642067726F7570732C2061636365737320616476616E636564206D656E75733C2F6C693E0A20202020202020203C6C693E496E667261726564207369676E616C20726561647320616E6420777269746573206465766963652073657474696E67733C2F6C693E0A20202020202020203C6C693E557365732032204141206261747465726965733C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E546865205749522D3331313020496E6672617265642053657474696E6720556E697420616C6C6F777320666F72206465636B206C6576656C20636F6D6D697373696F6E696E67206F662073656E736F727320616E642073776974636865732E205468652068616E6468656C642064657669636520697320696E7475697469766520746F2075736520616E6420616C6C6F777320666F7220616476616E6365642064657669636520636F6E66696775726174696F6E2E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A3C6C693E3C7370616E3E416476616E74616765733C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E506F696E7420616E6420707265737320746F20726561642F777269746520746F20646576696365733C2F6C693E0A20202020202020203C6C693E546F756368206469616C20616C6C6F777320666F72206F6E652068616E646564206E617669676174696F6E20616E642070726F6772616D6D696E673C2F6C693E0A20202020202020203C6C693E52656475636573206C61646465722074696D6520647572696E6720696E7374616C6C6174696F6E20616E6420636F6D6D697373696F6E696E673C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',NULL,'WP_PP',NULL),
	(44,'8700 Series','Lighting Control¬Ç via relay-direct functionality',X'3C756C3E0A202020203C6C693E312C20322C20332C203420427574746F6E205377697463682053746174696F6E733C2F6C693E0A202020203C6C693E44696D6D6572204F4E2F4F4646205377697463682053746174696F6E3C2F6C693E0A3C2F756C3E','8700 Series Wall Stations are used in small to medium sized applications that use Douglas Lighting Controls LitePak, small or satellite relay panels and where straightforward lighting control functionality is required','8700_series.jpg','8700_series.jpg','8700_series.jpg',5,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E4669747320696E746F206120312D67616E6720626F783C2F6C693E0A20202020202020203C6C693E53776974636865732063616E2062652067726F7570656420746F676574686572206372656174696E672063656E7472616C697A65642077616C6C2073746174696F6E3C2F6C693E0A20202020202020203C6C693E4E6574776F726B20636F6E6E656374696F6E2077697468204469616C6F6720322D77697265202831382F32292064617461206C696E653C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E446F75676C6173204C69676874696E6720436F6E74726F6C73203837303020736572696573206C696768742073776974636865732070726F7669646520636F6E74726F6C206F76657220666163696C697479206C69676874696E6720766961206D6F6D656E746172792072656C61792D6469726563742066756E6374696F6E616C6974792E20383730302073657269657320737769746368657320617265207573656420696E20736D616C6C20746F206D656469756D2073697A6564206170706C69636174696F6E7320746861742075736520446F75676C6173204C69676874696E6720436F6E74726F6C73204C69746550616B2C20736D616C6C206F7220736174656C6C6974652072656C61792070616E656C7320616E6420776865726520737472616967687420666F7277617264206C69676874696E6720636F6E74726F6C2066756E6374696F6E616C6974792069732072657175697265642E20383730302073776974636865732061726520616C736F20656173696C7920636F6E6669677572656420696E746F20612073797374656D207468617420757365732044697665727361206C6F7720766F6C74616765206F63637570616E63792073656E736F72732E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',NULL,'8700_series',NULL),
	(45,'3500 Series','Lighting Control¬Ç via digital functionality',X'3C756C3E0A202020203C6C693E312C20322C20332C20342026616D703B20382D627574746F6E2073776974636865733C2F6C693E0A202020203C6C693E44696D6D6572204F4E2F4F4646205377697463682053746174696F6E3C2F6C693E0A202020203C6C693E4B657920616374697661746564207377697463683C2F6C693E0A3C2F756C3E','3500 Series Wall Stations are used in Dialog digitally controlled systems to provide a clean and thoughtful user interface.','3500_series.jpg','3500_series.jpg','3500_series.jpg',5,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E4469676974616C2C2070726F6772616D6D61626C652073776974636865733C2F6C693E0A20202020202020203C6C693E456173696C7920636F6E6E6563747320746F206E6F6E2D706F6C6172697A6564204469616C6F67206E6574776F726B20776974682031382F3220776972653C2F6C693E0A20202020202020203C6C693E4C45442073746174757320696E64696361746F72733C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E4469616C6F6720333530302073657269657320737769746368657320617265206469676974616C6C7920636F6E74726F6C6C656420616E642070726F6772616D6D61626C6520666F7220636F6D6D65726369616C20737769746368696E6720726571756972656D656E74732E2053776974636865732063616E2062652067726F7570656420746F20666F726D20636F6D706163742077616C6C207377697463682073746174696F6E732E204120636F6D706C6574652072616E6765206F6620737769746368206661636520706C617465732061726520617661696C61626C6520696E2073637265776C65737320776869746520706C6173746963206F7220737461696E6C65737320737465656C20287769746820736372657773292E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',NULL,'3500_series',NULL),
	(46,'WN-7700 Switch Enclosures','Lockable Switch Enclosures to Prevent Tampering',X'3C756C3E0A202020203C6C693E466C757368206F722073757266616365206D6F756E7420696E7374616C6C6174696F6E3C2F6C693E0A202020203C6C693E536964652068696E6765206F7220746F702068696E676520646F6F72733C2F6C693E0A202020203C6C693E4B6579204C6F636B3C2F6C693E0A3C2F756C3E','WN-7700 Series lockable steel switch enclosures are well designed to prevent damage, tampering and abuse of switches. Switch enclosures are typically used in high school gymnasiums or public and high traffic areas. Enclosures consist of a back box, mounting plate, and cover plate with hinged door.','WN-7700.jpg','WN-7700.jpg','WN-7700.jpg',5,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E576964652072616E6765206F662073697A65733C2F6C693E0A20202020202020203C6C693E537465656C20636F6E737472756374696F6E3C2F6C693E0A20202020202020203C6C693E4C6F636B696E6720646F6F723C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E574E2D3737303020536572696573206C6F636B61626C6520737465656C2073776974636820656E636C6F7375726573206172652077656C6C2064657369676E656420746F2070726576656E742064616D6167652C2074616D706572696E6720616E64206162757365206F662073776974636865732E2053776974636820656E636C6F737572657320617265207479706963616C6C79207573656420696E2068696768207363686F6F6C2067796D6E617369756D73206F72207075626C696320616E64206869676820747261666669632061726561732E20456E636C6F737572657320636F6E73697374206F662061206261636B20626F782C206D6F756E74696E6720706C6174652C20616E6420636F76657220706C61746520776974682068696E67656420646F6F722E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',NULL,'WN-7700',NULL),
	(47,'Face Plates','Face Plates for 8700 and 3500 Series Switches',X'3C756C3E0A202020203C6C693E312D67616E6720746F20352D67616E67206661636520706C617465733C2F6C693E0A202020203C6C693E4D6F6465726E2077686974652073656D692D676C6F73732C2073637265776C6573732064657369676E3C2F6C693E0A202020203C6C693E537461696E6C65737320537465656C2073637265772064657369676E3C2F6C693E0A3C2F756C3E','White, semi-gloss screwless face plates provide a pleasing cover for Dialog and 8700 series switches. Stainless steel face plates are also available and are installed with traditional mounting screws.','face_plates.jpg','face_plates.jpg','face_plates.jpg',5,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E312D67616E6720746F20352D67616E67206661636520706C617465733C2F6C693E0A20202020202020203C6C693E4D6F6465726E2077686974652073656D692D676C6F73732C2073637265776C6573732064657369676E3C2F6C693E0A20202020202020203C6C693E537461696E6C65737320537465656C2073637265772064657369676E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E57686974652C2073656D692D676C6F73732073637265776C657373206661636520706C617465732070726F76696465206120706C656173696E6720636F76657220666F72204469616C6F6720616E642038373030207365726965732073776974636865732E20537461696E6C65737320737465656C206661636520706C617465732061726520616C736F20617661696C61626C6520616E642061726520696E7374616C6C6564207769746820747261646974696F6E616C206D6F756E74696E67207363726577732E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',NULL,'face_plates',NULL),
	(48,'WIR-3110','Handheld Infrared Setting Unit',X'412068616E6468656C6420496E6672617265642053657474696E6720556E6974207573656420746F207365742074686520706172616D6574657273206F6620737769746368657320616E642073656E736F72732E','The WIR-3110 Infrared Setting Unit allows for deck level commissioning of sensors and switches. The handheld device is intuitive to use and allows for advanced device configuration.','WIR-3110.jpg','WIR-3110.jpg',NULL,6,'',NULL,NULL,'WIR-3110',NULL),
	(51,'WNG-3131','BACnet Gateway',X'416E2049502047617465776179207573656420746F20696E636F72706F72617465206120746865204469616C6F672053797374656D20696E746F2061204D616E6167656D656E7420436F6E74726F6C204E6574776F726B20746861742075736573204241436E657420746563686E6F6C6F67792E','Th WNG-3131 is used to incorporate a Dialog Lighting Control System into a Management Control Network that uses BACnet technology','WNG-3131.jpg','WNG-3131.jpg',NULL,6,'',NULL,NULL,'WNG-3131',NULL),
	(58,'Dialog &mdash; Our Centralized Lighting Control System','Dialog is a digital, addressable, programmable lighting control system for entire floors, buildings and large multi-building applications.',NULL,'Dialog is an advanced digital lighting control system connecting and controlling lights through relays, occupancy sensors, photo sensors, and switch stations. Dialog enables your site to meet AHSRAE 90.1 and California Title 24 requirements.','sd4.png','dialog.jpg','dialog.jpg',1,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E436F6D706C657465206C69676874696E6720636F6E74726F6C2073797374656D20666163746F727920636F6E666967757265642C2070726F6772616D6D65642C20616E642074657374656420666F7220656163682070726F6A6563743C2F6C693E0A20202020202020203C6C693E496E636C756465732072656C61792070616E656C732C20636F6E74726F6C6C6572732C206F63637570616E63792F766163616E63792073656E736F72732C206461796C696768742073656E736F72732C20616E642077616C6C207377697463682073746174696F6E733C2F6C693E0A20202020202020203C6C693E322F3138415747206E6574776F726B20636F6E6E656374696F6E20666F72206561737920616E642072656C6961626C65206461746120616E6420706F776572207472616E736D697373696F6E3C2F6C693E0A20202020202020203C6C693E496E746567726174656420746F7563682D73637265656E206F72207765622062726F7773657220696E7465726661636520666F72206F6E2D73697465206F722072656D6F74652070726F6772616D6D696E67206368616E676573203C2F6C693E0A20202020202020203C6C693E695061642067726170686963616C20636F6E74726F6C20696E7465726661636520666F722072656D6F74652073797374656D20636F6E74726F6C3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A',X'3C6C693E0A093C756C3E0A09093C6C693E204469616C6F6720697320616E20616476616E636564206469676974616C206C69676874696E6720636F6E74726F6C2073797374656D20636F6E6E656374696E6720616E6420636F6E74726F6C6C696E67206C6967687473207468726F7567682072656C6179732C206F63637570616E63792073656E736F72732C2070686F746F2073656E736F72732C2062616C6C617374732C20616E64207377697463682073746174696F6E732E204469616C6F6720697320612063656E7472616C697A656420636F6E74726F6C6C65722074686174206D616E6167657320666163696C697479206C69676874696E6720726571756972656D656E747320676C6F62616C6C792C206F7220627920617265612C206F7220627920726F6F6D2E2054686520666C65786962696C69747920616E642074727565207363616C6162696C697479206F66204469616C6F6720656173696C7920616C6C6F777320697420746F206265207573656420696E20612076617269657479206F66206170706C69636174696F6E732066726F6D20736D616C6C20636F6D6D65726369616C206275696C64696E677320746F206C6172676520666163696C69746965732073756368206173206F666669636520746F776572732C207363686F6F6C732C20616972706F72747320616E64207374616469756D732E3C2F6C693E0A093C2F756C3E0A3C2F6C693E0A0A3C6C693E3C7370616E3E416476616E74616765733C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E5363686564756C696E673C2F6C693E0A20202020202020203C6C693E4F4E2F4F464620537769746368696E673C2F6C693E0A20202020202020203C6C693E44696D6D696E672F4D756C74692D6C6576656C20436F6E74726F6C3C2F6C693E0A20202020202020203C6C693E4869676820456E64205472696D3C2F6C693E0A20202020202020203C6C693E506C7567204C6F616420436F6E74726F6C3C2F6C693E0A093C6C693E4F63637570616E63792D626173656420436F6E74726F6C3C2F6C693E0A093C6C693E4461796C6967687420436F6E74726F6C3C2F6C693E0A093C6C693E436F6E7461637420436C6F7375726520496E746567726174696F6E3C2F6C693E0A093C6C693E4241436E657420496E746567726174696F6E3C2F6C693E0A093C6C693E52656D6F74652044657669636520436F6E66696775726174696F6E3C2F6C693E0A093C6C693E44656D616E6420526573706F6E73652052656164793C2F6C693E0A093C6C693E42726F7773657220426173656420436F6E74726F6C204755493C2F6C693E0A093C6C693E4D6F62696C652044657669636520436F6E74726F6C3C2F6C693E0A0A090A202020203C2F756C3E0A3C2F6C693E',NULL,'dialog',NULL),
	(60,'WBI-2671','BACnet Node',X'41204241436E6574204D53545020646576696365207468617420616C6C6F777320746865205752532D323232342052656C6179205363616E6E657220746F2073656E6420616E642072656365697665204241436E657420636F6D6D616E647320746F206F722066726F6D2061204241532E','The WBI-2671 BACnet Node allows programming, monitoring, and control of 5 output groups, and control and monitoring of 24 individual relays and 5 switch inputs by pluggings into a Programmable Relay Scanner incorporatating that scanner into a BACnet network.','WBI-2671.jpg','WBI-2671.jpg',NULL,6,'','',NULL,'WBI-2671',NULL),
	(61,'WNX-2624','Newtork Node',X'41204E6574776F726B204E6F6465207468617420616C6C6F777320746865205752532D323232342052656C6179205363616E6E65727320746F206265206E6574776F726B656420746F6765746865722E','The WNX-2624 Network Node plugs into a WRS-2224 Programmable Relay Scanner and connects that scanner into a LonWorks Network','WNX-2624.jpg','WNX-2624.jpg',NULL,6,'','',NULL,'WNX-2624',NULL),
	(62,'WR-6161','Mechanical Latching Relays',X'546865206D6F73742072656C6961626C652073696E676C6520706F6C6520484944206C61746368696E672072656C6179206F6E20746865206D61726B65742E205468652057522D36313631206973207375697461626C6520666F7220616C6C207479706573206F66206C6F61647320696E636C7564696E6720636170616369746F7220636F72726563746564204849442062616C6C617374732E','These latching relays are rated for 30A branch circuits and 120/277/347Vac loads. They are suitable for all types of lighting loads including capacitor-corrected HID ballasts.','WR-6161.jpg','WR-6161.jpg',NULL,6,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E4275696C742D696E206F76657272696465206C657665722026616D703B204F4E2F4F464620696E64696361746F723C2F6C693E0A20202020202020203C6C693E5375697461626C6520666F72206869676820696E2D727573682064757479206170706C69636174696F6E733C2F6C693E0A20202020202020203C6C693E57522D363136312072656C6179732068617665206120554C2D6365727469666965642073686F727420636972637569742063757272656E7420726174696E6720285343435229206F662031382C303030413C2F6C693E0A20202020202020203C6C693E4D6F756E7420746F20736E6170207261696C206F6620446F75676C61732072656C61792070616E656C3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E','',NULL,'WR-6161',NULL),
	(63,'WR-6172','Mechanical Latching Relays',X'546865206D6F73742072656C6961626C6520322D706F6C6520484944206C61746368696E672072656C6179206F6E20746865206D61726B65742E205468652057522D36313732206973207375697461626C6520666F7220616C6C207479706573206F66206C6F61647320696E636C7564696E6720636170616369746F7220636F72726563746564204849442062616C6C617374732E','These 2-pole latching relays are rated for 20A branch circuits and 120/277/347/480Vac loads. They are suitable for all types of lighting loads including capacitor-corrected HID ballasts and have a built-in override lever & ON/OFF indicator.','WR-6172.jpg','WR-6172.jpg',NULL,6,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E4275696C742D696E206F76657272696465206C657665722026616D703B204F4E2F4F464620696E64696361746F723C2F6C693E0A20202020202020203C6C693E5375697461626C6520666F72206869676820696E2D727573682064757479206170706C69636174696F6E73554C2D6365727469666965642073686F727420636972637569742063757272656E7420726174696E6720285343435229206F662031382C303030412E3C2F6C693E0A20202020202020203C6C693E4D6F756E7420746F20736E6170207261696C206F6620446F75676C61732072656C61792070616E656C3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A','',NULL,'WR-6172',NULL),
	(64,'WR-6221','2-Wire Knock-out Relays',X'412072656C6961626C6520224B6E6F636B6F757422206D6F756E7465642073696E676C6520706F6C65206C61746368696E672072656C61792E','These 1-pole latching relays fit into knock-outs rather than mounting onto relay snap rails and are designed for 16A branch circuits.','WR-6221.jpg','WR-6221.jpg',NULL,6,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E5363726577207465726D696E616C73206F6E206C6F6164207369646520616E6420636F6C6F726564207072652D7374726970706564206C65616473206F6E20636F6E74726F6C20736964653C2F6C693E0A20202020202020203C6C693E4275696C742D696E206F76657272696465206C657665722026616D703B204F4E2F4F464620696E64696361746F723C2F6C693E0A20202020202020203C6C693E4669747320746F207374616E6461726420312F32222070697065206B6E6F636B6F75742028372F382220686F6C65293C2F6C693E0A20202020202020203C6C693E44657369676E656420746F20656173696C79206D6F756E7420746F206261727269657273206F66205745782050616E656C7320666F72206669656C642D617373656D626C65642073797374656D733C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A','',NULL,'WR-6221',NULL),
	(65,'WR-RIB240*B-EL','UL-924 Emergency Shunt Relays',NULL,'Relay shunt to control emergency lighting circuits','WR-RIB240_B-EL.jpg','WR-RIB240_B-EL.jpg',NULL,6,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E456E636C6F7365642072656C6179207368756E74206369726375697420746F20656E61626C6520656D65726765726779206C69676874696E6720647572696E67206120706F776572206F75746167653C2F6C693E0A20202020202020203C6C693E313230566163206F7220323737566176206170706C69636174696F6E3C2F6C693E0A20202020202020203C6C693E53656C662D636F6E7461696E656420696E20506C656E756D2F4E454D41203120726174656420656E636C6F737572653C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A','',NULL,'WR-RIB240_B-EL',NULL),
	(66,'WRE-9242Z','Emergency Circuit Relays',X'416E20456D657267656E637920506F7765722044657669636520746861742077696C6C20737769746368204F4E20757020746F20322057522D363136312072656C61797320746F20737570706F7274206261636B757020616E6420656D657267656E637920706F77657220726571756972656D656E74732E','Relay to control emergency lighting circuits','WRE-9242Z.jpg','WRE-9242Z.jpg',NULL,6,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E456E636C6F7365642072656C617920746F20656E61626C6520656D65726765726779206C69676874696E6720647572696E67206120706F776572206F75746167653C2F6C693E0A20202020202020203C6C693E313230566163206F7220323737566176206170706C69636174696F6E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A','',NULL,'WRE-9242Z',NULL),
	(112,'Dialog Room Controller','Decentralized Lighting Control Systems',X'3C703E54616B6520436F6E74726F6C20456173696C79207769746820746865204469616C6F6720526F6F6D20436F6E74726F6C6C65723B206120506C756720E280984E20436F6E74726F6C2674726164653B204C69676874696E6720436F6E74726F6C20536F6C7574696F6E20666F7220436C617373726F6F6D7320616E64204F6666696365732E3C2F703E','','DRC.png','DRC.png','DRC.png',2,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E436F6D706C6574652C20636F6E66696775726564206170706C69636174696F6E204B69747320666F7220726F6F6D207370656369666963206C69676874696E6720636F6E74726F6C3C2F6C693E0A20202020202020203C6C693E506C756720E280984E20436F6E74726F6C20757020746F203620646966666572656E74206C6F61647320286C69676874696E672C2072656365707461636C652C20656D657267656E6379206C69676874696E67293C2F6C693E0A20202020202020203C6C693E506C756720E280984E20436F6E74726F6C20757020746F20342064696D6D696E67206368616E6E656C7320696E646570656E64656E74206F66206C69676874696E67206C6F6164733C2F6C693E0A20202020202020203C6C693E496E746567726174656420456D657267656E6379204C69676874696E6720436F6E74726F6C2028554C39323420636572746966696564293C2F6C693E0A20202020202020203C6C693E4561737920746F20636F6E6E656374207573696E672077697468206C6F7720636F73742C20706F6C6172697479206E65757472616C2031382F3220776972653C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E4561737920746F20696E7374616C6C2C206561737920746F207573652C20746865204469616C6F6720526F6F6D20436F6E74726F6C6C657220666F72206C6967687420616E642072656365707461636C6520636F6E74726F6C206F66206F66666963657320616E6420636C617373726F6F6D73206973206120506C756720274E20436F6E74726F6C2072656164792C206F75742D6F662D7468652D626F782073797374656D207468617420697320666163746F727920636F6E666967757265642C206C6162656C6C65642C20616E6420746573746564206265666F726520736869706D656E742E2053797374656D206B69747320696E636C7564653A204469616C6F6720526F6F6D20436F6E74726F6C6C65722C204F63637570616E637920616E64204461796C696768742073656E736F72732C20616E642057616C6C2053746174696F6E732E204469616C6F6720526F6F6D20436F6E74726F6C6C65722073797374656D7320617265204241436E657420616E642044656D616E6420526573706F6E73652072656164792E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A3C6C693E3C7370616E3E416476616E74616765733C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E4F6E2D646576696365207465737420627574746F6E7320666F722074657374696E67206369726375697420776972696E673C2F6C693E0A20202020202020203C6C693E436F6C6F7220636F6465642077697265732C206C6162656C7320616E64206F6E2D64657669636520776972696E6720696E737472756374696F6E7320666F7220717569636B20696E7374616C6C6174696F6E3C2F6C693E0A20202020202020203C6C693E44656D616E6420526573706F6E73652052656164793C2F6C693E0A20202020202020203C6C693E554C20616E6420435341206365727469666965643C2F6C693E0A20202020202020203C6C693E554C3932342063657274696669656420666F7220456D657267656E6379204C69676874696E673C2F6C693E0A20202020202020203C6C693E506C656E756D20726174656420696E2055534120616E642043616E6164613C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A',NULL,'dialog_room_controller',NULL),
	(113,'LitePak','Standardized Lighting Control Solutions for Warehouses and Commercial Facilities',X'3C703E4C69746550616B2070616E656C7320696E636C7564652070726F6772616D6D61626C6520746F7563682070616473207769746820342C20382C2031362C20616E642032342072656C6179732E2052656C617920657870616E73696F6E2070616E656C732028776974686F757420746F7563682070616473292061726520616C736F20617661696C61626C6520746F20737570706F72742061206C61726765206E756D626572206F66206C6F6164732E2057616C6C2073746174696F6E732C206F63637570616E637920616E64206461796C696768742073656E736F72732063616E20626520616464656420666F7220696E637265617365642066756E6374696F6E616C6974792E3C2F703E','','litepak.jpg','litepak.jpg','litepak.jpg',3,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E5374616E64617264697A6564204C69676874696E6720436F6E74726F6C732053797374656D3C2F6C693E0A20202020202020203C6C693E4175746F6D6174656420636F6E74726F6C2074686F75676820612070726F6772616D6D61626C6520746F7563682D7061643C2F6C693E0A20202020202020203C6C693E57616C6C2073746174696F6E732C206F63637570616E637920616E64206461796C696768742073656E736F722063616E20626520616464656420666F7220696E637265617365642066756E6374696F6E616C6974793C2F6C693E0A20202020202020203C6C693E457870616E73696F6E2050616E656C732063616E20626520616464656420666F72206C61726765722073797374656D733C2F6C693E0A20202020202020203C6C693E446F6F722070616E656C206F7074696F6E7320696E636C75646573206C6F636B732C20666C757368206D6F756E7420616E642073757266616365206D6F756E7420646F6F72733C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E4C69746550616B2068617320612070726F6772616D6D61626C65203336352D64617920617374726F6E6F6D6963616C2074696D6520616E64206461796C6967687420636F6E74726F6C6C65722077697468696E207468652070616E656C207468617420697320736574207468726F7567682074686520746F756368207061642E2054686520636F6E74726F6C6C657220696E636C75646573206120342D6C696E652067726170686963616C20646973706C61792077697468206275696C7420696E2070726F6D70747320666F722076696577696E6720616E642065646974696E672070726F6772616D732E2054686520636F6E74726F6C6C65722063616E2073746F726520757020746F20393030206576656E74732E204561636820636F6E74726F6C6C6572206861732038206F75747075747320746861742063616E2065616368206265206F70657261746564206279206120756E697175652070726F6772616D2E20557020746F20342072656C6179732063616E20626520636F6E6E656374656420746F20616E206F75747075742E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A3C6C693E3C7370616E3E416476616E74616765733C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E372D646179207363686564756C657320616E6420686F6C69646179207363686564756C65733C2F6C693E0A20202020202020203C6C693E417374726F6E6F6D6963616C20436F6E74726F6C2063616C63756C617465732073756E73657420616E642073756E726973652028616E206F6666736574206F6620C2B1313830206D696E757465732063616E206265206164646564293C2F6C693E0A20202020202020203C6C693E4461796C6967687420436F6E74726F6C2063616E206265207573656420746F20736574206C69676874206C6576656C20666F722065616368206F757470757420287265717569726573206578746572696F72206461796C696768742073656E736F72293C2F6C693E0A20202020202020203C6C693E4F7574707574732063616E2062652073657420746F20737769746368204F4E2C204F46462C204F4646207769746820666C69636B207761726E20616E642063616E20646F2053656E74727920537769746368204F464620636F6D6D616E64733C2F6C693E0A20202020202020203C6C693E54686520636F6E74726F6C6C6572206F7574707574732063616E206D6F6E69746F7220636F6E6E65637465642072656C6179732E205768656E20612072656C6179206973207377697463686564204F4E2C20746865206F75747075742077696C6C20737769746368206974204F46462061667465722061207072652D7365742074696D652E205468697320666561747572652063616E206265207363686564756C656420746F2066756E6374696F6E20647572696E6720756E6F6363757069656420706572696F64732E3C2F6C693E0A20202020202020203C6C693E412047726F7570204F766572726964652053776974636820696E70757420697320737570706C6965642073776974636820616C6C206F7220736F6D65206F757470757473204F4E206F72204F46463C2F6C693E0A20202020202020203C6C693E54686520636F6E74726F6C6C6572206175746F6D61746963616C6C792061646A7573747320666F72206C65617020796561727320616E64204461796C6967687420736176696E67732074696D65206173206E65656465643C2F6C693E0A20202020202020203C6C693E4E6F6E2D766F6C6174696C65206D656D6F7279206D61696E7461696E732070726F6772616D6D696E6720696E20746865206576656E74206F66206120706F776572206661696C7572653C2F6C693E0A20202020202020203C6C693E5375697461626C6520666F7220616C6C207479706573206F66206C69676874696E67206C6F61647320696E636C7564696E6720636170616369746F7220636F72726563746564204849442062616C6C617374733C2F6C693E0A20202020202020203C6C693E554C2F43534120617070726F7665643C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A',NULL,'litepak',NULL),
	(114,'Full Two Way','Full 2 Way Remote Lighting Control System',NULL,'','','full_two_way.png',NULL,9,NULL,NULL,NULL,'full-two-way',1),
	(115,'Legacy','Over the years, Douglas Lighting Controls has provided the market with solutions to better serve lighting control requirements. As we continue to develop new products, we will provide information on our previous \"legacy\" devices in the list below.',NULL,'','legacy.jpg','legacy.jpg','legacy.jpg',7,NULL,NULL,NULL,'legacy',1),
	(120,'WTP-1000/WTP-2000 TrakPak ','The WTP-1000/WTP-2000 TrakPak (current limiting panels) are used to manage and reduce the current associated with track lighting systems.','','The WTP-1000/WTP-2000 TrakPak (current limiting panels) are used to manage and reduce the current associated with track lighting systems.','WTP.png','WTP.png','',6,NULL,NULL,NULL,'wtp-1000_wtp-2000-trak-pak',NULL),
	(121,'Resource Library','Marketing Materials and Sales Tools.',NULL,'','legacy.jpg','legacy.jpg',NULL,10,NULL,NULL,NULL,'resource-library',NULL);

/*!40000 ALTER TABLE `products_new` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table products_new_copygabrielle_14th_April_2016
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_new_copygabrielle_14th_April_2016`;

CREATE TABLE `products_new_copygabrielle_14th_April_2016` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_additional` blob,
  `blurb` text COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'sd4.png',
  `hero` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SD4.jpg',
  `secondary` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `product_type_id` int(10) unsigned NOT NULL,
  `overview` blob,
  `details` blob,
  `product_sub_category_id` int(10) unsigned DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `only_resources` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_product_type_id_index` (`product_type_id`),
  CONSTRAINT `products_new_copygabrielle_14th_April_2016_ibfk_1` FOREIGN KEY (`product_type_id`) REFERENCES `product_type_new` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `products_new_copygabrielle_14th_April_2016` WRITE;
/*!40000 ALTER TABLE `products_new_copygabrielle_14th_April_2016` DISABLE KEYS */;

INSERT INTO `products_new_copygabrielle_14th_April_2016` (`id`, `name`, `description`, `description_additional`, `blurb`, `thumbnail`, `hero`, `secondary`, `created_at`, `updated_at`, `product_type_id`, `overview`, `details`, `product_sub_category_id`, `url`, `only_resources`)
VALUES
	(39,'WOR Ceiling Sensors','Line and Low Voltage Recessed Ceiling Occupany Sensors',NULL,'Diversa Sensors are highly configurable Dual Technology and PIR only occupancy/vacancy sensors that provide load control based on room occupancy.  Diversa sensors enable locations to meet AHSRAE 90.1 and California Title 24 requirements.','WOR.jpg','WOR.jpg','WOR.jpg','0000-00-00 00:00:00','0000-00-00 00:00:00',4,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E4475616C20546563686E6F6C6F677920616E64205049522073656E736F72733C2F6C693E0A20202020202020203C6C693E3132302F3237375661632C2033343756616320616E64204C6F7720566F6C74616765203234566163206D6F64656C733C2F6C693E0A20202020202020203C6C693E4C6F772050726F66696C652044657369676E20666F722061707065616C696E6720696E7374616C6C6174696F6E3C2F6C693E0A20202020202020203C6C693E4F6E2D626F6172642073657474696E6720666F722074696D652D6F75742064656C617920616E64206461796C69676874206C6576656C20286461796C696768742073656E736F7273206D6F64656C73293C2F6C693E0A20202020202020203C6C693E416476616E636520636F6E66696775726174696F6E206F66204475616C20546563686E6F6C6F67792053656E736F727320776974682048616E6468656C6420496E6672617265642053657474696E6720556E69743C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E44697665727361205265636573736564204365696C696E672053656E736F727320696E2050495220287061737369766520696E66726172656429206F72204475616C20546563686E6F6C6F6779202850495220616E642070726F7072696574617279204144492D566F69636529206D6F64656C732070726F76696465206F63637570616E637920646574656374696F6E206F766572206C617267652061726561732E3C2F6C693E0A093C6C693E44697665727361204475616C20546563686E6F6C6F67792073656E736F72732061726520636C617373206C656164696E67207370656369666961626C652073656E736F72732074686174206F7574706572666F726D2074686520636F6D7065746974696F6E206279207265647563696E672066616C73652074726967676572732064756520746F20485641432C206D757369632C20766962726174696F6E2C207472616666696320616E64207265647563696E6720706F77657220636F6E73756D7074696F6E206279207573696E672061207061737369766520736F756E6420646574656374696F6E20746563686E6F6C6F677920726174686572207468616E20616E2061637469766520756C747261736F6E696320746563686E6F6C6F67792E20436F6D62696E696E672050495220616E64204144492D566F69636520746563686E6F6C6F677920616C6C6F7773207468652073656E736F7220746F206D61696E7461696E206F63637570616E6379207468726F7567682069747320766F69636520646574656374696F6E20746563686E6F6C6F677920696E206172656173207468617420646F206E6F742070726F7669646520646972656374206C696E65206F6620736967687420746F206120706572736F6E2E3C2F6C693E0A093C6C693E44697665727361205049522073656E736F72732066696C6C20746865206E65656420666F7220504952206F6E6C79207370656369666965642070726F6A65637473206F722070726F76696465206120636F6D6D65726369616C2067726164652C206561737920746F207573652F696E7374616C6C2073656E736F7220666F7220696E74726F647563746F7279207573616765206F66206C6967687420636F6E74726F6C7320616E6420776F726B2077656C6C20696E2061726561732074686174206861766520646972656374206C696E65206F662073696768742066726F6D2073656E736F7220746F20706572736F6E2E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A3C6C693E3C7370616E3E416476616E74616765733C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E43616E20626520636F6E6669677572656420666F7220566163616E6379206D6F646520286D616E75616C204F4E2F4175746F204F4646293C2F6C693E0A20202020202020203C6C693E5374616E646172642C20457874656E646564206F72204869676820426179206C656E73657320666F72206F7074696D616C206172656120636F7665726167653C2F6C693E0A20202020202020203C6C693E4D6F756E747320666C7573682077697468206365696C696E6720696E746F2061207374616E64617264206F637461676F6E20626F783C2F6C693E0A20202020202020203C6C693E54696C74696E67206C656E7320646972656374732073656E736F7220746F206F7220617761792066726F6D20706172746963756C6172206172656173206F7220666F72206D6F756E74696E67206F6E20736C6F706564206365696C696E67733C2F6C693E0A20202020202020203C6C693E4F7074696F6E7320666F72206461796C696768742073656E736F722C20302D3130762064696D6D696E672C20617578696C696172792072656C61793C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',NULL,'WOR',NULL),
	(40,'WOS Wall Switch Sensors','Line and Low Voltage Wall Switch Occupany Sensors',NULL,'Diversa Sensors are highly configurable Dual Technology and PIR only occupancy/vacancy sensors that provide load control based on room occupancy.  Diversa sensors enable locations to meet AHSRAE 90.1 and California Title 24 requirements.','WOS.jpg','WOS.jpg','WOS.jpg','0000-00-00 00:00:00','0000-00-00 00:00:00',4,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E4475616C20546563686E6F6C6F677920616E64205049522073656E736F72733C2F6C693E0A20202020202020203C6C693E3132302F3237375661632C2033343756616320616E64204C6F7720566F6C74616765203234566163206D6F64656C733C2F6C693E0A20202020202020203C6C693E4D6F6465726E2064657369676E20666F722061707065616C696E6720696E7374616C6C6174696F6E3C2F6C693E0A20202020202020203C6C693E4F6E2D626F6172642073657474696E6720666F722074696D652D6F75742064656C617920616E64206461796C69676874206C6576656C20286461796C696768742073656E736F7273206D6F64656C73293C2F6C693E0A20202020202020203C6C693E416476616E636520636F6E66696775726174696F6E206F66204475616C20546563686E6F6C6F67792053656E736F727320776974682048616E6468656C6420496E6672617265642053657474696E6720556E69743C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E446976657273612057616C6C205377697463682053656E736F727320696E2050495220287061737369766520696E66726172656429206F72204475616C20546563686E6F6C6F6779202850495220616E642070726F7072696574617279204144492D566F69636529206D6F64656C732063616E206265207573656420696E20656E636C6F73656420726F6F6D73207375636820617320636C617373726F6F6D732C2072657374726F6F6D7320616E64206F666669636573207768656E206D616E75616C6C79207475726E696E67206F6E206F72206F66662061206C69676874206D6179206265656E206E65656465642E3C2F6C693E0A093C6C693E44697665727361204475616C20546563686E6F6C6F67792073656E736F72732061726520636C617373206C656164696E67207370656369666961626C652073656E736F72732074686174206F7574706572666F726D2074686520636F6D7065746974696F6E206279207265647563696E672066616C73652074726967676572732064756520746F20485641432C206D757369632C20766962726174696F6E2C207472616666696320616E64207265647563696E6720706F77657220636F6E73756D7074696F6E206279207573696E672061207061737369766520736F756E6420646574656374696F6E20746563686E6F6C6F677920726174686572207468616E20616E2061637469766520756C747261736F6E696320746563686E6F6C6F67792E20436F6D62696E696E672050495220616E64204144492D566F69636520746563686E6F6C6F677920616C6C6F7773207468652073656E736F7220746F206D61696E7461696E206F63637570616E6379207468726F7567682069747320766F69636520646574656374696F6E20746563686E6F6C6F677920696E206172656173207468617420646F206E6F742070726F7669646520646972656374206C696E65206F6620736967687420746F206120706572736F6E2E3C2F6C693E0A093C6C693E44697665727361205049522073656E736F72732066696C6C20746865206E65656420666F7220504952206F6E6C79207370656369666965642070726F6A65637473206F722070726F76696465206120636F6D6D65726369616C2067726164652C206561737920746F207573652F696E7374616C6C2073656E736F7220666F7220696E74726F647563746F7279207573616765206F66206C6967687420636F6E74726F6C7320616E6420776F726B2077656C6C20696E2061726561732074686174206861766520646972656374206C696E65206F662073696768742066726F6D2073656E736F7220746F20706572736F6E2E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A3C6C693E3C7370616E3E416476616E74616765733C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E43616E20626520636F6E6669677572656420666F7220566163616E6379206D6F646520286D616E75616C204F4E2F4175746F204F4646293C2F6C693E0A20202020202020203C6C693E57616C6C2073776974636820666F72206D616E75616C20636F6E74726F6C3C2F6C693E0A20202020202020203C6C693E313830266465673B20646574656374696F6E202D206D6F756E747320666C75736820746F207468652077616C6C20696E2061207374616E646172642067616E6720626F783C2F6C693E0A20202020202020203C6C693E4F7074696F6E7320666F72206461796C696768742073656E736F722C20302D3130762064696D6D696E672C20617578696C696172792072656C61793C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',NULL,'WOS',NULL),
	(41,'WOW Corner/Hallway Sensors','Line and Low Voltage Corner/Hallway Occupancy Sensors',NULL,'Diversa Sensors are highly configurable Dual Technology and PIR only occupancy/vacancy sensors that provide load control based on room occupancy.  Diversa sensors enable locations to meet AHSRAE 90.1 and California Title 24 requirements.','WOW.jpg','WOW.jpg','WOW.jpg','0000-00-00 00:00:00','0000-00-00 00:00:00',4,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E4475616C20546563686E6F6C6F67792053656E736F727320666F722061747269756D732C206C6F62626965732C2068616C6C77617973206F7220737461697277656C6C733C2F6C693E0A20202020202020203C6C693E3132302F3237375661632C2033343756616320616E64204C6F7720566F6C74616765203234566163206D6F64656C733C2F6C693E0A20202020202020203C6C693E4D6F6465726E2064657369676E20666F722061707065616C696E6720696E7374616C6C6174696F6E3C2F6C693E0A20202020202020203C6C693E4F6E2D626F6172642073657474696E6720666F722074696D652D6F75742064656C617920616E64206461796C69676874206C6576656C20286461796C696768742073656E736F7273206D6F64656C73293C2F6C693E0A20202020202020203C6C693E416476616E636520636F6E66696775726174696F6E206F66204475616C20546563686E6F6C6F67792053656E736F727320776974682048616E6468656C6420496E6672617265642053657474696E6720556E69743C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E4469766572736120436F726E6572204D6F756E742053656E736F72732077697468204475616C20546563686E6F6C6F67792073656E73696E6720666F722061747269756D732C206C6F62626965732C2068616C6C77617973206F7220737461697277656C6C73206665617475726520612062616C6C20616E6420736F636B65742064657369676E20746F2070726F7669646520666C65786962696C69747920696E206D6F756E74696E67207468652073656E736F7220746F2077616C6C732C206365696C696E6773206F7220736C6F706564206365696C696E677320666F72206F7074696D697A696E672073656E736F7220706F736974696F6E2E3C2F6C693E0A093C6C693E44697665727361204475616C20546563686E6F6C6F67792073656E736F72732061726520636C617373206C656164696E67207370656369666961626C652073656E736F72732074686174206F7574706572666F726D2074686520636F6D7065746974696F6E206279207265647563696E672066616C73652074726967676572732064756520746F20485641432C206D757369632C20766962726174696F6E2C207472616666696320616E64207265647563696E6720706F77657220636F6E73756D7074696F6E206279207573696E672061207061737369766520736F756E6420646574656374696F6E20746563686E6F6C6F677920726174686572207468616E20616E2061637469766520756C747261736F6E696320746563686E6F6C6F67792E20436F6D62696E696E672050495220616E64204144492D566F69636520746563686E6F6C6F677920616C6C6F7773207468652073656E736F7220746F206D61696E7461696E206F63637570616E6379207468726F7567682069747320766F69636520646574656374696F6E20746563686E6F6C6F677920696E206172656173207468617420646F206E6F742070726F7669646520646972656374206C696E65206F6620736967687420746F206120706572736F6E2E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A3C6C693E3C7370616E3E416476616E74616765733C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E43616E20626520636F6E6669677572656420666F7220566163616E6379206D6F646520286D616E75616C204F4E2F4175746F204F4646293C2F6C693E0A20202020202020203C6C693E53776976656C206D6F756E74696E67206865616420616C6C6F777320666F7220696E7374616C6C6174696F6E20696E20612076617269657479206F66206C6F636174696F6E733C2F6C693E0A20202020202020203C6C693E436F726E6572206F72204C617267652061726561206C656E73657320666F7220737065636966696320636F766572616765207061747465726E733C2F6C693E0A20202020202020203C6C693E4F7074696F6E7320666F72206461796C696768742073656E736F722C20302D3130762064696D6D696E672C20617578696C696172792072656C61793C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',NULL,'WOW',NULL),
	(42,'WP-PP Power Packs','Converts line voltage to low voltage to power Diversa low voltage occupancy sensors',NULL,'Diversa Power Packs provide low voltge power to Diversa low voltage occupancy sensors.','WP-PP.jpg','WP-PP.jpg','WP-PP.jpg','0000-00-00 00:00:00','0000-00-00 00:00:00',4,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E436F6D6D697373696F6E696E6720746F6F6C20666F722044697665727361204F63637570616E63792053656E736F727320616E64204469616C6F67204F63637570616E63792053656E736F72732C204461796C696768742053656E736F722C20616E642057616C6C2053776974636865732053746174696F6E733C2F6C693E0A20202020202020203C6C693E53657420636F6E66696775726174696F6E732C207365742075702061646472657373657320616E642067726F7570732C2061636365737320616476616E636564206D656E75733C2F6C693E0A20202020202020203C6C693E496E667261726564207369676E616C20726561647320616E6420777269746573206465766963652073657474696E67733C2F6C693E0A20202020202020203C6C693E557365732032204141206261747465726965733C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E546865205749522D3331313020496E6672617265642053657474696E6720556E697420616C6C6F777320666F72206465636B206C6576656C20636F6D6D697373696F6E696E67206F662073656E736F727320616E642073776974636865732E205468652068616E6468656C642064657669636520697320696E7475697469766520746F2075736520616E6420616C6C6F777320666F7220616476616E6365642064657669636520636F6E66696775726174696F6E2E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A3C6C693E3C7370616E3E416476616E74616765733C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E506F696E7420616E6420707265737320746F20726561642F777269746520746F20646576696365733C2F6C693E0A20202020202020203C6C693E546F756368206469616C20616C6C6F777320666F72206F6E652068616E646564206E617669676174696F6E20616E642070726F6772616D6D696E673C2F6C693E0A20202020202020203C6C693E52656475636573206C61646465722074696D6520647572696E6720696E7374616C6C6174696F6E20616E6420636F6D6D697373696F6E696E673C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',NULL,'WP_PP',NULL),
	(44,'8700 Series','Lighting Control¬Ç via relay-direct functionality',X'3C756C3E0A202020203C6C693E312C20322C20332C203420427574746F6E205377697463682053746174696F6E733C2F6C693E0A202020203C6C693E44696D6D6572204F4E2F4F4646205377697463682053746174696F6E3C2F6C693E0A3C2F756C3E','8700 Series Wall Stations are used in small to medium sized applications that use Douglas Lighting Controls LitePak, small or satellite relay panels and where straightforward lighting control functionality is required','8700_series.jpg','8700_series.jpg','8700_series.jpg','0000-00-00 00:00:00','0000-00-00 00:00:00',5,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E4669747320696E746F206120312D67616E6720626F783C2F6C693E0A20202020202020203C6C693E53776974636865732063616E2062652067726F7570656420746F676574686572206372656174696E672063656E7472616C697A65642077616C6C2073746174696F6E3C2F6C693E0A20202020202020203C6C693E4E6574776F726B20636F6E6E656374696F6E2077697468204469616C6F6720322D77697265202831382F32292064617461206C696E653C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E446F75676C6173204C69676874696E6720436F6E74726F6C73203837303020736572696573206C696768742073776974636865732070726F7669646520636F6E74726F6C206F76657220666163696C697479206C69676874696E6720766961206D6F6D656E746172792072656C61792D6469726563742066756E6374696F6E616C6974792E20383730302073657269657320737769746368657320617265207573656420696E20736D616C6C20746F206D656469756D2073697A6564206170706C69636174696F6E7320746861742075736520446F75676C6173204C69676874696E6720436F6E74726F6C73204C69746550616B2C20736D616C6C206F7220736174656C6C6974652072656C61792070616E656C7320616E6420776865726520737472616967687420666F7277617264206C69676874696E6720636F6E74726F6C2066756E6374696F6E616C6974792069732072657175697265642E20383730302073776974636865732061726520616C736F20656173696C7920636F6E6669677572656420696E746F20612073797374656D207468617420757365732044697665727361206C6F7720766F6C74616765206F63637570616E63792073656E736F72732E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',NULL,'8700_series',NULL),
	(45,'3500 Series','Lighting Control¬Ç via digital functionality',X'3C756C3E0A202020203C6C693E312C20322C20332C20342026616D703B20382D627574746F6E2073776974636865733C2F6C693E0A202020203C6C693E44696D6D6572204F4E2F4F4646205377697463682053746174696F6E3C2F6C693E0A202020203C6C693E4B657920616374697661746564207377697463683C2F6C693E0A3C2F756C3E','3500 Series Wall Stations are used in Dialog digitally controlled systems to provide a clean and thoughtful user interface.','3500_series.jpg','3500_series.jpg','3500_series.jpg','0000-00-00 00:00:00','0000-00-00 00:00:00',5,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E4469676974616C2C2070726F6772616D6D61626C652073776974636865733C2F6C693E0A20202020202020203C6C693E456173696C7920636F6E6E6563747320746F206E6F6E2D706F6C6172697A6564204469616C6F67206E6574776F726B20776974682031382F3220776972653C2F6C693E0A20202020202020203C6C693E4C45442073746174757320696E64696361746F72733C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E4469616C6F6720333530302073657269657320737769746368657320617265206469676974616C6C7920636F6E74726F6C6C656420616E642070726F6772616D6D61626C6520666F7220636F6D6D65726369616C20737769746368696E6720726571756972656D656E74732E2053776974636865732063616E2062652067726F7570656420746F20666F726D20636F6D706163742077616C6C207377697463682073746174696F6E732E204120636F6D706C6574652072616E6765206F6620737769746368206661636520706C617465732061726520617661696C61626C6520696E2073637265776C65737320776869746520706C6173746963206F7220737461696E6C65737320737465656C20287769746820736372657773292E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',NULL,'3500_series',NULL),
	(46,'WN-7700 Switch Enclosures','Lockable Switch Enclosures to Prevent Tampering',X'3C756C3E0A202020203C6C693E466C757368206F722073757266616365206D6F756E7420696E7374616C6C6174696F6E3C2F6C693E0A202020203C6C693E536964652068696E6765206F7220746F702068696E676520646F6F72733C2F6C693E0A202020203C6C693E4B6579204C6F636B3C2F6C693E0A3C2F756C3E','WN-7700 Series lockable steel switch enclosures are well designed to prevent damage, tampering and abuse of switches. Switch enclosures are typically used in high school gymnasiums or public and high traffic areas. Enclosures consist of a back box, mounting plate, and cover plate with hinged door.','WN-7700.jpg','WN-7700.jpg','WN-7700.jpg','0000-00-00 00:00:00','0000-00-00 00:00:00',5,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E576964652072616E6765206F662073697A65733C2F6C693E0A20202020202020203C6C693E537465656C20636F6E737472756374696F6E3C2F6C693E0A20202020202020203C6C693E4C6F636B696E6720646F6F723C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E574E2D3737303020536572696573206C6F636B61626C6520737465656C2073776974636820656E636C6F7375726573206172652077656C6C2064657369676E656420746F2070726576656E742064616D6167652C2074616D706572696E6720616E64206162757365206F662073776974636865732E2053776974636820656E636C6F737572657320617265207479706963616C6C79207573656420696E2068696768207363686F6F6C2067796D6E617369756D73206F72207075626C696320616E64206869676820747261666669632061726561732E20456E636C6F737572657320636F6E73697374206F662061206261636B20626F782C206D6F756E74696E6720706C6174652C20616E6420636F76657220706C61746520776974682068696E67656420646F6F722E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',NULL,'WN-7700',NULL),
	(47,'Face Plates','Face Plates for 8700 and 3500 Series Switches',X'3C756C3E0A202020203C6C693E312D67616E6720746F20352D67616E67206661636520706C617465733C2F6C693E0A202020203C6C693E4D6F6465726E2077686974652073656D692D676C6F73732C2073637265776C6573732064657369676E3C2F6C693E0A202020203C6C693E537461696E6C65737320537465656C2073637265772064657369676E3C2F6C693E0A3C2F756C3E','White, semi-gloss screwless face plates provide a pleasing cover for Dialog and 8700 series switches. Stainless steel face plates are also available and are installed with traditional mounting screws.','face_plates.jpg','face_plates.jpg','face_plates.jpg','0000-00-00 00:00:00','0000-00-00 00:00:00',5,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E312D67616E6720746F20352D67616E67206661636520706C617465733C2F6C693E0A20202020202020203C6C693E4D6F6465726E2077686974652073656D692D676C6F73732C2073637265776C6573732064657369676E3C2F6C693E0A20202020202020203C6C693E537461696E6C65737320537465656C2073637265772064657369676E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E57686974652C2073656D692D676C6F73732073637265776C657373206661636520706C617465732070726F76696465206120706C656173696E6720636F76657220666F72204469616C6F6720616E642038373030207365726965732073776974636865732E20537461696E6C65737320737465656C206661636520706C617465732061726520616C736F20617661696C61626C6520616E642061726520696E7374616C6C6564207769746820747261646974696F6E616C206D6F756E74696E67207363726577732E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',NULL,'face_plates',NULL),
	(48,'WIR-3110','Handheld Infrared Setting Unit',X'412068616E6468656C6420496E6672617265642053657474696E6720556E6974207573656420746F207365742074686520706172616D6574657273206F6620737769746368657320616E642073656E736F72732E','The WIR-3110 Infrared Setting Unit allows for deck level commissioning of sensors and switches. The handheld device is intuitive to use and allows for advanced device configuration.','WIR-3110.jpg','WIR-3110.jpg',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',6,'',NULL,NULL,'WIR-3110',NULL),
	(51,'WNG-3131','BACnet Gateway',X'416E2049502047617465776179207573656420746F20696E636F72706F72617465206120746865204469616C6F672053797374656D20696E746F2061204D616E6167656D656E7420436F6E74726F6C204E6574776F726B20746861742075736573204241436E657420746563686E6F6C6F67792E','Th WNG-3131 is used to incorporate a Dialog Lighting Control System into a Management Control Network that uses BACnet technology','WNG-3131.jpg','WNG-3131.jpg',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',6,'',NULL,NULL,'WNG-3131',NULL),
	(58,'Dialog &mdash; Our Centralized Lighting Control System','Dialog is a digital, addressable, programmable lighting control system for entire floors, buildings and large multi-building applications.',NULL,'Dialog is an advanced digital lighting control system connecting and controlling lights through relays, occupancy sensors, photo sensors, and switch stations. Dialog enables your site to meet AHSRAE 90.1 and California Title 24 requirements.','sd4.png','dialog.jpg','dialog.jpg','0000-00-00 00:00:00','0000-00-00 00:00:00',1,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E436F6D706C657465206C69676874696E6720636F6E74726F6C2073797374656D20666163746F727920636F6E666967757265642C2070726F6772616D6D65642C20616E642074657374656420666F7220656163682070726F6A6563743C2F6C693E0A20202020202020203C6C693E496E636C756465732072656C61792070616E656C732C20636F6E74726F6C6C6572732C206F63637570616E63792F766163616E63792073656E736F72732C206461796C696768742073656E736F72732C20616E642077616C6C207377697463682073746174696F6E733C2F6C693E0A20202020202020203C6C693E322F3138415747206E6574776F726B20636F6E6E656374696F6E20666F72206561737920616E642072656C6961626C65206461746120616E6420706F776572207472616E736D697373696F6E3C2F6C693E0A20202020202020203C6C693E496E746567726174656420746F7563682D73637265656E206F72207765622062726F7773657220696E7465726661636520666F72206F6E2D73697465206F722072656D6F74652070726F6772616D6D696E67206368616E676573203C2F6C693E0A20202020202020203C6C693E695061642067726170686963616C20636F6E74726F6C20696E7465726661636520666F722072656D6F74652073797374656D20636F6E74726F6C3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A',X'3C6C693E0A093C756C3E0A09093C6C693E204469616C6F6720697320616E20616476616E636564206469676974616C206C69676874696E6720636F6E74726F6C2073797374656D20636F6E6E656374696E6720616E6420636F6E74726F6C6C696E67206C6967687473207468726F7567682072656C6179732C206F63637570616E63792073656E736F72732C2070686F746F2073656E736F72732C2062616C6C617374732C20616E64207377697463682073746174696F6E732E204469616C6F6720697320612063656E7472616C697A656420636F6E74726F6C6C65722074686174206D616E6167657320666163696C697479206C69676874696E6720726571756972656D656E747320676C6F62616C6C792C206F7220627920617265612C206F7220627920726F6F6D2E2054686520666C65786962696C69747920616E642074727565207363616C6162696C697479206F66204469616C6F6720656173696C7920616C6C6F777320697420746F206265207573656420696E20612076617269657479206F66206170706C69636174696F6E732066726F6D20736D616C6C20636F6D6D65726369616C206275696C64696E677320746F206C6172676520666163696C69746965732073756368206173206F666669636520746F776572732C207363686F6F6C732C20616972706F72747320616E64207374616469756D732E3C2F6C693E0A093C2F756C3E0A3C2F6C693E0A0A3C6C693E3C7370616E3E416476616E74616765733C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E5363686564756C696E673C2F6C693E0A20202020202020203C6C693E4F4E2F4F464620537769746368696E673C2F6C693E0A20202020202020203C6C693E44696D6D696E672F4D756C74692D6C6576656C20436F6E74726F6C3C2F6C693E0A20202020202020203C6C693E4869676820456E64205472696D3C2F6C693E0A20202020202020203C6C693E506C7567204C6F616420436F6E74726F6C3C2F6C693E0A093C6C693E4F63637570616E63792D626173656420436F6E74726F6C3C2F6C693E0A093C6C693E4461796C6967687420436F6E74726F6C3C2F6C693E0A093C6C693E436F6E7461637420436C6F7375726520496E746567726174696F6E3C2F6C693E0A093C6C693E4241436E657420496E746567726174696F6E3C2F6C693E0A093C6C693E52656D6F74652044657669636520436F6E66696775726174696F6E3C2F6C693E0A093C6C693E44656D616E6420526573706F6E73652052656164793C2F6C693E0A093C6C693E42726F7773657220426173656420436F6E74726F6C204755493C2F6C693E0A093C6C693E4D6F62696C652044657669636520436F6E74726F6C3C2F6C693E0A0A090A202020203C2F756C3E0A3C2F6C693E',NULL,'dialog',NULL),
	(60,'WBI-2671','BACnet Node',X'41204241436E6574204D53545020646576696365207468617420616C6C6F777320746865205752532D323232342052656C6179205363616E6E657220746F2073656E6420616E642072656365697665204241436E657420636F6D6D616E647320746F206F722066726F6D2061204241532E','The WBI-2671 BACnet Node allows programming, monitoring, and control of 5 output groups, and control and monitoring of 24 individual relays and 5 switch inputs by pluggings into a Programmable Relay Scanner incorporatating that scanner into a BACnet network.','WBI-2671.jpg','WBI-2671.jpg',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',6,'','',NULL,'WBI-2671',NULL),
	(61,'WNX-2624','Newtork Node',X'41204E6574776F726B204E6F6465207468617420616C6C6F777320746865205752532D323232342052656C6179205363616E6E65727320746F206265206E6574776F726B656420746F6765746865722E','The WNX-2624 Network Node plugs into a WRS-2224 Programmable Relay Scanner and connects that scanner into a LonWorks Network','WNX-2624.jpg','WNX-2624.jpg',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',6,'','',NULL,'WNX-2624',NULL),
	(62,'WR-6161','Mechanical Latching Relays',X'546865206D6F73742072656C6961626C652073696E676C6520706F6C6520484944206C61746368696E672072656C6179206F6E20746865206D61726B65742E205468652057522D36313631206973207375697461626C6520666F7220616C6C207479706573206F66206C6F61647320696E636C7564696E6720636170616369746F7220636F72726563746564204849442062616C6C617374732E','These latching relays are rated for 30A branch circuits and 120/277/347Vac loads. They are suitable for all types of lighting loads including capacitor-corrected HID ballasts.','WR-6161.jpg','WR-6161.jpg',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',6,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E4275696C742D696E206F76657272696465206C657665722026616D703B204F4E2F4F464620696E64696361746F723C2F6C693E0A20202020202020203C6C693E5375697461626C6520666F72206869676820696E2D727573682064757479206170706C69636174696F6E733C2F6C693E0A20202020202020203C6C693E57522D363136312072656C6179732068617665206120554C2D6365727469666965642073686F727420636972637569742063757272656E7420726174696E6720285343435229206F662031382C303030413C2F6C693E0A20202020202020203C6C693E4D6F756E7420746F20736E6170207261696C206F6620446F75676C61732072656C61792070616E656C3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E','',NULL,'WR-6161',NULL),
	(63,'WR-6172','Mechanical Latching Relays',X'546865206D6F73742072656C6961626C6520322D706F6C6520484944206C61746368696E672072656C6179206F6E20746865206D61726B65742E205468652057522D36313732206973207375697461626C6520666F7220616C6C207479706573206F66206C6F61647320696E636C7564696E6720636170616369746F7220636F72726563746564204849442062616C6C617374732E','These 2-pole latching relays are rated for 20A branch circuits and 120/277/347/480Vac loads. They are suitable for all types of lighting loads including capacitor-corrected HID ballasts and have a built-in override lever & ON/OFF indicator.','WR-6172.jpg','WR-6172.jpg',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',6,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E4275696C742D696E206F76657272696465206C657665722026616D703B204F4E2F4F464620696E64696361746F723C2F6C693E0A20202020202020203C6C693E5375697461626C6520666F72206869676820696E2D727573682064757479206170706C69636174696F6E73554C2D6365727469666965642073686F727420636972637569742063757272656E7420726174696E6720285343435229206F662031382C303030412E3C2F6C693E0A20202020202020203C6C693E4D6F756E7420746F20736E6170207261696C206F6620446F75676C61732072656C61792070616E656C3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A','',NULL,'WR-6172',NULL),
	(64,'WR-6221','2-Wire Knock-out Relays',X'412072656C6961626C6520224B6E6F636B6F757422206D6F756E7465642073696E676C6520706F6C65206C61746368696E672072656C61792E','These 1-pole latching relays fit into knock-outs rather than mounting onto relay snap rails and are designed for 16A branch circuits.','WR-6221.jpg','WR-6221.jpg',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',6,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E5363726577207465726D696E616C73206F6E206C6F6164207369646520616E6420636F6C6F726564207072652D7374726970706564206C65616473206F6E20636F6E74726F6C20736964653C2F6C693E0A20202020202020203C6C693E4275696C742D696E206F76657272696465206C657665722026616D703B204F4E2F4F464620696E64696361746F723C2F6C693E0A20202020202020203C6C693E4669747320746F207374616E6461726420312F32222070697065206B6E6F636B6F75742028372F382220686F6C65293C2F6C693E0A20202020202020203C6C693E44657369676E656420746F20656173696C79206D6F756E7420746F206261727269657273206F66205745782050616E656C7320666F72206669656C642D617373656D626C65642073797374656D733C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A','',NULL,'WR-6221',NULL),
	(65,'WR-RIB240*B-EL','UL-924 Emergency Shunt Relays',NULL,'Relay shunt to control emergency lighting circuits','WR-RIB240_B-EL.jpg','WR-RIB240_B-EL.jpg',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',6,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E456E636C6F7365642072656C6179207368756E74206369726375697420746F20656E61626C6520656D65726765726779206C69676874696E6720647572696E67206120706F776572206F75746167653C2F6C693E0A20202020202020203C6C693E313230566163206F7220323737566176206170706C69636174696F6E3C2F6C693E0A20202020202020203C6C693E53656C662D636F6E7461696E656420696E20506C656E756D2F4E454D41203120726174656420656E636C6F737572653C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A','',NULL,'WR-RIB240_B-EL',NULL),
	(66,'WRE-9242Z','Emergency Circuit Relays',X'416E20456D657267656E637920506F7765722044657669636520746861742077696C6C20737769746368204F4E20757020746F20322057522D363136312072656C61797320746F20737570706F7274206261636B757020616E6420656D657267656E637920706F77657220726571756972656D656E74732E','Relay to control emergency lighting circuits','WRE-9242Z.jpg','WRE-9242Z.jpg',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',6,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E456E636C6F7365642072656C617920746F20656E61626C6520656D65726765726779206C69676874696E6720647572696E67206120706F776572206F75746167653C2F6C693E0A20202020202020203C6C693E313230566163206F7220323737566176206170706C69636174696F6E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A','',NULL,'WRE-9242Z',NULL),
	(112,'Dialog Room Controller','Decentralized Lighting Control Systems',X'3C703E54616B6520436F6E74726F6C20456173696C79207769746820746865204469616C6F6720526F6F6D20436F6E74726F6C6C65723B206120506C756720E280984E20436F6E74726F6C2674726164653B204C69676874696E6720436F6E74726F6C20536F6C7574696F6E20666F7220436C617373726F6F6D7320616E64204F6666696365732E3C2F703E','','DRC.png','DRC.png','DRC.png','0000-00-00 00:00:00','0000-00-00 00:00:00',2,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E436F6D706C6574652C20636F6E66696775726564206170706C69636174696F6E204B69747320666F7220726F6F6D207370656369666963206C69676874696E6720636F6E74726F6C3C2F6C693E0A20202020202020203C6C693E506C756720E280984E20436F6E74726F6C20757020746F203620646966666572656E74206C6F61647320286C69676874696E672C2072656365707461636C652C20656D657267656E6379206C69676874696E67293C2F6C693E0A20202020202020203C6C693E506C756720E280984E20436F6E74726F6C20757020746F20342064696D6D696E67206368616E6E656C7320696E646570656E64656E74206F66206C69676874696E67206C6F6164733C2F6C693E0A20202020202020203C6C693E496E746567726174656420456D657267656E6379204C69676874696E6720436F6E74726F6C2028554C39323420636572746966696564293C2F6C693E0A20202020202020203C6C693E4561737920746F20636F6E6E656374207573696E672077697468206C6F7720636F73742C20706F6C6172697479206E65757472616C2031382F3220776972653C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E4561737920746F20696E7374616C6C2C206561737920746F207573652C20746865204469616C6F6720526F6F6D20436F6E74726F6C6C657220666F72206C6967687420616E642072656365707461636C6520636F6E74726F6C206F66206F66666963657320616E6420636C617373726F6F6D73206973206120506C756720274E20436F6E74726F6C2072656164792C206F75742D6F662D7468652D626F782073797374656D207468617420697320666163746F727920636F6E666967757265642C206C6162656C6C65642C20616E6420746573746564206265666F726520736869706D656E742E2053797374656D206B69747320696E636C7564653A204469616C6F6720526F6F6D20436F6E74726F6C6C65722C204F63637570616E637920616E64204461796C696768742073656E736F72732C20616E642057616C6C2053746174696F6E732E204469616C6F6720526F6F6D20436F6E74726F6C6C65722073797374656D7320617265204241436E657420616E642044656D616E6420526573706F6E73652072656164792E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A3C6C693E3C7370616E3E416476616E74616765733C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E4F6E2D646576696365207465737420627574746F6E7320666F722074657374696E67206369726375697420776972696E673C2F6C693E0A20202020202020203C6C693E436F6C6F7220636F6465642077697265732C206C6162656C7320616E64206F6E2D64657669636520776972696E6720696E737472756374696F6E7320666F7220717569636B20696E7374616C6C6174696F6E3C2F6C693E0A20202020202020203C6C693E44656D616E6420526573706F6E73652052656164793C2F6C693E0A20202020202020203C6C693E554C20616E6420435341206365727469666965643C2F6C693E0A20202020202020203C6C693E554C3932342063657274696669656420666F7220456D657267656E6379204C69676874696E673C2F6C693E0A20202020202020203C6C693E506C656E756D20726174656420696E2055534120616E642043616E6164613C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A',NULL,'dialog_room_controller',NULL),
	(113,'LitePak','Standardized Lighting Control Solutions for Warehouses and Commercial Facilities',X'3C703E4C69746550616B2070616E656C7320696E636C7564652070726F6772616D6D61626C6520746F7563682070616473207769746820342C20382C2031362C20616E642032342072656C6179732E2052656C617920657870616E73696F6E2070616E656C732028776974686F757420746F7563682070616473292061726520616C736F20617661696C61626C6520746F20737570706F72742061206C61726765206E756D626572206F66206C6F6164732E2057616C6C2073746174696F6E732C206F63637570616E637920616E64206461796C696768742073656E736F72732063616E20626520616464656420666F7220696E637265617365642066756E6374696F6E616C6974792E3C2F703E','','litepak.jpg','litepak.jpg','litepak.jpg','0000-00-00 00:00:00','0000-00-00 00:00:00',3,X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E5374616E64617264697A6564204C69676874696E6720436F6E74726F6C732053797374656D3C2F6C693E0A20202020202020203C6C693E4175746F6D6174656420636F6E74726F6C2074686F75676820612070726F6772616D6D61626C6520746F7563682D7061643C2F6C693E0A20202020202020203C6C693E57616C6C2073746174696F6E732C206F63637570616E637920616E64206461796C696768742073656E736F722063616E20626520616464656420666F7220696E637265617365642066756E6374696F6E616C6974793C2F6C693E0A20202020202020203C6C693E457870616E73696F6E2050616E656C732063616E20626520616464656420666F72206C61726765722073797374656D733C2F6C693E0A20202020202020203C6C693E446F6F722070616E656C206F7074696F6E7320696E636C75646573206C6F636B732C20666C757368206D6F756E7420616E642073757266616365206D6F756E7420646F6F72733C2F6C693E0A202020203C2F756C3E0A3C2F6C693E',X'3C6C693E0A202020203C756C3E0A20202020202020203C6C693E4C69746550616B2068617320612070726F6772616D6D61626C65203336352D64617920617374726F6E6F6D6963616C2074696D6520616E64206461796C6967687420636F6E74726F6C6C65722077697468696E207468652070616E656C207468617420697320736574207468726F7567682074686520746F756368207061642E2054686520636F6E74726F6C6C657220696E636C75646573206120342D6C696E652067726170686963616C20646973706C61792077697468206275696C7420696E2070726F6D70747320666F722076696577696E6720616E642065646974696E672070726F6772616D732E2054686520636F6E74726F6C6C65722063616E2073746F726520757020746F20393030206576656E74732E204561636820636F6E74726F6C6C6572206861732038206F75747075747320746861742063616E2065616368206265206F70657261746564206279206120756E697175652070726F6772616D2E20557020746F20342072656C6179732063616E20626520636F6E6E656374656420746F20616E206F75747075742E3C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A3C6C693E3C7370616E3E416476616E74616765733C2F7370616E3E0A202020203C756C3E0A20202020202020203C6C693E372D646179207363686564756C657320616E6420686F6C69646179207363686564756C65733C2F6C693E0A20202020202020203C6C693E417374726F6E6F6D6963616C20436F6E74726F6C2063616C63756C617465732073756E73657420616E642073756E726973652028616E206F6666736574206F6620C2B1313830206D696E757465732063616E206265206164646564293C2F6C693E0A20202020202020203C6C693E4461796C6967687420436F6E74726F6C2063616E206265207573656420746F20736574206C69676874206C6576656C20666F722065616368206F757470757420287265717569726573206578746572696F72206461796C696768742073656E736F72293C2F6C693E0A20202020202020203C6C693E4F7574707574732063616E2062652073657420746F20737769746368204F4E2C204F46462C204F4646207769746820666C69636B207761726E20616E642063616E20646F2053656E74727920537769746368204F464620636F6D6D616E64733C2F6C693E0A20202020202020203C6C693E54686520636F6E74726F6C6C6572206F7574707574732063616E206D6F6E69746F7220636F6E6E65637465642072656C6179732E205768656E20612072656C6179206973207377697463686564204F4E2C20746865206F75747075742077696C6C20737769746368206974204F46462061667465722061207072652D7365742074696D652E205468697320666561747572652063616E206265207363686564756C656420746F2066756E6374696F6E20647572696E6720756E6F6363757069656420706572696F64732E3C2F6C693E0A20202020202020203C6C693E412047726F7570204F766572726964652053776974636820696E70757420697320737570706C6965642073776974636820616C6C206F7220736F6D65206F757470757473204F4E206F72204F46463C2F6C693E0A20202020202020203C6C693E54686520636F6E74726F6C6C6572206175746F6D61746963616C6C792061646A7573747320666F72206C65617020796561727320616E64204461796C6967687420736176696E67732074696D65206173206E65656465643C2F6C693E0A20202020202020203C6C693E4E6F6E2D766F6C6174696C65206D656D6F7279206D61696E7461696E732070726F6772616D6D696E6720696E20746865206576656E74206F66206120706F776572206661696C7572653C2F6C693E0A20202020202020203C6C693E5375697461626C6520666F7220616C6C207479706573206F66206C69676874696E67206C6F61647320696E636C7564696E6720636170616369746F7220636F72726563746564204849442062616C6C617374733C2F6C693E0A20202020202020203C6C693E554C2F43534120617070726F7665643C2F6C693E0A202020203C2F756C3E0A3C2F6C693E0A',NULL,'litepak',NULL),
	(114,'Full Two Way','Full 2 Way Remote Lighting Control System',NULL,'','','full_two_way.png',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',9,NULL,NULL,NULL,'full_two_way',1),
	(115,'Legacy','Over the years, Douglas Lighting Controls has provided the market with solutions to better serve lighting control requirements. As we continue to develop new products, we will provide information on our previous \"legacy\" devices in the list below.',NULL,'','legacy.jpg','legacy.jpg','legacy.jpg','0000-00-00 00:00:00','0000-00-00 00:00:00',7,NULL,NULL,NULL,'legacy',1),
	(120,'WTP-1000/WTP-2000 TrakPak ','The WTP-1000/WTP-2000 TrakPak (current limiting panels) are used to manage and reduce the current associated with track lighting systems.','','The WTP-1000/WTP-2000 TrakPak (current limiting panels) are used to manage and reduce the current associated with track lighting systems.','WTP.png','WTP.png','','0000-00-00 00:00:00','0000-00-00 00:00:00',6,NULL,NULL,NULL,'wtp-1000_wtp-2000-trak-pak',NULL);

/*!40000 ALTER TABLE `products_new_copygabrielle_14th_April_2016` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table repLocations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `repLocations`;

CREATE TABLE `repLocations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `locname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lng` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `view_state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postal` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `web` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `repLocations` WRITE;
/*!40000 ALTER TABLE `repLocations` DISABLE KEYS */;

INSERT INTO `repLocations` (`id`, `locname`, `lat`, `lng`, `address`, `address2`, `city`, `state`, `view_state`, `postal`, `phone`, `web`, `email`)
VALUES
	(0,'','','','','','','','','','','',NULL),
	(1,'AMA Lighting - Birmingham ','33.477596','-86.777201','402 Office Park Drive','Suite G50','Mountain Brook','US-AL','AL','35223','205-278-8199','http://www.amalighting.net',''),
	(2,'AMA Lighting - Mobile','30.671936','-88.139009','813 Downtowner Blvd','Suite A','Mobile','US-AL','AL','36609','251-343-7693','http://www.amalighting.net','devani@amalighting.com'),
	(3,'Porter Lighting & Controls','34.69364','-92.312191','6001 Murray street','','Little Rock','US-AR','AR','72209','501-570-8100','http://www.porterlc.com','bgardisser@porter-sales.com'),
	(4,'Wild West Lighting','33.627296','-111.899631','15550 N. 84th Street','Suite 201','Scottsdale','US-AZ','AZ','58260','480-368-9909','http://www.wildwestlighting.com','tstrand@wildwestlighting.com'),
	(5,'Swoboda Lighting','38.67062','-121.368665','5740 Roseville Rd','Suite F','Sacramento','US-CA','CA','95842','916-521-8063','http://www.swobodalighting.com','larry@SwobodaLighting.com'),
	(6,'Wunder Lighting & Controls','37.926282','-122.013813','2977 Ygnacio Valley Rd','Suite 190','Walnut Creek','US-CA','CA','94598','510-812-7301','http://www.wunderlc.com','mike@wunderlc.com'),
	(7,'Integrated Lighting','35.375498','-119.011258','814 18th St.','','Bakersfield','US-CA','CA','93301','661-322-5150','http://www.gotlumens.com','dean@gotlumens.com'),
	(8,'Prudential Lighting Products','34.016818','-118.24157','1737 East 22nd Street','','Los Angeles','US-CA','CA','90058','213-746-0360','http://www.plpsocal.com','DFord@plpsocal.com'),
	(9,'George Lacey Sales','39.694915','-104.99885','1210 South Jason Street','','Denver','US-CO','CO','80223','303-390-8220','http://www.georgelaceysales.com','slacey@georgelaceysales.com'),
	(10,'Sesco Lighting - FL','28.598228','-81.364409','1133 West Morse Blvd','','Winterpark','US-FL','FL','32789','407-629-6100','http://www.sescolighting.com','amackay@sescolighting.com'),
	(11,'Sesco Lighting - GA','33.95378','-84.217578','2000 Miller Court W','','Norcross','US-GA','GA','30071','770-449-7045','http://www.sescolighting.com','bjohnson@sescolighting.com'),
	(12,'M Lau Company','21.454037','-157.817114','47-096 Kamehameha Hwy','','Kaneohe','US-HI','HI','96744','808-239-0199','http://www.mlaucompanyinc.com','mike@mlaucompanyinc.com'),
	(15,'Specified Lighting Systems','39.91723','-86.0423','8904 Bash St ','Suite B','Indianapolis','US-IN','IN','46256','317-577-8100','http://www.sls-indiana.com','BBender@slsindiana.com'),
	(16,'Pinnacle Lighting','38.849671','-94.622348','15405 Aberdeen ','','Leawood','US-KS','KS','66224','816-590-6188','','roger@pinnaclelightingandcontrols.com'),
	(17,'One Source Lighting & Design, LLC','37.809171','-85.986896','604 Brown St','','Vinegrove','US-KE','KE','40175','270-877-2221','http://www.2lightu.com','victoria@2lightu.com'),
	(18,'Curtis Stout Inc','29.896884','-90.144976','201 Evans Rd','Suite 110','Harahan','US-LA','LA','70123','504-818-1848','http://www.chstout.com','info@chstout.com'),
	(19,'Porter Sales','34.693610','-92.312445','6001 Murray St','','Little Rock','US-LA','LA','72209','501-870-8100','http://www.bellandmccoy.com/porterlc/','bgardisser@porter-sales.com'),
	(21,'Michigan Lighting Systems - East','42.557885','-83.122271','1389 Wheaton Ave, Suite 500','','Troy','US-MI','MI','48083','248-542-2200','http://www.michlightingsystems.com/east/','josterwyk@mls-east.com'),
	(22,'Michigan Lighting Systems - West','43.021199','-85.743203','2918 North Ridge NW','','Grand Rapids','US-MI','MI','49544','616-785-1415','http://www.michlightingsystems.com/west/','jbyers@mls-west.com'),
	(23,'Mlazgar Associates','44.860433','-93.408409','10340 Viking Drive','Suite 150','Eden Praire','US-MN','MN','55344','952-943-8080','http://www.mlazgar.com','markm@mlazgar.com'),
	(26,'Prime West','45.674317','-111.061515','502 S 19th Ave','Ste 102','Bozeman','US-MT','MT','59718','406-586-2227','http://www.primewestelectrical.com','jodie@pwestgroup.com'),
	(27,'NRG Sales Llc','41.660378','-93.592105','5485 NE 17th St','Suite A','Des Moines','US-IA','IA','','515-528-8201','http://ww.nrgsales.com','jturek@nrgsales.com'),
	(28,'MB Widerman','36.451902','-79.732817','113 James Circle','','Eden','US-NC','NC','27288','336-635-3492','http://www.mbw-a.com','mbw@mbw-a.com'),
	(29,'DECO Lights Inc','36.108947','-115.205194','4780 Harmon Ave','','Las Vegas','US-NV','NV','89103','702-257-8075','http://www.decolightsinc.com','paulines@decolightsinc.com'),
	(30,'Luminosity Lighting Agency','40.6061951','-75.561321','4375 Parkland Dr','','Allentown','US-PA','PA','19047','215-480-4880','http://www.l2a.us','larry.fine@l2a.us'),
	(31,'Schroeder Sales Inc','35.083367','-106.650204','215 Gold Southwest ','Suite 101','Alburquerque','US-NM','NM','87102','505-248-2332','http://www.schroedersales.com','kromero@schroedersales.com'),
	(33,'Stan Deutsch Associates (SDA)','40.740038','-73.935416','31-30 Hunters Point Ave ','','Long Island City','US-NY','NY','11101','718-361-9150','www.sdalighting.com','info@sdalighting.com'),
	(34,'Quality Lighting Systems','42.73612','-73.844945','417 Karner Road','','Albany','US-NY','NY','12205','518-456-2820','http://www.qlsny.com',''),
	(35,'Bright Focus Sales','41.508074','-81.675551','2310 Superior Avenue','Suite 220','Cleveland','US-OH','OH','44114','216-751-8384','http://www.brightfocussales.com','lpatrizi@brightsales.com'),
	(37,'Triple C Lighting','35.467485','-97.533153','1212 W. Main Street','','Oklahoma City','US-OK','OK','73106','405-223-5545','http://www.tripleccompanies.com','insidesales@tripleccompanies.com'),
	(38,'Malcar Northwest','45.495961','-122.622617','3705 S.E. 39th Street','','Portland','US-OR','OR','97202','503-233-8755','http://www.malcar.com','bmcquillan@malcarnw.com'),
	(39,'Repco II Inc','40.410163','-80.034369','1201 Chappel Ave','','Pittsburg','US-PA','PA','15216','412-562-6220','http://www.repcoii.com','johnnorthern@repcoii.com'),
	(40,'The Lighting Source (South Carlolina Lighting)','34.821415','-82.289731','1200 Woodruff Road','','Greenville','US-SC','SC','29169','803-791-9953','http://www.tls-sc.com','charles@tls-sc.com'),
	(41,'Sesco Lighting - TN','36.087556','-86.753225','4543 Trousdale Dr','','Nashville','US-TN','TN','37204','615-383-2030','http://www.sescolighting.com','ccannon@sescolighting.com'),
	(43,'Ert Lighting & Sales','29.544764','-98.494672','106 W. Turbo Dr.','','San Antonio','US-TX','TX','78216','210-366-1947','http://www.ertlighting.com','krystal@ertlighting.com'),
	(44,'Lighting Associates','29.801933','-95.564123','1557 W San Houston Pkwy','','Houston','US-TX','TX','77043','713-467-6436','http://www.laihouston.com','cdunn@laihouston.com'),
	(45,'NexGen Lighting Solutions','32.825381','-96.910893','2320 Hinton Dr.','','Dallas','US-TX','TX','76092','214-247-7415','nexgenlightingsolutions.com','bkenna@nexgenlightingsolutions.com'),
	(46,'Stevens Sales Co','40.760775','-111.845688','268 West 1530 South','','Salt Lake City','US-UT','UT','84115','801-487-8971','http://www.ssco.net','cindi@ssco.net'),
	(47,'Helfrich Agency Inc','36.531118','-82.249162','2569 Homeview Dr','','Bristol','US-VA','VA','23294','804-346-8818','http://www.helfrichlight.com','kay@helfrichlight.com'),
	(48,'Pacific Lighting Systems','47.544842','-122.325191','6363 7th Avenue South','Suite 100','Seattle','US-WA','WA','98108','206-436-8828','http://www.pacificlightingsystems.com','vrad@pacificlightingsystems.com'),
	(49,'Blankenship','47.675697','-117.326833','5517 East Trent Ave','','Spokane','US-WA','WA','99212','509-535-6006','http://www.blankenshipassoc.com','herbg@blankenshipassoc.com'),
	(50,'Mlazgar Associates','43.039018','-88.174829','720 Larry Ct.','','Waukesha','US-WI','WI','53183','(414) 943-1915','www.mlazgar.com','markm@mlazgar.com'),
	(51,'Apex Lighting Solutions','41.584188','-72.719812','446 Smith St','','Middleton','US-CT','CT','06457','860-632-8766','http://www.apexltg.com','WDietz@apexltg.com'),
	(55,'Prolux Lighting','53.561348','-113.627888','11214 178 St','','Edmonton','CA-AB','AB','T5S 1P2','780-454-3701','http://www.prolux.com','THydzik@prolux.com'),
	(56,'A.M Agencies','51.018579','-114.043406','118-4029-8th St SE','','Calgary','CA-AB','AB','T2G 3A5','403-243-1161','http://www.amagency.com','trudoski@amagency.com'),
	(57,'Douglas Lighting Controls','49.263686','-123.004405','4455 Juneau Street','','Burnaby','CA-BC','BC','V5C 4C4','877-873-2797','http://www.douglaslightingcontrols.com','customerservice@douglaslightingcontrols.com'),
	(58,'Integra Agencies','49.91395','-97.183064','301 Weston St','Unit E','Winnipeg','CA-MB','MB','R3E 3H4','204-694-1339','http://www.integraagencyltd.mb.ca','dean@integraagencyltd.mb.ca'),
	(59,'Vigilant Technical Sales','47.551309','-52.729069','28 Symonds Ave','','St. John\'s','CA-NF','NL','A1E 5B1','709-753-6685','http://www.vbnc.com',''),
	(60,'ELP Marketing','44.638322','-63.667227','32 McQuade Lake Crescent','','Halifax','CA-NS','NS','B3S 1B6','902-450-5155','http://www.elp.ns.ca','phore@elp.ns.ca'),
	(61,'Rainbow Lighting Agencies','43.69483','-79.494919','130 Industry St','Unit 11','North York','CA-ON','ON','M6M 5G3','416-604-8020','http://www.rainbow-lighting.com','dawnad@rainbow-lighting.com'),
	(62,'Gross Sales','42.988936','-81.225941','26 Glebe St','','London','CA-ON','ON','N1S 1P5','519-267-6262','http://www.grosssalesarchitecturallighting.com','jsmith.gsal@rogers.com'),
	(63,'Aland Enterprises','45.378616','-75.741419','1479 Laperriere Ave','2nd Fl','Ottawa','CA-ON','ON','K1Z 7SB','613-761-1188','http://www.alandent.com','bduff@alandent.com'),
	(64,'Douglas Lighting Controls','45.493102','-73.685052','349 Isabey ','','St. Laurnet','CA-QC','QC','H4T 1Y2','514-342-6581','http://www.douglaslightingcontrols.com','jmpaulin@douglaslightingcontrols.com'),
	(65,'For-Trem','46.797374','-71.330097','5220 Rue Rideau','','Quebec City','CA-QC','QC','G2E 6E4','418-877-1001','http://www.for-trem.ca','danielfortin@for-trem.ca'),
	(66,'Promellis','45.673745','-73.903626','99 √âmilien-Marcoux','Ste. #104','Blainville','CA-QC','QC','J7C 0B4','514-745-0259','http://www.promellis.com','mstonge@promellis.com'),
	(67,'Electra Sales','50.477768','-104.556161','1810 East Turvey Rd','','Regina','CA-SK','SK','S4B 2G7','306-791-0266','http://www.electrasalesltd.ca','justin@electrasalesltd.ca'),
	(68,'Philips Lighting San Diego','32.909718','-117.174142','6740 Top Gun Street','','San Diego','US-CA','CA','92121','844-268-2791','','maddy.kent@philips.com'),
	(69,'Oasis Lighting','43.677635','-116.669698','910 Teton Ave','','Caldwell','US-ID','ID','83605','208-477-3300','http://www.oasislightingassociates.com','mike@ola-ltg.com'),
	(70,'Philips Lighting Chicago','41.995763','-87.883866','10275 W. Higgins Rd','','Rosemont','US-IL','IL','60018','847-390-5111','','jim.livingstone@philips.com'),
	(71,'Curtis Stout Inc','30.427515','-91.068722','10611 Plaza Americana Drive','','Baton Rouge','US-LA','LA','71109','501-372-2555','http://www.chstout.com','info@chstout.com'),
	(72,'Stevens Sales Co','40.760775','-111.845688','268 West 1530 South','','Salt Lake City','US-WY','UT','84115','801-487-8971','http://www.ssco.net','philip@ssco.net'),
	(73,'George Lacey Sales','39.694915','-104.99885','1210 South Jason Street','','Denver','US-WY','CO','80223','303-390-8220','http://www.georgelaceysales.com','slacey@georgelaceysales.com'),
	(74,'Oasis Lighting','43.677635','-116.669698','910 Teton Ave','','Caldwell','US-WY','ID','83605','208-477-3300','http://www.oasislightingassociates.com','mike@ola-ltg.com'),
	(75,'ELP Marketing','46.077063','-64.829408','266 Edinburgh Drive','','Moncton','CA-NB','NB','E1E 4C7','506-862-1515','http://www.elp.ns.ca','plefrancois@elp.ns.ca'),
	(76,'Vigilant Technical Sales','47.551309','-52.729069','28 Symonds Ave','','St. John\'s','CA-NL','NL','A1E 5B1','709-753-6685','http://www.vigilanttechnicalsales.ca','jon@vigilanttechnicalsales.ca '),
	(77,'Prolux Lighting','53.561348','-113.627888','11214 178 St','','Edmonton','CA-NT','AB','T5S 1P2','780-454-3701','http://www.prolux.com','tdrader@prolux.com'),
	(78,'Prolux Lighting','53.561348','-113.627888','11214 178 St','','Edmonton','CA-NU','AB','T5S 1P2','780-454-3701','http://www.prolux.com','tdrader@prolux.com'),
	(79,'ELP Marketing','44.638322','-63.667227','32 McQuade Lake Crescent','','Halifax','CA-PE','PI','B3S 1B6','902-450-5155','http://www.elp.ns.ca','phore@elp.ns.ca'),
	(80,'Douglas Lighting Controls','49.265166','-123.001301','4455 Juneau Street','','Burnaby','CA-YT','YK','V5C 4C4','778-840-3778','http://www.douglaslightingcontrols.com','bcquotes@douglaslightingcontrols.com'),
	(81,'NRG Sales Llc','41.233649','-96.037989','8031 West Center Rd','Suite 100','Omaha','US-NE','NE','','402-515-9690','http://ww.nrgsales.com','jturek@nrgsales.com'),
	(83,'Luminosity Lighting Agency','40.6061951','-75.561321','4375 Parkland Dr','','Allentown','US-DE','PA','19047','215-480-4880','http://www.l2a.us','larry.fine@l2a.us'),
	(85,'Luminosity Lighting Agency','40.6061951','-75.561321','4375 Parkland Dr','','Allentown','US-NJ','PA','19047','215-480-4880','http://www.l2a.us','larry.fine@l2a.us'),
	(86,'AMA Lighting - Mobile','30.671936','-88.139009','813 Downtowner Blvd','Suite A','Mobile','US-MS','AL','36609','251-343-7693','http://www.amalighting.net','devani@amalighting.com'),
	(87,'Curtis Stout Inc','35.0538794','-89.950933','3478 Winhoma Dr','','Memphis','US-TN','TN','38118','901-632-1616','http://www.chstout.com','sterling@chstout.com'),
	(88,'Mlazgar Associates','44.860433','-93.408409','10340 Viking Drive','Suite 150','Eden Praire','US-ND','MN','55344','952-943-8080','http://www.mlazgar.com','markm@mlazgar.com'),
	(90,'Mlazgar Associates','44.860433','-93.408409','10340 Viking Drive','Suite 150','Eden Praire','US-SD','MN','55344','952-943-8080','http://www.mlazgar.com','markm@mlazgar.com'),
	(91,'Pinnacle Lighting','38.849671','-94.622348','15405 Aberdeen ','','Leawood','US-MO','KS','66224','816-590-6188','','roger@pinnaclelightingandcontrols.com'),
	(92,'Swoboda Lighting','38.67062','-121.368665','5740 Roseville Rd','Suite F','Sacramento','US-NV','CA','95842','916-521-8063','http://www.swobodalighting.com','larry@SwobodaLighting.com'),
	(93,'Pacific Lighting Systems','47.544842','-122.325191','6363 7th Avenue South','Suite 100','Seattle','US-AK','WA','98108','206-436-8828','http://www.pacificlightingsystems.com','vrad@pacificlightingsystems.com'),
	(94,'Blankenship','47.675697','-117.326833','5517 East Trent Ave','','Spokane','US-ID','WA','99212','509-535-6006','http://www.blankenshipassoc.com','herbg@blankenshipassoc.com'),
	(95,'Apex Lighting Solutions','41.584188','-72.719812','446 Smith St','','Middleton','US-MA','CT','06457','860-632-8766','http://www.apexltg.com','WDietz@apexltg.com'),
	(96,'Apex Lighting Solutions','41.584188','-72.719812','446 Smith St','','Middleton','US-ME','CT','06457','860-632-8766','http://www.apexltg.com','WDietz@apexltg.com'),
	(97,'Apex Lighting Solutions','41.584188','-72.719812','446 Smith St','','Middleton','US-NH','CT','06457','860-632-8766','http://www.apexltg.com','WDietz@apexltg.com'),
	(98,'Apex Lighting Solutions','41.584188','-72.719812','446 Smith St','','Middleton','US-RI','CT','06457','860-632-8766','http://www.apexltg.com','WDietz@apexltg.com'),
	(99,'Apex Lighting Solutions','41.584188','-72.719812','446 Smith St','','Middleton','US-VT','CT','06457','860-632-8766','http://www.apexltg.com','WDietz@apexltg.com'),
	(100,'Lighting Environments','39.2743815','-76.621648','175 West Ostend St','Suite A-2','Baltimore','US-MD','MD','21230','410-712-0239','http://www.lightingenvironments.com','anne@lightingenvironments.com'),
	(101,'Lighting Environments','39.2743815','-76.621648','175 West Ostend St','Suite A-2','Baltimore','US-DC','MD','21230','410-712-0239','http://www.lightingenvironments.com','anne@lightingenvironments.com'),
	(102,'One Source Lighting & Design','37.809197','-85.987519','604 Brown Street','','Vinegrove','US-KY','KY','40175','270-877-2221','http://www.2lightu.com','robemery@2lightu.com'),
	(103,'Repco II ','40.410155','-80.034390','1201 Chappel Ave','','Pittsburg','US-WV','WV','15216','412-562-6220','www.repcoii.com','johnnorthern@repcoii.com');

/*!40000 ALTER TABLE `repLocations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table repLocations_copy
# ------------------------------------------------------------

DROP TABLE IF EXISTS `repLocations_copy`;

CREATE TABLE `repLocations_copy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `locname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lng` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `web` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `repLocations_copy` WRITE;
/*!40000 ALTER TABLE `repLocations_copy` DISABLE KEYS */;

INSERT INTO `repLocations_copy` (`id`, `locname`, `lat`, `lng`, `address`, `address2`, `city`, `state`, `postal`, `phone`, `web`, `created_at`, `updated_at`, `email`)
VALUES
	(1,'EMC','42.978222','-90.63526460000003','1500 Lafollette St. ','','Fennifmore','WI','53809','608-822-3550','www.emccontrols.com','2014-09-09 16:23:21','2014-09-09 16:23:27','webmaster@emccontrols.com'),
	(2,'GLS','39.694676','-104.99886100000003','1210 S. Jason Street','','Denver','CO','80223','303-394-0220','http://www.georgelaceysales.com','2014-09-09 16:30:02','0000-00-00 00:00:00','sales@georgelaceysales.com'),
	(3,'Mlazgar','44.8745145','-93.40404439999998','7162 Shady Oak Rd','','Eden Prairie','MN','55344','952-943-8080','http://mlazgar.com','2014-09-09 16:34:54','0000-00-00 00:00:00','sales@mlazgar.com'),
	(4,'Prolux','53.5616463','-113.6273506','11214 - 178 Street ','','Edmonton','AB','T2C 5J3','403-444-5300','http://www.prolux.com','2014-09-09 16:36:58','0000-00-00 00:00:00','trebelo@prolux.com'),
	(5,'Prolux','50.951799','-113.96891649999998','#409, 4615 -112 Ave S.E. ','','Calgary','AB','T2C 5J3','403-444-5300','http://www.prolux.com','0000-00-00 00:00:00','0000-00-00 00:00:00','jbartlett@prolux.com'),
	(6,'Prudential Lighting','34.0166663',' -118.24157660000003','1737 E. 22nd Street ','','Los Angeles','CA','90058','213-746-0360 ','http://www.plpsocal.com','0000-00-00 00:00:00','0000-00-00 00:00:00','sales@plpsocal.com'),
	(7,'Prudential Lighting','34.068574','-117.28374410000004','1823A Commercenter Circle ','','San Bernardino','CA','92408','909-744-5280 ','http://www.plpsocal.com','0000-00-00 00:00:00','0000-00-00 00:00:00','sales@plpsocal.com'),
	(8,'Prudential Lighting','34.4224452','-119.67941459999997','22 North Milpas Street, Suite E ','','Santa Barbara','CA','93103','805-715-6400','http://www.plpsocal.com','0000-00-00 00:00:00','0000-00-00 00:00:00','sales@plpsocal.com'),
	(9,'Symmetry','49.2812125','-123.06441489999997','1955 E. Hastings St.','','Vancouver','BC','V5L 1T5','778-373-3377','www.symmetry-lighting.com','0000-00-00 00:00:00','0000-00-00 00:00:00','info@symmetry-lighting.com'),
	(10,'Lightscapes','41.250237','-74.30990789999998','2 Iron Forge Rd','','Warwick','NY','10990','845-504-2406','www.lightscapesautomation.com','0000-00-00 00:00:00','0000-00-00 00:00:00','brian@lightscapesautomation.com'),
	(11,'AM Agencies','51.017568','-114.04325470000003','118, 4029 - 8th Street S.E.','','Calgary','AB','T2G 3A5','403-243-1161','http://amagency.com','0000-00-00 00:00:00','0000-00-00 00:00:00','dprusky@amagency.com');

/*!40000 ALTER TABLE `repLocations_copy` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table similar_product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `similar_product`;

CREATE TABLE `similar_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned DEFAULT NULL,
  `similar_product_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `product_id_similar_product_id` (`product_id`),
  KEY `similar_product_id_product_id` (`similar_product_id`),
  CONSTRAINT `product_id_similar_product_id` FOREIGN KEY (`product_id`) REFERENCES `products_new` (`id`) ON DELETE CASCADE,
  CONSTRAINT `similar_product_id_product_id` FOREIGN KEY (`similar_product_id`) REFERENCES `products_new` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `similar_product` WRITE;
/*!40000 ALTER TABLE `similar_product` DISABLE KEYS */;

INSERT INTO `similar_product` (`id`, `product_id`, `similar_product_id`, `created_at`, `updated_at`)
VALUES
	(1,39,42,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(3,40,42,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(5,41,42,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(7,42,39,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(8,42,40,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(9,42,41,'0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `similar_product` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table specifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `specifications`;

CREATE TABLE `specifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `specifications` WRITE;
/*!40000 ALTER TABLE `specifications` DISABLE KEYS */;

INSERT INTO `specifications` (`id`, `name`, `description`)
VALUES
	(1,'Diversa','Diversa is a line of PIR (passive infrared) and Dual Technology occupancy and vacancy sensors designed to meet your market requirements. Diversa Dual Technology sensors use PIR and proprietary ADI-Voice technology to identify room occupancy and vacancy. Diversa sensors are available in corner mount, recessed ceiling and wall switch models and support 120/277Vac, 347Vac and low voltage 24Vac applications. The specification document below is designed to help you develop specifications for Diversa sensors.'),
	(2,'Dialog System','The Dialog lighting control system utilizes networking technology connecting relay panels, switches and sensors based upon a 2-wire data line providing both power and data to all devices. The network is free topology to achieve maximum network distance. The system uses a web server device complete with a touch screen located in a relay panel so that programming and viewing of status can be accomplished at the panel or by any PC/laptop connected to the same LAN or via the internet.'),
	(3,'Standard Pre-assembled Products','Hardwired Low Voltage Lighting Controls utilizing 2 wire low voltage switches to control the Douglas relays. Simple Scanners and Programmable Scanners may be used to provide multiple relay switching for master ON/OFF controls. Additional controls such as photocells, occupancy sensors, time clocks, etc are also available. Panels equipped with programmable scanners and nodes can also be networked together using a LonWorks data bus.'),
	(7,'LitePak Control Panels','LitePak Control Panels are a cost effective panel targeted for small applications. The LitePak panels use the 20A lighting load rated Douglas relays and are controlled with a built-in full featured photo/astro/timer controller. Optionally override switches can be connected and expansion panels can be added.'),
	(9,'Native BACNet Networked Systems','Networkable system linking the Building Automation System with Lighting Controls using Native BACNet network nodes.'),
	(11,'Dialog Room Controller','<p>Easy to install, easy to use, the Dialog Room Controller for lighting and receptacle control of stand-alone offices and classrooms is a Plug \'N Control ready, out-of-the-box system that is configured, labelled and tested before shipment. Systems include: Dialog Room Controller, Occupancy and Daylight sensors and Wall Stations.</p>\n<p>The <em>Dialog Room Controller</em> system can also be used in a centralized, fully networked Dialog system. Select the appropriate kits, connect to the Dialog network controller (WLC-3150) and facility wide control capability is enabled. Advantages include global control and scheduling, faster installations, less long wire/conduit runs, and room-by-room commissioning before centralized network connections are completed. </p>');

/*!40000 ALTER TABLE `specifications` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table specifications_documents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `specifications_documents`;

CREATE TABLE `specifications_documents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `specification_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `spec_id_foreign` (`specification_id`),
  CONSTRAINT `specification_foreign_key` FOREIGN KEY (`specification_id`) REFERENCES `specifications` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `specifications_documents` WRITE;
/*!40000 ALTER TABLE `specifications_documents` DISABLE KEYS */;

INSERT INTO `specifications_documents` (`id`, `name`, `path`, `specification_id`)
VALUES
	(1,'Diversa Specification (.docx)','Specifications-Diversa PIR-DT 2014-07-16.docx',1),
	(2,'Diversa Specification (.pdf)','Specifications-Diversa PIR-DT 2014-07-16.pdf',1),
	(3,'Dialog Specification (.docx)','Specifications-Dialog-20150722.docx',2),
	(4,'Dialog Specification (.pdf)','Specifications Dialog 20150722.pdf',2),
	(5,'Standard Pre-assembled Product Specification (.doc)','DLC-2010-1_Hardwired_Spec.doc',3),
	(6,'Standard Pre-assembled Product Specification (.dwg)','Simple Hardwired Spec.dwg',3),
	(11,'LitePak Control Panels Specification (.doc)','DLC-2010-6_LitePak_Spec.doc',7),
	(12,'LitePak Control Panels Specification (.dwg)','LitePak Template.dwg',7),
	(14,'Native BACNet Networked Systems Specification (.doc)','DLC-2010-7b_BACnet_Spec.doc',9),
	(18,'Dialog Room Controller Full Specification (.doc)','DialogRoomController-FullSpecv03-2015-06-02.doc',11),
	(19,'Dialog Room Controller Full Specification (.pdf)','DialogRoomController-FullSpecv03-2015-06-02.pdf',11);

/*!40000 ALTER TABLE `specifications_documents` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) DEFAULT '1',
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `isactive`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'nickfranciosi@gmail.com',1,'$2y$10$ojOuKF5jSuxs8Wb0FrdL3.zZXSkZ8cLUYu36h3BEC9wyT5ESZ1Qi.','PdQxDOuCstaeHNBUAITBUmvK9IMx2AYZtRrpafMT1740t09o9qzjJho4amWU','2014-07-30 18:58:37','2015-01-21 16:32:12'),
	(2,'cholstein@unvlt.com',1,'$2y$10$0TNru8YKefp/eBF9EGhnoeggkfSabONqJ3v3uQcXxBGFMKpUoW4rK','qG6way908AnInJR6PtAH1eZ5w4Xw47TNjd7lNsJwNOpeAjSZSs4X8pKvqCTX','2014-09-08 13:58:16','2014-12-05 21:31:31'),
	(3,'C2L Trusted',1,'$2y$10$.i9ed75CPj7h3oK2OOY7xujfU9CyjvYQgzPmPKzjfKcyOqmi9P8aO','TcBnSV7VmD28XVRBpKm5JyNJeEdUruiGTfOSAd0sfv0yITv9J4cudmeyyAm4','2015-01-16 14:55:35','2015-01-19 16:16:49');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
