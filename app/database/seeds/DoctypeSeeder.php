<?php

class DoctypeSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $documentType = Doctype::create(array(
            'name' => 'BIM'
        ));

        $documentType2 = Doctype::create(array(
            'name' => 'Instalation Instructions'
        ));

        $documentType3 = Doctype::create(array(
            'name' => 'IES'
        ));

        $documentType4 = Doctype::create(array(
            'name' => 'Spec Sheet'
        ));

        $documentType5 = Doctype::create(array(
            'name' => 'App Note'
        ));

        $documentType6 = Doctype::create(array(
            'name' => 'BIM'
        ));
    }

}