<?php

class CertificationSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $documentType = Certification::create(array(
            'name' => 'CSACUS',
            'thumbnail' => 'csacus.jpg',
            'description' => 'logo for certifications'
        ));

        $documentType2 = Certification::create(array(
            'name' => 'Energy Star',
            'thumbnail' => 'energy_star.jpg',
            'description' => 'logo for certifications'
        ));

        $documentType3 = Certification::create(array(
            'name' => 'Lighitng Facts',
            'thumbnail' => 'lighting_facts.jpg',
            'description' => 'logo for certifications'
        ));

    }

}