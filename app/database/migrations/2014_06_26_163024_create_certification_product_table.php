<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCertificationProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('certification_product', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('certification_id')->length(10)->unsigned()->index();
			$table->foreign('certification_id')->references('id')->on('certifications')->onDelete('cascade');
			$table->integer('product_id')->length(10)->unsigned()->index();
			$table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('certification_product');
	}

}
