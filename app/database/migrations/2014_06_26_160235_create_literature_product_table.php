<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLiteratureProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('literature_product', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('literature_id')->length(10)->unsigned()->index();
			$table->foreign('literature_id')->references('id')->on('literature')->onDelete('cascade');
			$table->integer('product_id')->length(10)->unsigned()->index();
			$table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('literature_product');
	}

}
