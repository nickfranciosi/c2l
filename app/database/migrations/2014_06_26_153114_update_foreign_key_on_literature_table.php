<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateForeignKeyOnLiteratureTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('literature', function(Blueprint $table)
		{
			$table->foreign('doctype_id')->references('id')->on('doctype');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('literature', function(Blueprint $table)
		{
			$table->dropForeign('doctype_id');
		});
	}

}
