<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLiteratureTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('literature', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('doctype_id')->lenth(10);
			$table->string('name');
			$table->text('description');
			$table->string('path');
			$table->string('thumbnail');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('literature');
	}

}
