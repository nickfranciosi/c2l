@extends('layouts.default')


@section('content')
    {{Breadcrumbs::render('applications')}}
    <div class="container" id="applications">
      <div class="app-top">
        <h1>Applications</h1>
        <p>From single-room offices to multi-building corporate environments, Douglas Lighting Controls has been called upon to provide solid, reliable lighting control systems for over 50 years. Within the application groups below we’ve provided a short list of projects that use our lighting controls. We ship over 1500 systems from our North American production facility each year, so chances are we’ve designed a system to meet your requirements.</p>
        <p>If you don’t see an example in the list below, just give us a call&mdash;we’re always happy to discuss your project with you and find the right solution.</p>
      </div>
      <div class="application-carousel">
        @include('applications.carousel')
      </div>
      <a href="/spotlights" style="text-decoration: none;">
        <h1 style="margin-bottom: 40px; top: -75px; position: relative;">Project Spotlights &mdash; Case Studies</h1>
      </a>
    </div>
@stop
