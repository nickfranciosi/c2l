<div id="carouselContainer">
    <div id="productCarousel" class="owl-carousel">
      @foreach($carouselItems as $carouselItem)
      <div class="productItem"> 
        <img data-src="{{ asset('images/applications/thumbnail/' . $carouselItem->thumbnail )}}" alt="" class="lazyOwl">
        <h4>{{ $carouselItem->name }}</h4>
        <p>{{ Str::limit($carouselItem->tagline, 150) }}</p>
        <div class="buttonWell">
            <a href="{{ route('single_application', [$carouselItem->url]) }}" class="viewOverview">Read More</a>
        </div>
      </div>
    @endforeach
    </div>
</div>
