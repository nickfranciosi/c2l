@extends('layouts.default')


@section('content')
    {{Breadcrumbs::render('single_application', $application)}}
    <div class="container singleApplication">
        <div class="app-top">
          <div class="square--two-up">
              <img src="{{ asset('images/applications/hero/' . $application->hero) }}" alt="">
          </div>
          <div class="app-info square--two-up">
              <h2>{{ $application->name }}</h2>
              <p> {{ $application->tagline }}</p>
              <p> {{ $application->description }} </p>
              <div class="buttonWell">
                  <a href="{{ asset('resources/application_grids/' . $application->pdf_grid) }}" class="button" target="_blank">Projects Detail</a>
              </div>

          </div>
          <div class="clearfix"></div>
        </div>
        <div class="app-list">
            <h3>Projects List</h3>
            <ul class="ovalList">
                @foreach($application->applications as $app)
                    <li>{{ $app->name }} | {{ $app->info_2 }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@stop
