@extends('layouts.default')

@section('content')
  <div class="container clear-header-xlarge" id="oneline"> 
    <h1>One Line Drawings</h1>
    <div class="slab-bottom">
      <div class="square--two-up" onload="fillsystem();">
        <form class="form" name="onelinegen" method="POST" >
          <fieldset>
            <h2>Select your options here</h2>
            <div class="form-group">
              <label>System Type:</label>
              <select NAME="system" id="system">
                <Option value="0" >Please select a system:</option>
              </select>
            </div>
            <div class="form-group">
              <label>Model:</label>
              <select id="model" NAME="model"></select>
            </div>
            <div class="form-group">
              <label>Riser Style:</label>
              <select id="riserstyle" NAME="riserstyle"></select>
            </div>
            <div class="form-group">
              <label># of Floors:</label>
              <select id="floors" NAME="floors"></select>
            </div>
            <div class="form-group">
              <label># of Risers:</label>
              <select id="risers" NAME="risers"></select>
            </div>
            <div class="form-group">
              <label># of Panels per Floor:</label>
              <select id="panels" NAME="panels" ></select>
            </div>
            <div class="form-group">
              <label>Total Panels:</label>
              <input id="total" name="total" type="text" disabled="true" readonly="true" >
            </div>
          </fieldset>
        </form>
      </div>
      <div class="square--two-up">
        <form class="form preview">
          <fieldset>
              <h2>Preview</h2>
              <img  id="Preview" src="{{ asset('images/oneline/blank.jpg') }}">
              <input name="Generate" type="submit" value="Generate" onClick="popit()">
          </fieldset>
        </form >
      </div>
    </div>
  </div>
@stop

@section('extra_scripts')
  <script src="{{ asset('js/douglas-oneline.js') }}"></script>
  <script>
    window.onload = function(){fillsystem();};
    $('form').on('submit', function(e){
      e.preventDefault();
    });
  </script>
@stop
