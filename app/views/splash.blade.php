<html class="no-js"><head>
    <meta charset="utf-8">
    <title>C2L</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" href="{{ asset('css/component.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="icon" type="image/png" href="{{ asset('images/favicon.png') }}">
  </head>
  <body class="splash" style="background: rgb(255, 255, 255);">
      <div style="text-align: center;margin-top:80px;">
        <img src="images/splash_logo.png" alt="" width="600px">
        <h1>Coming Soon</h1>
          <div class="column">
            <h3>For more information on C2L or the Space Player</h3><a class="button" href="mailto:CHolstein@unvlt.com?Subject=Request%20For%20C2L%20Information">Click Here</a>
          </div>
          <div class="column">
            <h3>Looking for the Space Player? Click the Logo</h3>
            <a href="http://unvlt.com/products/space-player/" target="_blank"><img src="images/spaceplayer_splash.png" ></a>
          </div>
          
          <div class="column">
            <h3>Trusted Partners</h3><a class="button" href="/protected">Click Here</a>
          </div>
        <div id="copyright">
          <p style="font-family: Arial; font-size: 11px;">&copy; 2015 Universal Lighting Technologies, All Rights Reserved.</p>
        </div>
      </div>

    <script async="" src="//www.google-analytics.com/analytics.js"></script><script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/components/jquery.js"><\/script>');</script>
    <script src="assets/js/scripts.min.js"></script>

    <script>
    (function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
    (f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
    l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-XXXXXXXX-XX');
    ga('send', 'pageview');
    </script>

  
</body></html>