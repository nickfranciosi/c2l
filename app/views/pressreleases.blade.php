@extends('layouts.default')


@section('content')
    @include('partials.aboutBanner')
    <div class="slab">
        <div class="container" id="about">
          <div class="press-releases">
            <h1>Press Releases</h1>
            <h3>2017</h3>
            <ul>
              <li><p><a class="download" href="/resources/press_release/Douglas Dialog Lighting Control Unit Press Release - FINAL.pdf">Douglas Lighting Controls Introduces Next Generation Lighting Control Unit </a></p></li>
            </ul>
            <ul>
              <li><p><a class="download" href="/resources/press_release/Panasonic%20Michiura%20visit%202017%20-%20FINAL.pdf">Panasonic Lighting Americas Hosts Global Lighting Business Division</a></p></li>
            </ul>
            <ul>
              <li><p><a class="download" href="/resources/press_release/PESLA LightFair 2017 - FINAL.pdf">Panasonic Lighting Americas Reveals New Products and Innovations
at LIGHTFAIR<sup>®</sup> International</a></p></li>
            </ul>
            <ul>
              <li><p><a class="download" href="/resources/press_release/Douglas DRC 2 Release - FINAL.pdf">Douglas Lighting Controls Unveils Dialog Room Controller 2</a></p></li>
            </ul>
            <ul>
              <li><p><a class="download" href="/resources/press_release/Douglas_Wireless Controls release - FINAL.pdf">Douglas Lighting Controls Introduces Bluetooth<sup>®</sup> Wireless Lighting Control System</a></p></li>
            </ul>
            <ul>
              <li><p><a class="download" href="/resources/press_release/Douglas%20New%20Office%20Release%20-%20FINAL.pdf">Douglas Lighting Controls Opens New Office Space in Canada</a></p></li>
            </ul>
            <h3>2015</h3>
            <ul>
              <li><p><a class="download" href="/resources/press_release/Douglas%20Lighting%20Controls%20Introduces%20Stand-Alone%20Room%20Controller.pdf">Douglas Lighting Controls Introduces Stand-Alone Room Controller</a></p></li>
            </ul>
            <h3>2014</h3>
            <ul>
              <li><p><a class="download" href="/resources/press_release/Douglas%20Lighting%20Controls%20Extends%20Diversa%20Occupancy%20Sensors%20Line.pdf">Douglas Lighting Controls Extends Diversa Occupancy Sensors Line</a></p></li>
            </ul>
          </div>

          <div class="videos">
            <h1>Videos</h1>
            <br />
            <ul>
              <li>
                <p><a target="_blank" href="https://www.youtube.com/watch?v=dbxzvhM5f0U&t=1s">Dialog Bluetooth Wireless Solutions</a></p>
              </li>
              <li>
                <p><a target="_blank" href="https://www.youtube.com/watch?v=ixHqskjQrM0&t=7s">Dialog Centralized Control System</a></p>
              </li>
            </ul>
          </div>

        </div>
    </div>
@stop
