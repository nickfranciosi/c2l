@extends('layouts.default')


@section('content')
    @include('partials.aboutBanner')
    <div class="slab">
        <div class="container" id="about">
          <div class="about-links">
            <h1><a target="_blank" href="/pressreleases">Press Releases &amp; Videos</a></h1>
            <h1><a target="_blank" href="/pdf/CompanyProfile.pdf">Company Profile</a></h1>
            <h1><a target="_blank" href="https://tinyurl.com/m2r4jun">Virtual Tour</a></h1>
          </div>
          <h2>About Us</h2>
          <p>Since 1962 <strong>Douglas Lighting Controls, a leading</strong> manufacturer and supplier of lighting control equipment for the United States and Canada, has achieved many firsts in lighting control:</p>
          <p>In the early <strong>1960s</strong>, Douglas distributed the 2-wire relay, switches and panels for field assembly. Into the latter half of the decade, Douglas began to manufacture and install the first programmable lighting control system in North America using telecom technology.</p>
          <p>The <strong>1970s</strong> marked a new era for Douglas Lighting Controls as we would go on to pioneer pre-assembled panels and promote pre-assembled lighting control systems as the standard for new construction projects.</p>
          <p>In the <strong>1980s</strong>, Douglas incorporated emerging computing technology and interfaces into its lighting control system. These advanced solutions, placing Douglas on the forefront of innovation, added graphical control and data logging programs to our repertoire.</p>
          <p>As various technologies made their way into the fabric of the controls industry in the <strong>1990s</strong>, Douglas Lighting Controls adopted the LonWorks control technology to a number of our control products and officially joined the LonMark Lighting Control Standards group.</p>
          <p>Building on past technological success, the 21st century ushered in product innovations tailored for a world connected through the Web. The internet has simplified tasks and increased the accessibility for many control industries, including lighting controls. Adapting in the early <strong>2000s</strong>, Douglas Lighting Controls added internet control and web access to our LonWorks lighting control product group.</p>
          <p>In 2010, Douglas Lighting Controls was purchased by Panasonic Lighting Americas and operates today as a member of the Panasonic Group. Fundamentally our business has not changed – we still provide industry leading solutions for lighting control challenges. With the support of Panasonic, we have the backing of a global enterprise to complement our industry expertise.</p>
          <p><strong>Douglas Lighting Controls</strong> continues to solve lighting control problems by engineering easy to install and use, centralized and decentralized digital lighting control systems for schools, offices, commercial buildings, campuses, and sports complexes throughout North America. Douglas systems include relays panels, controllers, occupancy/vacancy sensors, daylight sensors and wall switch stations. Each system is built to order in our North American production facility and prior to shipment, each system is programmed and tested to ensure the system installation and commissioning is as fast and easy as possible. We also provide services for project/system drawing, technical support, and onsite system commissioning. With more than 50 years in operation, we are recognized for our deep understanding of lighting control market and ability to provide the right solution for each facility.</p>
        </div>
    </div>
@stop
