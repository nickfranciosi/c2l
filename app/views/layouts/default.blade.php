<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <title>Douglas Lighting Control</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('css/component.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="icon" href="{{ asset('images/favicon.ico') }}">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    @include('partials.scriptsHeader')
</head>
<body>
  @include('partials.header')

  @yield('content')

  @include('partials.footer')

  <script  src="{{ asset('js/bundle.js') }}"></script>

  @yield('extra_scripts')

  @if(Session::has('message'))
    <script type="text/javascript">
      var message = {{"'" . Session::get('message') . "'"}};
      $(function(){
        humane.log(message, {timeout: 8000, clickToClose: true });
        console.log(message);
      });
    </script>
  @endif

  <script>
    console.log('env: {{ App::environment() }}')
  </script>

  @if (App::environment('production'))
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-1835862-5', 'auto');
      ga('send', 'pageview');

    </script>
  @endif
</body>
</html>
