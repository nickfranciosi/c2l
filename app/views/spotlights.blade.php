@extends('layouts.default')


@section('content')
    @include('partials.aboutBanner')
    <div class="slab">
        <div class="container" id="about">
          <h1>Project Spotlights &mdash; Case Studies</h1>
          <ul class="spotlight-list">
            <li class="single-spotlight">
              <img src="/images/Project%20Spotlight_Douglas%20HQ_web-thumb.png" alt="Spotlight Document thumbnail image">
              <a href="#"><p class="spotlight-title">Douglas Lighting Controls Retrofits Centre of Excellence with Own Solutions, Sees Immediate Payback</p></a>
            </li>
          </ul>
        </div>
    </div>
@stop

<style media="screen">

  .single-spotlight {
    display: flex;
  }

  .single-spotlight img {
    width: 120px;
    height: 150px;
    object-fit: cover;
    margin-right: 20px;
  }

  .single-spotlight a {
    max-width: 300px;
  }
  
</style>
