@extends('layouts.default')

@section('content')
  <div id="spec" class="container clear-header-xlarge">
    <h1>Specifications</h1>
    <div class="specContainer">
      @foreach($specifications as $spec)
        <div class="specItem">
          <h2>{{ $spec->name }}</h2>
          <p>{{ $spec->description }}</p>
          <ul class="">
            @foreach($spec->specification_documents as $doc)
              <li><a href="{{ asset('resources/specifications/' . $doc->path) }}" target="_blank">{{ $doc->name }}</a></li>
            @endforeach
          </ul>
        </div>
      @endforeach
    </div>
  </div>


@stop
