@extends('layouts.default')


@section('content')
    <div class="container clear-header-xlarge" id="contactPage">
        <h1>Contact Us</h1>
        <p>Feel free to contact us with any questions you might have about Douglas Lighting Controls.</p>
        <div id="form" class="square--two-up">
            {{ Form::open(['route' => 'contact','data-parsley-validate']) }}
            <div class="form-group">
                {{ Form::label('name', 'Name')}}
                {{ Form::text('name',null , ['required']) }}
                {{ $errors->first('name', '<span class="error">:message</span>') }}
            </div>

            <div class="form-group">
                {{ Form::label('email', 'Email')}}
                {{ Form::email('email', null, ['required']) }}
                {{ $errors->first('email', '<span class="error">:message</span>') }}
            </div>

             <div class="form-group">
                {{ Form::label('phone', 'Phone')}}
                {{ Form::text('phone', null, ['required']) }}
                {{ $errors->first('phone', '<span class="error">:message</span>') }}
            </div>

            <div class="form-group">
                {{ Form::label('question', 'Questions/Comments')}}
                {{ Form::textarea('question', null, ['rows' => '8', 'required']) }}
            </div>
                {{ Honeypot::generate('my_name', 'my_time') }}
            <div class="form-group">
                {{ Form::submit('Send') }}
            </div>

            {{ Form::close() }}
        </div>
        <div id='mapInfo' class="square--two-up">
            <div id="mapContainer">
                <iframe width="518" height="530" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=280-3605%20Gilmore%20Way%2C%20Burnaby%2C%20BC%20V5G%204X5%2C%20Canada&key=AIzaSyCwAOSTl9Jxkxe6KCrcG57SxMJEGHKDyAg"></iframe>
            </div>
            <ul>
                <li>280-3605 Gilmore Way, Burnaby, BC V5G 4X5, Canada</li>
                <li>Email: <a href="mailto:customerservice@douglaslightingcontrols.com">customerservice@douglaslightingcontrols.com</a></li>
            </ul>
        </div>

        <br clear="both">

        <div class="contact-cards">
          <div>
            <p>
              Technical Support (English)<br />
              p. 877-873-2797 ext. 1<br />
              <a href="mailto:techsupport@douglaslightingcontrols.com">techsupport@douglaslightingcontrols.com</a>
            </p>
          </div>

          <div>
            <p>
              Technical Support (French)<br />
              p. 514-342-6581<br />
              <a href="mailto:techsupport@douglaslightingcontrols.com">techsupport@douglaslightingcontrols.com</a>
            </p>
          </div>

          <div>
            <p>
              Customer Service (English)<br />
              p. 877- 873-2797<br />
              <a href="mailto:customerservice@douglaslightingcontrols.com">customerservice@douglaslightingcontrols.com</a>
            </p>
          </div>

          <div>
            <p>
              Customer Service (French &amp; Eastern Canada)<br />
              p. 514-342-6581<br />
              <a href="mailto:customerservice@douglaslightingcontrols.com">customerservice@douglaslightingcontrols.com</a>
            </p>
          </div>

          <br clear="both">
          <h2>USA</h2>

          <div>
            <p>
              Brad Stevenson, Vice President, Sales<br />
              p. 949-466-8889<br />
              <a href="mailto:bstevenson@douglaslightingcontrols.com">bstevenson@douglaslightingcontrols.com</a>
            </p>
          </div>

          <div>
            <p>
              Jeff Pease, Director of US Specification Sales<br />
              CT, IA, IL, IN, KY, MA, MI, MN, MO, OH, WI, Southern NJ<br />
              p. 262-391-9267<br />
              <a href="mailto:jpease@douglaslightingcontrols.com">jpease@douglaslightingcontrols.com</a>
            </p>
          </div>

          <div>
            <p>
              Buddy Combs, Regional Sales Manager<br />
              AL, AR, FL Panhandle, LA, NM, OK, Western TN, TX<br />
              p. 405-686-8159<br />
              <a href="mailto:bcombs@douglaslightingcontrols.com">bcombs@douglaslightingcontrols.com</a>
            </p>
            <br />
          </div>

          <div>
            <p>
              Wendy Hammatt, Regional Sales Manager<br />
              CO, ID, KS, MT, NE, OR, WA, WY, Western IA, Western MO<br />
              p. 206-380-5461<br />
              <a href="mailto:whammatt@douglaslightingcontrols.com">whammatt@douglaslightingcontrols.com</a>
            </p>
          </div>

          <div>
            <p>
              Branden Jamison, Regional Sales Manager<br />
              AZ, CA, NV, UT, Mexico<br />
              p. 909-942-0213<br />
              <a href="mailto:bjamison@douglaslightingcontrols.com">bjamison@douglaslightingcontrols.com</a>
            </p>
          </div>

          <div>
            <p>
              Jim Phelan, Regional Sales Manager<br />
              NY, VA, WV, Northern NJ, Western PA<br />
              p. 412-759-8388<br />
              <a href="mailto:jphelan@douglaslightingcontrols.com">jphelan@douglaslightingcontrols.com</a>
            </p>
            <br />
          </div>

          <div>
            <p>
              Thomas Rose, Regional Sales Manager<br />
              FL, GA, MD, NC, SC, Eastern PA, Central & Eastern TN<br />
              p. 865-603-7864<br />
              <a href="mailto:trose@douglaslightingcontrols.com">trose@douglaslightingcontrols.com</a>
            </p>
          </div>

          <br clear="both">
          <h2>CANADA</h2>

          <div>
            <p>
              Jean-Marc Paulin, Regional Sales Manager<br />
              NF, NS, NB, PEI, ON (Ottawa only), QC<br />
              p. 514-342-6581 ext.230<br />
              <a href="mailto:jmpaulin@douglaslightingcontrols.com">jmpaulin@douglaslightingcontrols.com</a>
            </p>
          </div>

          <div>
            <p>
              Steve Kostidis, Director of Canadian Sales<br />
              AB, MB, ON, SK, NWT<br />
              p. 905-330-2611<br />
              <a href="mailto:skostidis@douglaslightingcontrols.com">skostidis@douglaslightingcontrols.com</a>
            </p>
          </div>

          <div>
            <p>
              Paul Cleary, Regional Sales Manager<br />
              All BC, YK<br />
              p. 778-840-3778<br />
              <a href="mailto:pcleary@douglaslightingcontrols.com">pcleary@douglaslightingcontrols.com</a>
            </p>
          </div>
        </div>
    </div>
@stop
