@extends('layouts.default')


@section('content')
        {{Breadcrumbs::render('search')}}

        <div id="searchResults" class="container">
            <h2>Search Results for <span class="userSearch">{{{$query}}}</span></h2>
            <div class="search">
                {{ Form::open(['route' => 'search_results', 'id' => 'searchAgain']) }}
                {{ Form::text('query', null, ['placeholder' => 'Search Again']) }}
                {{ Form::submit('Search')}}
                {{ Form::close() }}
            </div>
            <div id="productResults" class="square--two-up">
                <h3>Products</h3>
                <ul>
                @if($productsFound->isEmpty())
                    <li>No results found</li>
                @endif
                @foreach($productsFound as $product)
                    <li>
                        <h4><a href="{{ route('single_product',[$product->url])}}">{{ $product->name}}</a></h4>
                        <p>{{ $product->description}}</p>
                        <a class="readMore"href="{{ route('single_product',[$product->url])}}">Read more</a>
                    </li>
                @endforeach
                </ul>
            </div>
            <div id="litResults" class="square--two-up">
                <h3>Literature</h3>
                <ul>
                @if($literatureFound->isEmpty())
                    <li>No results found</li>
                @endif
                @foreach($literatureFound as $literature)
                    <li>
                        <h4><a href="{{ asset('resources' . '/'. $literature->doctype->folder . '/' . $literature->path) }}">{{ $literature->name}}</a></h4>
                    </li>
                @endforeach
                </ul>
            </div>

        </div>
@stop
