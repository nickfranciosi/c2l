@extends('layouts.default')

@section('content')
  <div class="container clear-header" id="sitemap">
    <h1>Sitemap</h1>
    <div class="square--three-up">
      <h2>Menu</h2>
      <ul class="ovalList">
        <li><a href="{{ route('home') }}">Home</a></li>
        <li><a href="{{ route('solutions') }}">Solutions</a></li>
        <li><a href="{{route('applications')}}">Applications</a></li>
        <li><a href="{{ route('about') }}">About</a></li>
        <li><a href="{{ route('support') }}">Support</a>
        <li><a href="{{route('rep_locator')}}">Rep Locator</a></li>
        <li><a href="{{route('oneline')}}">Oneline Drawings</a></li>
        <li><a href="{{route('specifications')}}">Specifications</a></li>
      </ul>
    </div>
    <div class="square--three-up">
      <h2>Products</h2>
        <ul class="ovalList">
          @foreach($product_types as $type)
              @if($type->has_subcategory)
                  <li><a href="{{ route('filterPage', $type->url) }}" alt="{{ $type->name }}">{{ $type->name }}</a></li>
              @elseif($type->name === 'Space Player')
                  <li><a href="{{ $type->url }}" target="_blank" alt="{{ $type->name }}">{{ $type->name }}</a></li>
              @else
                  <li><a href="{{ route('single_product', $type->url) }}" alt="{{ $type->name }}">{{ $type->name }}</a></li>
              @endif
          @endforeach
        </ul>
    </div>
    <div class="square--three-up">
      <h2>Applications</h2>
        <ul class="ovalList">
          @foreach($application_types as $app)
            <li><a href="{{ route('single_application', [$app->url]) }}">{{ $app->name }}</a></li>
          @endforeach
        </ul>
    </div>
  </div>
@stop
