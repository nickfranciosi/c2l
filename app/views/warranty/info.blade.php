@extends('layouts.default')


@section('content')
    <div class="container" id="warrantyInfo">
        <h2>Warranty Info</h2>
        <div class="warrantySection">
            
            <h4><i class="fa fa-file-pdf-o"></i> Warranty Download (English)</h4>
            <a href="{{ asset('resources/warranties/Luminaire_Warranty.pdf')}}">Download</a>
        </div>
        <div class="warrantySection">
            
            <h4><i class="fa fa-file-pdf-o"></i> Warranty Download (French)</h4>
            <a href="{{ asset('resources/warranties/Luminaire_Warranty_French.pdf')}}">Download</a>
        </div>
         <div class="warrantySection">
            
            <h4><i class="fa fa-file-pdf-o"></i> Warranty Download (Spanish)</h4>
            <a href="{{ asset('resources/warranties/Luminaire_Warranty_Spanish.pdf')}}">Download</a>
        </div>
    </div>
@stop