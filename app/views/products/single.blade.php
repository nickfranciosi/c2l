@extends('layouts.default')


@section('content')
    {{Breadcrumbs::render('single_product', $product)}}
    <div id="snapshot" class="container">
        @if ($product->hero)
            <div class="productImage square--two-up">
                <img src="{{ asset('images/products/heros/' . $product->hero) }}" alt="">
            </div>
        @endif
        <div class="productInfo square--two-up">
            <h2>{{ $product->name }}</h2>
            <p> {{ $product->description }}</p>
            {{ $product->description_additional }}
            <div class="buttonSection">
                <!-- <p>Working on a complete C2L system?</p> -->
                <a href="{{ route('rep_locator') }}">Find Your Rep</a>
            </div>

            {{-- This is a section for content that will be only for specific products --}}

            {{-- Downlights besides SD8 --}}
            @if ($product->product_type_id == 1 && !in_array($product->id, array(3,5)))
                <!-- <div class="buttonSection"> -->
                <!--     <a href="#dfk_modal" class="fancybox">Drywall Frame Kit</a> -->
                <!--     <div id="dfk_modal" style="display: none;"> -->
                <!--         <div class="imageHolder"> -->
                            {{-- <img src="{{ asset('images/products/dfk_loop.gif')}}" alt=""> --}}
                <!--         </div> -->
                <!--         <div class="infoHolder"> -->
                <!--             <ul class="blueOvalList"> -->
                <!--                 <li><span>Accommodates ceilings up to 1&#38;Prime; thick.</span></li> -->
                <!--                 <li><span>Light commercial bar hangers included.  14&#45;26&#38;Prime; extension range.</span></li> -->
                <!--                 <li><span>Bar hanger brackets adjust 2&#45;3/4&#38;Prime; vertically &#38;amp; also accommodate 3rd party bar hangers.</span></li> -->
                <!--                 <li><span>SD4 and DFK4 are used for demonstration purposes.</span></li> -->
                <!--             </ul> -->
                <!--         </div> -->
                <!--     </div> -->
                <!-- </div> -->

            {{-- SpacePlayer --}}
            @elseif($product->product_type_id == 2)
               <!-- <div class="buttonSection"> -->
               <!--      <a href="#spaceplayer_modal" class="fancybox">Play Video</a> -->
               <!--      <div id="spaceplayer_modal" style="display: none;">  -->
               <!--          <video class="video&#45;js vjs&#45;default&#45;skin" -->
               <!--            controls preload="auto"  -->
               <!--            poster="{{ asset('videos/SpacePlayer.jpg')}}" -->
               <!--           > -->
               <!--               <source src="{{ asset('videos/SpacePlayer.mp4')}}" type='video/mp4' /> -->
               <!--               <source src="{{ asset('videos/SpacePlayer.webm')}}" type='video/webm' /> -->
               <!--               <source src="{{ asset('videos/SpacePlayer.oggtheora.ogv')}}" type='video/ogg' /> -->
               <!--               <p class="vjs&#45;no&#45;js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5&#45;video&#45;support/" target="_blank">supports HTML5 video</a></p> -->
               <!--          </video> -->
               <!--  -->
               <!--      </div> -->
               <!--  </div> -->
            @else

            @endif

        </div>

    </div>
    <div id="overviewResource">
        <div class="container">
            @if (!$product->only_resources)
                <div id="buttonSlider">
                    <div class="whitebox">
                        <button class="overviewButton active">Overview</button>
                        <button class="resourceButton">Resources</button>
                    </div>
                </div>
                <div id="overview">
                    <div class="square--two-up">
                        <h2>Overview</h2>
                        <ul class="blueOvalList">
                            {{ $product->overview }}
                        </ul>
                        @if ($product->details)
                            <h2>Details</h2>
                            <ul class="blueOvalList">
                                {{ $product->details}}
                            </ul>
                        @endif
                    </div>
                    <div class="square--two-up">
                        <div id="imageDimensions">
                            <div class="dimensionValues">
                               <!-- <a href="#dimensions_modal" class="fancybox">See Dimensions</a> -->
                               <!-- <div id="dimensions_modal" style="display: none;"> -->
                                {{-- <img src="{{ asset('resources/dimensions/' . $product->name . ' Dimensions.jpg') }}" alt=""> --}}
                                <!-- </div> -->
                            </div>
                            {{-- <img src="{{ asset('images/pointer.png') }}" alt="" class="pointer"> --}}
                            @if ($product->secondary)
                                <img src="{{ asset('images/products/secondary/' . $product->secondary) }}" alt="" class="dimensionImg">
                            @endif
                        </div>
                        <div id="specLogos">
                            @foreach($product->certifications as $cert)
                              @if($cert->thumbnail)
                                <img src="{{ asset('images/certifications').'/'.$cert->thumbnail }}" alt="{{ $cert->name }}">
                              @else
                                <span class="certification-text">{{ $cert->name }}</span>
                              @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
            <div id="resources">
                <div class="infoSection square--quarter-threequarter">
                    <h2>Resources</h2>
                    <!-- <a href="{{route('zip', $product->id)}}" id="zipFetcher">Download all resources</a> -->
                </div>

                <div class="documentWell square--quarter-threequarter">

                        <?php if(in_array("Installation Instructions", $doctypesIncluded)) {?>
                            <div class="pdf">

                                <ul class="blueOvalList">
                                    <li><span class="slideTrigger inactiveAccordion">Instructions</span>
                                        <ul class="slideTarget">
                            @foreach($product->literature as $literature)
                                    @if($literature->doctype->name ==="Installation Instructions")
                                    <li><a target="_blank" href="{{ asset('resources' . '/'. $literature->doctype->folder . '/' . $literature->path) }}">{{ $literature->name }}</a></li>
                                   @endif
                            @endforeach
                                </ul>
                            </li>
                                </ul>
                            </div>
                        <?php } ?>
                        <?php if(in_array("Brochures", $doctypesIncluded)) {?>
                             <div class="brochures">

                                <ul class="blueOvalList">
                                    <li><span class="slideTrigger inactiveAccordion">Brochures</span>
                                        <ul class="slideTarget">
                                    @foreach($product->literature as $literature)
                                        @if($literature->doctype->name ==="Brochures")
                                        <li><a target="_blank" href="{{ asset('resources' . '/'. $literature->doctype->folder . '/' . $literature->path) }}">{{ $literature->name }}</a></li>
                                       @endif
                                    @endforeach
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        <?php } ?>
                    <?php if(in_array("Spec Sheet", $doctypesIncluded)) {?>
                             <div class="images">

                                <ul class="blueOvalList">
                                    <li><span class="slideTrigger inactiveAccordion">Spec Sheets</span>
                                        <ul class="slideTarget">
                                    @foreach($product->literature as $literature)
                                        @if($literature->doctype->name ==="Spec Sheet")
                                        <li><a target="_blank" href="{{ asset('resources' . '/'. $literature->doctype->folder . '/' . $literature->path) }}">{{ $literature->name }}</a></li>
                                       @endif
                                    @endforeach
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                    <?php } ?>
                        <?php if(in_array("Cutsheet", $doctypesIncluded)) {?>
                            <div class="cutsheets">
                                <ul class="blueOvalList">
                                    <li><span class="slideTrigger inactiveAccordion">Cutsheets</span>
                                        <ul class="slideTarget">
                                    @foreach($product->literature as $literature)
                                        @if($literature->doctype->name ==="Cutsheet")
                                        <li><a target="_blank" href="{{ asset('resources' . '/'. $literature->doctype->folder . '/' . $literature->path) }}">{{ $literature->name }}</a></li>
                                       @endif
                                @endforeach
                                    </ul>
                                </li>
                                </ul>
                            </div>
                        <?php } ?>
                        <?php if(in_array("Manual", $doctypesIncluded)) {?>
                            <div class="manuals">
                                <ul class="blueOvalList">
                                    <li><span class="slideTrigger inactiveAccordion">Manuals</span>
                                        <ul class="slideTarget">
                                    @foreach($product->literature as $literature)
                                        @if($literature->doctype->name ==="Manual")
                                        <li><a target="_blank" href="{{ asset('resources' . '/'. $literature->doctype->folder . '/' . $literature->path) }}">{{ $literature->name }}</a></li>
                                       @endif
                                @endforeach
                                    </ul>
                                </li>
                                </ul>
                            </div>
                        <?php } ?>
                        <?php if(in_array("Press Release", $doctypesIncluded)) {?>
                            <div class="press_releases">
                                <ul class="blueOvalList">
                                    <li><span class="slideTrigger inactiveAccordion">Press Releases</span>
                                        <ul class="slideTarget">
                                    @foreach($product->literature as $literature)
                                        @if($literature->doctype->name ==="Press Release")
                                        <li><a target="_blank" href="{{ asset('resources' . '/'. $literature->doctype->folder . '/' . $literature->path) }}">{{ $literature->name }}</a></li>
                                       @endif
                                @endforeach
                                    </ul>
                                </li>
                                </ul>
                            </div>
                        <?php } ?>
                        <?php if(in_array("Marketing Material", $doctypesIncluded)) {?>
                                 <div class="images">

                                    <ul class="blueOvalList">
                                        <li><span class="slideTrigger inactiveAccordion">Marketing Materials</span>
                                            <ul class="slideTarget">
                                        @foreach($product->literature as $literature)
                                            @if($literature->doctype->name ==="Marketing Material")
                                            <li><a target="_blank" href="{{ asset('resources' . '/'. $literature->doctype->folder . '/' . $literature->path) }}">{{ $literature->name }}</a></li>
                                           @endif
                                        @endforeach
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                        <?php } ?>
                        <?php if(in_array("Sales Tool", $doctypesIncluded)) {?>
                                 <div class="images">

                                    <ul class="blueOvalList">
                                        <li><span class="slideTrigger inactiveAccordion">Sales Tools</span>
                                            <ul class="slideTarget">
                                        @foreach($product->literature as $literature)
                                            @if($literature->doctype->name ==="Sales Tool")
                                            <li><a target="_blank" href="{{ asset('resources' . '/'. $literature->doctype->folder . '/' . $literature->path) }}">{{ $literature->name }}</a></li>
                                           @endif
                                        @endforeach
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                        <?php } ?>
                        <?php if(in_array("Product Training", $doctypesIncluded)) {?>
                                 <div class="images">

                                    <ul class="blueOvalList">
                                        <li><span class="slideTrigger inactiveAccordion">Product Training</span>
                                            <ul class="slideTarget">
                                        @foreach($product->literature as $literature)
                                            @if($literature->doctype->name ==="Product Training")
                                            <li><a target="_blank" href="{{ asset('resources' . '/'. $literature->doctype->folder . '/' . $literature->path) }}">{{ $literature->name }}</a></li>
                                           @endif
                                        @endforeach
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                        <?php } ?>
                        <?php if(in_array("Agent Onboarding Packets", $doctypesIncluded)) {?>
                                 <div class="images">

                                    <ul class="blueOvalList">
                                        <li><span class="slideTrigger inactiveAccordion">Agent Onboarding Packets</span>
                                            <ul class="slideTarget">
                                        @foreach($product->literature as $literature)
                                            @if($literature->doctype->name ==="Agent Onboarding Packets")
                                            <li><a target="_blank" href="{{ asset('resources' . '/'. $literature->doctype->folder . '/' . $literature->path) }}">{{ $literature->name }}</a></li>
                                           @endif
                                        @endforeach
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                        <?php } ?>
                        <?php if(in_array("Miscellaneous", $doctypesIncluded)) {?>
                                 <div class="images">

                                    <ul class="blueOvalList">
                                        <li><span class="slideTrigger inactiveAccordion">Miscellaneous</span>
                                            <ul class="slideTarget">
                                        @foreach($product->literature as $literature)
                                            @if($literature->doctype->name ==="Miscellaneous")
                                            <li><a target="_blank" href="{{ asset('resources' . '/'. $literature->doctype->folder . '/' . $literature->path) }}">{{ $literature->name }}</a></li>
                                           @endif
                                        @endforeach
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                        <?php } ?>
                        <?php if(in_array("Specifications", $doctypesIncluded)) {?>
                                 <div class="images">

                                    <ul class="blueOvalList">
                                        <li><span class="slideTrigger inactiveAccordion">Specifications</span>
                                            <ul class="slideTarget">
                                        @foreach($product->literature as $literature)
                                            @if($literature->doctype->name ==="Specifications")
                                            <li><a target="_blank" href="{{ asset('resources' . '/'. $literature->doctype->folder . '/' . $literature->path) }}">{{ $literature->name }}</a></li>
                                           @endif
                                        @endforeach
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                        <?php } ?>

                </div>

            </div>
        </div>
    </div>
    @include('partials.find')
    <!-- <div class="likeProductsLabel"> -->
    <!--     <div class="container"> -->
    <!--      <h2>Similar Products</h2> -->
    <!--     </div> -->
    <!-- </div> -->
    <!-- @include('products.carousel') -->

@stop

@section('extra_scripts')
    <script src="http://fgnass.github.io/spin.js/spin.min.js"></script>
@stop
