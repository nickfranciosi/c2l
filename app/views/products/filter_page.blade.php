@extends('layouts.default')


@section('content')
    
    <div class="grayContainer">
        {{Breadcrumbs::render('filterPage', $category)}}
        <div class="filter_top">
            <div class="container">
                <div id="categoryHeadline">
                    <h2>{{ $category->name }}</h2>
                </div>
            </div>
        </div>
       @include('partials.product_well')
    </div>
@stop
