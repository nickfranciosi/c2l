<div id="carouselContainer">
    <div id="productCarousel" class="owl-carousel">
      @foreach($carouselItems as $carouselItem)
      <div class="productItem"> 
        <img src="{{ asset('images/products/thumbnails/' . $carouselItem->thumbnail )}}" alt="">
        <h4>{{ $carouselItem->name }}</h4>
        <p>{{ Str::limit($carouselItem->description, 150) }}</p>
        <div class="buttonWell">
            <a href="{{ route('single_product',[$carouselItem->url]) }}" class="viewOverview">Read More</a>
            <a href="{{ route('single_product',[$carouselItem->url,'#resources']) }}" class="viewResources">View Resources</a>
        </div>
      </div>
    @endforeach
    </div>
</div>
