@extends('layouts.default')



@section('content')
    <div class="container" id="salesPage">

    
       <div class="salesTout square--three-up">
           <div class="imageWrapper"><img src="{{ asset('images/map.png') }}" alt=""></div>
           <h3>rep locator</h3>
           <p>Click here to locate your C2L Lighting Agent.</p>
           <a href="{{route('rep_locator')}}">Read More</a>
       </div>
       <div class="salesTout square--three-up">
           <div class="imageWrapper"><img src="{{ asset('images/support.png') }}" alt=""></div>
           <h3>tech support</h3>
           <p>As an industry leader in technical support, we are here to answer your questions. Click here to contact a Technical Support Specialist today.</p>
           <a href="{{route('tech_support')}}">Read More</a>
       </div>
       <div class="salesTout square--three-up">
           <div class="imageWrapper"><img src="{{ asset('images/warranty.png') }}" alt=""></div>
           <h3>our warranties</h3>
           <p>Click here to learn more about our products covered under our limited warranties as well as our terms and conditions.</p>
           <a href="{{route('warranty')}}">Read More</a>
       </div>
    </div>
@stop
