@extends('layouts.default')



@section('content')
    <div class="container" id="salesPage">


       <div class="salesTout square--three-up">
           <div class="imageWrapper"><img src="{{ asset('images/map.png') }}" alt=""></div>
           <h3>Rep Locator</h3>
           <p>Click here to locate your Douglas Lighting Controls Agent.</p>
           <a href="{{route('rep_locator')}}">Read More</a>
       </div>
       <div class="salesTout square--three-up">
           <div class="imageWrapper"><img src="{{ asset('images/support.png') }}" alt=""></div>
           <h3>One-Line Drawings</h3>
           <p>Use our One-Line Drawings tool to generate diagrams for your projects.</p>
           <a href="{{route('oneline')}}">Read More</a>
       </div>
       <div class="salesTout square--three-up">
           <div class="imageWrapper"><img src="{{ asset('images/warranty.png') }}" alt=""></div>
           <h3>Specifications</h3>
           <p>Access our product specifications documents.</p>
           <a href="{{route('specifications')}}">Read More</a>
       </div>
    </div>
@stop
