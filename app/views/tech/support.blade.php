@extends('layouts.default')


@section('content')
    <div class="container" id="techSupport">
        <div class="headerText">
            <h2>Tech Support</h2>
            <p>Looking for more information or have specific product questions?  Please fill out the form below and a Technical Engineering Services representative will contact you.</p>
        </div>
        <div class="form reverse">
            {{ Form::open(['route' => 'tech_support','data-parsley-validate']) }}
            <div class="form-group">
                {{ Form::label('name', 'Name')}}
                {{ Form::text('name', null, ['required']) }}
                {{ $errors->first('name', '<span class="error">:message</span>') }}
            </div>

            <div class="form-group">
                {{ Form::label('email', 'Email')}}
                {{ Form::email('email', null, ['required']) }}
                {{ $errors->first('email', '<span class="error">:message</span>') }}
            </div>

             <div class="form-group">
                {{ Form::label('phone', 'Phone')}}
                {{ Form::text('phone', null, ['required']) }}
                {{ $errors->first('phone', '<span class="error">:message</span>') }}
            </div>

            <div class="form-group">   
                {{ Form::label('question', 'Questions/Comments')}}
                {{ Form::textarea('question', null, ['rows' => '8', 'required']) }}
            </div>
                {{ Honeypot::generate('my_name', 'my_time') }}
            <div class="form-group">  
                {{ Form::submit('Send') }}
            </div>
            
            {{ Form::close() }}
        </div>
    </div>
@stop