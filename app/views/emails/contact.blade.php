<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>C2L | Control To Light</title>
</head>
<body>
    <h2>New Contact Form Submission: {{ $from }}</h2>
    @if(isset($name))
    <p>Name: {{ $name }}</p>
    @endif
    @if(isset($phone))
    <p>Phone: {{ $phone }}</p>
    @endif
    <p>{{$comment}}</p>
</body>
</html>