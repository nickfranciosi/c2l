<div id="findRepBar">
    {{ Form::open(['action' => 'RepController@index']) }}
    <div class="square--two-up">
      <i class="fa fa-map-marker"></i><h2>Find Your Douglas Rep</h2>
    </div>
    <div class="square--two-up">
      {{ Form::text('repZip',null, ['placeholder' => "Enter Zip Code"]) }}
      {{ Form::submit('GO') }}
    </div>
    {{ Form::close() }}
</div>
