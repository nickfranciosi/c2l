<?php
$sortedHeaderItems = array();

foreach ($headerItems as $key => $value) {
  $sortedHeaderItems[] = $value;
}

usort($sortedHeaderItems, function ($a, $b) {
  return $a['sortOrder'] > $b['sortOrder'] ? 1 : -1;
});
?>

<header>
  <div class="container">
    <div class="rowOne">
      <div class="mainLogo">
        <a href="{{ route('home') }}"><img src="{{ asset('images/logo.svg')}}"></a>
      </div>
      <ul>
        <li><span>Toll free: </span><a href="tel:18778732797">877-873-2797</a></li>
        <li><span>Direct: </span><a href="tel:16048732797">604-873-2797</a></li>
        <li>
          <div class="search">
            {{ Form::open(['route' => 'search_results']) }}
            {{ Form::text('query', null, ['placeholder' => 'Search Site']) }}
            {{ Form::close() }}
          </div>
          <div class="clearfix"></div>
        </li>
      </ul>
    </div>
  </div>

  <div class="rowTwo">
    <nav>
      <div class="container">
        <ul>
          <li><a {{ empty(Request::segment(1)) ? "class='active'" : '';}}href="{{ route('home') }}">Home</a></li>
          <li>
            <a {{ (Request::segment(1) === 'products' || Request::segment(1) === 'solutions') ? "class='active'" : '';}}href="{{ route('solutions') }}">Solutions</a>
            <div class="sub-menu">
              <h3>Douglas Solutions</h3>
              <ul class="blueOvalList">
                <li></li>
                @foreach($sortedHeaderItems as $item)
                @if($item->has_subcategory)
                <li><a href="{{ route('filterPage', $item->url) }}" alt="{{ $item->name }}">{{ $item->name }}</a></li>
                @elseif($item->name === 'Space Player')
                <li><a href="{{ $item->url }}" target="_blank" alt="{{ $item->name }}">{{ $item->name }}</a></li>
                @elseif($item->name === 'Full Two Way' || $item->name === 'Resource Library')
                {{-- Don't show anything --}}
                @else
                <li><a href="{{ route('single_product', $item->url) }}" alt="{{ $item->name }}">{{ $item->name }}</a></li>
                @endif
                @endforeach
              </ul>
            </div>
          </li>
          <li><a {{ Request::segment(1) === 'applications' ? "class='active'" : '';}}href="{{route('applications')}}">Applications</a>
            <div class="sub-menu">
              <ul class="blueOvalList">
                <li></li>
                <li><span><a href="{{route('spotlights')}}">Project Spotlights &mdash; Case Studies</a></span></li>
              </ul>
            </div>
          </li>
          <li><a {{ Request::segment(1) === 'about' ? "class='active'" : '';}}href="{{ route('about') }}">About</a>
            <div class="sub-menu">
              <h3>About Douglas Lighting</h3>
              <ul class="blueOvalList">
                <li></li>
                <li><span><a href="{{route('pressreleases')}}">Press Releases</a></span></li>
                <li><span><a href="{{route('pressreleases')}}">Videos</a></span></li>
                <li><span><a target="_blank" href="/pdf/CompanyProfile.pdf">Company Profile</a></span></li>
              </ul>
            </div>
            </li>
          <li><a {{ Request::segment(1) === 'support' ? "class='active'" : '';}}href="{{ route('support') }}">Support</a>
            <div class="sub-menu">
              <h3>Douglas Support</h3>
              <p>Resources to help get your project started:</p>
              <ul class="blueOvalList">
                <li><span><a href="{{route('rep_locator')}}">Rep Locator</a></span></li>
                <li><span><a href="{{route('oneline')}}">One Line Drawings</a></span></li>
                <li><span><a href="{{route('specifications')}}">Specifications</a></span></li>
                <li><span><a href="{{route('contact')}}">Contact Us</a></span></li>
              </ul>
            </div>
          </li>
          <li class="nav-icon">
            <a href="https://www.linkedin.com/company/douglas-lighting-controls?trk=biz-companies-cyf" target="_blank" class="fa-disabled fa-linkedin-square-disabled">
              <span class="_">
                <script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
                <script type="IN/FollowCompany" data-id="2397424" data-counter="none"></script>
              </span>
            </a>
          </li>
        </ul>
      </div>
    </nav>

    <div class="mobile-menu">
      <p class="menu-trigger"><i class="fa fa-bars"></i></p>
      <div class="mobile-menu-content">
        <ul>
          <li><a href="/">Home</a></li>
          <!-- <li><a href="/products">Products</a></li> -->
          <li><a href="#" class="sub-menu" data-mobile-menu="solutions">Solutions</a>
            <ul class="sub-menu" data-mobile-menu="solutions">
              @foreach($headerItems as $item)
              @if($item->has_subcategory)
              <li><a href="{{ route('filterPage', $item->url) }}" alt="{{ $item->name }}">{{ $item->name }}</a></li>
              @elseif($item->name === 'Full Two Way' || $item->name === 'Resource Library')
              {{-- Don't show anything --}}
              @else
              <li><a href="{{ route('single_product', $item->url) }}" alt="{{ $item->name }}">{{ $item->name }}</a></li>
              @endif
              @endforeach
            </ul>
          </li>
          <li><a href="{{ route('applications') }}">Applications</a>
            <ul class="sub-menu" data-mobile-menu="applications">
              <li><span><a href="{{route('spotlights')}}">Project Spotlights &mdash; Case Studies</a></span></li>
            </ul>
          </li>
          <li><a href="{{ route('about') }}">About</a></li>
          <li><a href="#" class="sub-menu" data-mobile-menu="applications">Support</a>
            <ul class="sub-menu" data-mobile-menu="applications">
              <li><span><a href="{{route('rep_locator')}}">Rep Locator</a></span></li>
              <li><span><a href="{{route('oneline')}}">One Line Drawings</a></span></li>
              <li><span><a href="{{route('specifications')}}">Specifications</a></span></li>
              <li><span><a href="{{route('contact')}}">Contact Us</a></span></li>
            </ul>
          </li>
          <li>
            <a href="https://www.linkedin.com/company/douglas-lighting-controls?trk=biz-companies-cyf" target="_blank">
              <span class="_">
                <script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
                <script type="IN/FollowCompany" data-id="2397424" data-counter="none"></script>
              </span>
            </a>
          </li>
        </ul>
      </div>
    </div>

    <div class="mobile-blue-bar"></div>
    <div class="clearfix"></div>
  </div>
</header>
