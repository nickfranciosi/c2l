<?php
$sortedProductCategories = array();

foreach ($productCategories as $key => $value) {
  $sortedProductCategories[] = $value;
}

usort($sortedProductCategories, function ($a, $b) {
  return $a['sortOrder'] > $b['sortOrder'] ? 1 : -1;
});
?>


<div id="productWell">
    <div class="productRow">
        <div class="container">

            @foreach($sortedProductCategories as $category)
                @if($category->name === 'Full Two Way' || $category->name === 'Resource Library')
                    <?php continue ?>
                @endif

                <div class="productItem">
                    @if($category->hero)
                        <img src="{{ asset('images/products/heros/' . $category->hero) }}" alt="">
                    @else
                        <h2 style="text-align:center; line-height:350px">image coming soon</h2>
                    @endif

                    <h4>{{ $category->name }}</h4>

                    <div class="buttonWell">
                        @if($category->has_subcategory)
                            <a href="{{ route('filterPage', [$category->url]) }}" class="viewProducts">See products</a>
                        @elseif($category->name === 'Space Player')
                            <a href="{{ $category->url }}" target="_blank" alt="{{ $category->name }}" class="viewProducts">See products</a>
                        @else
                            <a href="{{ route('single_product', [$category->url]) }}" class="viewProducts">See products</a>
                        @endif
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</div>
