<div id="productWell">
       <div class="productRow">
            <div class="container">
            @foreach($products as $product)

               <div class="productItem">
                       <img src="{{ asset('images/products/thumbnails/' . $product->thumbnail)}}" alt="">
                       <h4>{{$product->name}}</h4>
                       <p>{{ Str::limit($product->blurb, 150) }}</p>
                       <div class="buttonWell">
                           <a href="{{ route('single_product',[$product->url]) }}" class="viewOverview">Read More</a>
                           <a href="{{ route('single_product',[$product->url,'#resources']) }}" class="viewResources">View Resources</a>
                       </div>
                       
                       <div class="">
                         Hello
                       </div>


                     </div>
              @endforeach
       </div>
</div>
