<footer>
    <div class="container">
        <div class="logoLegal square--three-two-up">
            <img src="{{ asset('images/logo-footer.svg')}}" alt="Douglas">
            <p>
                Douglas Lighting Controls, a member of the Panasonic Group, engineers energy efficient, easy to install and use, digital lighting control solutions for commercial buildings, campuses, and sports complexes throughout North America. Douglas systems include relays panels, controllers, occupancy/vacancy sensors, daylight sensors and wall switch stations. Douglas has a dedicated design team that produces system drawings and a technical support group for product questions and onsite system commissioning. With over 50 years in operation, Douglas is recognized for their deep understanding of lighting control systems and ability to provide the right solution for each facility.
            </p>
            <p>
                &copy;<span id="copyYear">20XX</span> Douglas Lighting Controls.  All rights reserved.
            </p>
        </div>

         <div class="links square--three-two-up">
            <h3>Links</h3>
            <ul>
              <li><a href="{{ route('home') }}">Home</a></li>
              <li><a href="{{ route('solutions') }}">Solutions</a></li>
              <li><a href="{{ route('applications') }}">Applications</a></li>
              <li><a href="{{ route('about') }}">About Douglas</a></li>
              <li><a href="{{ route('support') }}">Support</a></li>
              <li><a href="{{ route('sitemap') }}">Sitemap</a></li>
              <li><a href="/resources/misc/Terms-Conditions2013-05-24.pdf" target="_blank">Terms &amp; Conditions</a></li>
              <li><a href="{{ route('contact') }}">Contact Us</a></li>
            </ul>
        </div>

         <div class="contactForm square--three-two-up">
            <h3>Contact Us</h3>
            <p>Feel free to contact us with any questions you might have about Douglas.</p>
            {{ Form::open(['action' => 'ContactController@footerSend','data-parsley-validate']) }}

            {{ Form::label('footerEmail', 'Email')}}
            {{ Form::email('footerEmail',null, ['required']) }}
            {{ $errors->first('footerEmail', '<span class="error">The email field is required</span>') }}

            {{ Form::label('question', 'Questions/Comments')}}
            {{ Form::textarea('question', null, ['rows' => '4', 'required']) }}
            {{ $errors->first('question', '<span class="error">:message</span>') }}

            {{ Honeypot::generate('my_name_f', 'my_time_f') }}
            {{ Form::submit('Send') }}

            {{ Form::close() }}
        </div>
    </div>
    <div class="container no-margin">
        <div class="footer-logos">
          <a target="_blank" href="http://www.ies.org/"><img height="70" src="{{ asset('images/member-logos/ies-sustaining-member-logo-color-resized.jpg') }}" alt=""></a>
          <a target="_blank" href="http://www.lightingcontrolsassociation.org/"><img height="70" src="{{ asset('images/member-logos/lca-logo-member-rgb-small.gif') }}" alt=""></a>
          <a target="_blank" href="http://www.cagbc.org/"><img height="70" src="{{ asset('images/member-logos/cagbc-member-rgb-web.png') }}" alt=""></a>
          <img height="70" src="{{ asset('images/member-logos/t24-resized.png') }}" alt="">
          <div class="ashrae-text"><p>ASHRAE 90.1 Compliant</p></div>
        </div>

        <div id="panasonic">
            <div class="img" aria-labelledby="A member of the Panasonic Group"></div>
        </div>
    </div>
</footer>
