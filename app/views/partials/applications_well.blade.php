<div id="applicationWell">
    <div class="applicationRow">
        <div class="container">
            @foreach($types as $type)
                <div class="applicationItem"> 
                    <img src="{{ asset('images/applications/hero/' . $type->hero)}}" alt="">
                    <h4>{{$type->name}}</h4>
                    <ul class="ovalList">
                        @foreach($type->applications as $app)
                            <li><a href="{{ route('single_application', [$app->url]) }}">{{ $app->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
            @endforeach
        </div>
    </div>
</div>
