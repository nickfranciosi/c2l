@extends('layouts.default')


@section('content')
    
    <div class="container scootDown">
        <h2>User Admin</h2>
        <p>Users with a checkbox are active. To inactivate uncheck box and click "Update" next to that user</p>

        <table id="adminForm">
        @foreach($users as $user)
            {{Form::open()}}
                <tr>
                    <td class="check">{{Form::checkbox('active', $user->id, $user->isactive)}}</td>
                    {{Form::hidden('id', $user->id)}}
                    <td class="name">{{Form::label($user->email)}}</td>
                    <td>{{Form::submit('Update')}}</td>
                    </tr>
            {{Form::close()}}
        @endforeach
        </table>
    </div>
@stop

