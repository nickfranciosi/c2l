@extends('layouts.default')

@section('content')

    <div class="container form" id="loginForm">
        <div id="agentBlurb">
            <h2>Agent Resource Center</h2>
        </div>
        @if(Session::has('auth_fail_message'))
            <p>{{Session::get('auth_fail_message')}}</p>
        @endif

        {{ Form::open(['route' => 'login', 'data-parsley-validate']) }}
        <div class="form-group"> 
        {{ Form::label('email', 'Email')}}
        {{ Form::email('email', null, ['required'])}}
        {{ $errors->first('email', '<span class="error">:message</span>') }}
        </div>
        <div class="form-group"> 
        {{ Form::label('password', 'Password')}}
        {{ Form::password('password', null, ['required'])}}
        {{ $errors->first('password', '<span class="error">:message</span>') }}
        </div>
        <div class="form-group"> 
        {{ Form::submit('Log in')}}
          </div>
        {{ Form::close()}}

    </div>
@stop