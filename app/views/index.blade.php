@extends('layouts.default')


@section('content')
    @include('partials.homeBanner')
    <div class="blue-bar"></div>
    <div class="main container">
        <h1>Applications</h1>
    </div>
    @include('applications.carousel')
    @include('partials.find')
        <div class="container">
            <div id="contactCallout">
              <div class="square--sixty-forty">
                <div id="register">
                    <h2>Register</h2>
                    <p>Register to receive information about new products and important communications.</p>
                    <div id="emma-container">
                      <script type="text/javascript" src="https://app.e2ma.net/app2/audience/tts_signup/1795395/fa04f1eafd8a7283552bc4f158d62489/1360917/?v=a"></script><div id="load_check" class="signup_form_message" >This form needs Javascript to display, which your browser doesn't support. <a href="https://app.e2ma.net/app2/audience/signup/1795395/1360917/?v=a"> Sign up here</a> instead </div><script type="text/javascript">signupFormObj.drawForm();</script>
                    </div>
              </div>
              <!-- <div class="square&#45;&#45;sixty&#45;forty"> -->
              <!--   <div id="contact"> -->
              <!--       <i class="fa fa&#45;phone"></i> -->
              <!--       <h2>Contact</h2> -->
              <!--       <p>Contact us for help on your projects or questions on any of our products.</p> -->
              <!--       <a href="{{  route('contact')}}">Contact Us</a> -->
              <!--   </div> -->
              <!-- </div> -->
            </div>
        </div>
    </div>

@stop
