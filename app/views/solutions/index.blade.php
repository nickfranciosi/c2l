@extends('layouts.default')

@section('content')

    <div class="grayContainer ">
      {{Breadcrumbs::render('solutions')}}
      @include('partials.solutions')
    </div>
@stop
