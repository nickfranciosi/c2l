@extends('layouts.default')

@section('content')
    <div class="container clear-header-xlarge" id="repLocator">
        <h2>Rep Locator</h2>
        <div id="selections">
            <div id="stateDropdown">
                <select name="state_picked" id="state_picked" data-placeholder="Choose a State/Province" >
                    <option value=""></option>
                    <optgroup label="United States">
                        <option value="US-AL">Alabama</option>
                        <option value="US-AK">Alaska</option>
                        <option value="US-AZ">Arizona</option>
                        <option value="US-AR">Arkansas</option>
                        <option value="US-CA">California</option>
                        <option value="US-CO">Colorado</option>
                        <option value="US-CT">Connecticut</option>
                        <option value="US-DE">Delaware</option>
                        <option value="US-FL">Florida</option>
                        <option value="US-GA">Georgia</option>
                        <option value="US-HI">Hawaii</option>
                        <option value="US-ID">Idaho</option>
                        <option value="US-IL">Illinois</option>
                        <option value="US-IN">Indiana</option>
                        <option value="US-IA">Iowa</option>
                        <option value="US-KS">Kansas</option>
                        <option value="US-KY">Kentucky</option>
                        <option value="US-LA">Louisiana</option>
                        <option value="US-ME">Maine</option>
                        <option value="US-MD">Maryland</option>
                        <option value="US-MA">Massachusetts</option>
                        <option value="US-MI">Michigan</option>
                        <option value="US-MN">Minnesota</option>
                        <option value="US-MS">Mississippi</option>
                        <option value="US-MO">Missouri</option>
                        <option value="US-MT">Montana</option>
                        <option value="US-NE">Nebraska</option>
                        <option value="US-NV">Nevada</option>
                        <option value="US-NH">New Hampshire</option>
                        <option value="US-NJ">New Jersey</option>
                        <option value="US-NM">New Mexico</option>
                        <option value="US-NY">New York</option>
                        <option value="US-NC">North Carolina</option>
                        <option value="US-ND">North Dakota</option>
                        <option value="US-OH">Ohio</option>
                        <option value="US-OK">Oklahoma</option>
                        <option value="US-OR">Oregon</option>
                        <option value="US-PA">Pennsylvania</option>
                        <option value="US-RI">Rhode Island</option>
                        <option value="US-SC">South Carolina</option>
                        <option value="US-SD">South Dakota</option>
                        <option value="US-TN">Tennessee</option>
                        <option value="US-TX">Texas</option>
                        <option value="US-UT">Utah</option>
                        <option value="US-VT">Vermont</option>
                        <option value="US-VA">Virginia</option>
                        <option value="US-WA">Washington</option>
                        <option value="US-DC">Washington D.C.</option>
                        <option value="US-WV">West Virginia</option>
                        <option value="US-WI">Wisconsin</option>
                        <option value="US-WY">Wyoming</option>
                    </optgroup>
                    <optgroup label="Canada">
                        <option value="CA-AB">Alberta</option>
                        <option value="CA-BC">British Columbia</option>
                        <option value="CA-MB">Manitoba</option>
                        <option value="CA-NB">New Brunswick</option>
                        <option value="CA-NL">Newfoundland and Labrador</option>
                        <option value="CA-NS">Nova Scotia</option>
                        <option value="CA-ON">Ontario</option>
                        <option value="CA-PE">Prince Edward Island</option>
                        <option value="CA-QC">Quebec</option>
                        <option value="CA-SK">Saskatchewan</option>
                        <option value="CA-NT">Northwest Territories</option>
                        <option value="CA-NU">Nunavut</option>
                        <option value="CA-YT">Yukon</option>
                    </optgroup>
                </select>
            </div>
            <!-- <div id="textSearch">
                <form action="" id="manualEntry">
                    <input type="text" id="addressEntry" name="addressEntry" placeholder="Enter Address, State, or Zip" value="{{{$address}}}">
                </form>
                <button>Search</button>
            </div> -->
            <div id="instructions">
                <p>Select a state or province from the dropdown, or pick a state from the map to find your nearest Douglas Lighting Controls representitive.</p>
            </div>
            <div id="map-container">
                <div id="loc-list">
                    <ul id="list"></ul>
                </div>
                <div id="map"></div>
            </div>
        </div>
        <div id="mapContainer">
            <div id="caMap" style="width: 500px; height: 500px;"></div>
            <div id="usMap" style="width: 500px; height: 500px;"></div>
        </div>

        <div id="form-container">
            <form id="user-location" method="post" action="#">
                <div id="form-input">
                  <input type="text" id="address" name="address" value="{{{$address}}}"/>
                 </div>

                <button id="submit" type="submit">Submit</button>
            </form>
        </div>


    </div>


@stop


@section('extra_scripts')
    @if($address !== null)
        <script>
            $('#instructions').hide();
        </script>
    @endif
   <script src="{{ asset('js/chosen.jquery.min.js') }}"></script>
   <script src="{{ asset('js/handlebars-1.0.0.min.js') }}"></script>
   <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
   <script src="{{ asset('js/jquery.storelocator.min.js') }}"></script>
   <script src="{{ asset('js/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
   <script src="{{ asset('js/jvectormap/jquery-jvectormap-ca-lcc.js') }}"></script>
   <script src="{{ asset('js/jvectormap/jquery-jvectormap-us-lcc.js') }}"></script>
   <script src="{{ asset('js/jvectormap/jvectormap-main.js') }}"></script>


   <script>
    $(function(){
        $('#state_picked').chosen();

        $('#map-container').storeLocator({
          'dataType': 'json',
          'distanceAlert': -1,
          'dataLocation': '{{ asset('/getRepjson') }}',
          'KMLinfowindowTemplatePath': '{{ asset('/locationtemplates/kml-infowindow-description.html') }}',
          'KMLlistTemplatePath' : '{{ asset('/locationtemplates/kml-location-list-description.html') }}',
          'infowindowTemplatePath':'{{ asset('/locationtemplates/infowindow-description.html') }}',
          'listTemplatePath' : '{{ asset('/locationtemplates/location-list-description.html') }}',
          callbackBeforeSend: function () {

            this.url += '?state=' + $('#address').val();
          }
        });

        setTimeout(function(){$('#user-location').submit()}, 100);

        // Polyfill for when input does not support placeholder attribute
        if (! Modernizr.input.placeholder) {
          $('#addressEntry').val($('#addressEntry').attr('placeholder'));

          $('#addressEntry').blur(function () {
            if ($(this).val() === '') {
              $(this).val($(this).attr('placeholder'));
            }
          });

          $('#addressEntry').focus(function () {
            if ($(this).val() === $(this).attr('placeholder')) {
              $(this).val('');
            }
          });
        }
    });

    </script>

@stop
