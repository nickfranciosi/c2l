$(function(){
      $('#footerForm').on('submit',function(event){
          event.preventDefault();
          var postData = $(this).serialize();
          var formURL = $(this).attr("action");
          $.ajax({
            url : formURL,
            type: "POST",
            data : postData,
            datatype: 'json',
            success:function(data, textStatus, jqXHR)
            {
                console.log(data.errors);
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                console.log("error");
            },
          });
      });
});
