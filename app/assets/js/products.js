$(function(){
    var $buttonGroup = $('#buttonSlider button');
    var $overviewButton = $('.overviewButton');
    var $resourceButton = $('.resourceButton');
    var $resourcesSection = $('#resources');
    var $overviewSection = $('#overview');
    var $resourceAccordion = $('.slideTarget');


    function showOverview(){
        $buttonGroup.removeClass('active');
        $overviewButton.addClass('active');
        $resourcesSection.hide();
        $overviewSection.show();
    }

    function showResources(){
        $buttonGroup.removeClass('active');
        $resourceButton.addClass('active');
        $overviewSection.hide();
        $resourcesSection.show();
        $resourceAccordion.hide();

    }

    function disableHashScroll(){
        setTimeout(function() {
            if (location.hash) {
              window.scrollTo(0, 0);
            }
          }, 1);
    }

    function checkHash(){
        if(window.location.hash === '#resources' || document.getElementById('overview') === null) {
          showResources();
          disableHashScroll();
        } else {
         showOverview();
         disableHashScroll();
        }
    }

    $buttonGroup.click(function(){
        $buttonGroup.removeClass('active');
        $(this).addClass('active');

        if($overviewButton.hasClass('active')){
            showOverview();
        }

        if($resourceButton.hasClass('active')){
            showResources();
        }
    });

    checkHash();
    
});
