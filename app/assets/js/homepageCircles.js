$(function () {
  var $circles = $('#compCircles').find('>a');
  $('#tabbedContent div').hide();
  $circles.first().addClass('active');
  $('#tabbedContent '+ $circles.first().attr('href')).show();
  // (function _loop(idx) {
  //   $circles.removeClass('active').eq(idx).click();
  //   var circleLoop = setTimeout(function () {
  //     _loop((idx + 1) % $circles.length);
  //   }, 8000);

  //   $('#compCircles').hover(function(){
  //     clearInterval(circleLoop);
  //     $circles.removeClass('active');
  //   },function(){
  //       _loop(0);
  //   });

  // }(0));


  $circles.on('click',function(e){
    e.preventDefault();
    $circles.removeClass('active');
    var $this = $(this);
    $this.addClass('active');
    var toShow = $this.attr('href');
    $('#tabbedContent div').hide();
    $('#tabbedContent ' + toShow).show();
  });
});