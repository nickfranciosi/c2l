var $ = require('jquery');
// var humane = require('humanejs');
var app = require('./app.js');
var raphael = require('raphael');
var jqMaps = require('usmap');
var carousel = require('./carousel.js');
var copyright = require('./copyrightYear.js');
var prouducts = require('./products.js');
var fancybox = require('./vendor/jquery.fancybox.js');
var fancyboxInit = require('./fancyboxInit.js');
var accordion = require('./accordion.js');
var videojs = require('./vendor/video.js');
var homepageCircles = require('./homepageCircles.js');
var zipdownload = require('./zipdownload.js');
var parsleyjs = require('parsleyjs');
