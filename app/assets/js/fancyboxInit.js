$(function(){
    $('.fancybox').fancybox({
        openEffect  : 'fade',
        closeEffect : 'fade',
        nextEffect  : 'fade',
        prevEffect  : 'fade',
        padding     : 20,
        maxWidth    : 700,
        margin      : [20, 60, 20, 60] // Increase left/right margin
    });

});