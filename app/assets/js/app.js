if (navigator.platform === 'Win32') {
  // Appending global styles to the page are _much_ faster than using JavaScript
  $('<style>\
      *,header a{ font-weight: 400; }\
      h1,h2,h3,h4,h5,h6{ font-weight: 300; }\
    </style>').appendTo('head')
}
$(function(){
    $(window).on('scroll',function(){
        var position = $(window).scrollTop();
        if(position > 10){
            $('header').addClass('white');
        }else{
            $('header').removeClass('white');
        }
    });

    $('.mobile-menu .menu-trigger').on('click', function(){
      $(this).closest('.mobile-menu').toggleClass('menu-open');
    });

    $('.mobile-menu a.sub-menu').on('click', function(){
      var $mobileNavItemClicked = $(this).data('mobile-menu');
      var subMenusArr= $('ul[data-mobile-menu]');
      for(i=0; i < subMenusArr.length; i++){
        $subMenusItem = $(subMenusArr[i]);
        if ($subMenusItem.data('mobile-menu') === $mobileNavItemClicked){
          $subMenusItem.toggleClass('active');
        }
      }
    });
});
