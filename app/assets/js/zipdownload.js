$(function(){

      $('#zipFetcher').on('click', function(e){
        e.preventDefault();
        var opts = {
          lines: 13, // The number of lines to draw
          length: 20, // The length of each line
          width: 10, // The line thickness
          radius: 30, // The radius of the inner circle
          corners: 1, // Corner roundness (0..1)
          rotate: 0, // The rotation offset
          direction: 1, // 1: clockwise, -1: counterclockwise
          color: '#000', // #rgb or #rrggbb or array of colors
          speed: 1, // Rounds per second
          trail: 60, // Afterglow percentage
          shadow: false, // Whether to render a shadow
          hwaccel: false, // Whether to use hardware acceleration
          className: 'spinner', // The CSS class to assign to the spinner
          zIndex: 2e9, // The z-index (defaults to 2000000000)
          top: '50%', // Top position relative to parent
          left: '50%' // Left position relative to parent
        };
        var target = document.getElementById('resources');
        var spinner = new Spinner(opts).spin(target);

        var url = $(this).attr('href');
        $.ajax({
          url: url,
          type: 'GET',
          success:function(data, textStatus, jqXHR) 
          {
              document.location = '/resources/zips/' + data + '.zip';
              spinner.stop();
          },
          error: function(jqXHR, textStatus, errorThrown) 
          {   spinner.stop();
              humane.log("There was an error with your download. Please try again later.", {timeout: 4000, clickToClose: true });  
          }
        });

      });


    });