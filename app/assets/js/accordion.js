$(function(){
  $('.slideTrigger').click(function(){
    $toggleButton= $(this);
    $toggleTarget = $toggleButton.parent().children('.slideTarget'); 
    if ($toggleButton.hasClass('activeAccordion')){
      $toggleButton.removeClass('activeAccordion');
      $toggleButton.addClass('inactiveAccordion');
      $toggleTarget.slideUp();
    } else {
      $toggleButton.removeClass('inactiveAccordion');
      $toggleButton.addClass('activeAccordion');
      $toggleTarget.slideDown();
    }
  });
});
