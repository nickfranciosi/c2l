var owl = require('owl');

$("#productCarousel").owlCarousel({
    items: 4,
    responsive: true,
    navigation: true,
    navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
    pagination: false,
    lazyLoad: true
});

$("#bannerRotator").owlCarousel({
    responsive: true,
    navigation: false,
    paginationSpeed: 400,
    autoPlay: 5000,
    singleItem: true,
    stopOnHover: true,
    lazyLoad: true
});
