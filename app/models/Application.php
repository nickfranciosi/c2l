<?php 

class Application extends \Eloquent {
    protected $fillable = [];

    public $table = 'applications';

    public function applicationType(){
      return $this->belongsTo('ApplicationType');
    }
}
