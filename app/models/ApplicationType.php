<?php

class ApplicationType extends \Eloquent {

    protected $fillable = [];

    public $table = 'application_type';

    public function applications(){
        return $this->hasMany('Application');
    }
}
