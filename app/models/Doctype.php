<?php

class Doctype extends \Eloquent {
	protected $fillable = ['name'];

    public $table = 'doctype';

    public function literature()
    {
        return $this->hasMany('Literature');
    }


}