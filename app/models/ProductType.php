<?php

class ProductType extends \Eloquent {

    protected $fillable = ['name'];

    public $table = 'product_type_new';

    public function products(){
        return $this->hasMany('Product');
    }


}
