<?php

class SimilarProduct extends \Eloquent {
    protected $fillable = [];

    public $table = 'similar_product';

    public function products(){
      return $this->belongsToMany('Product');
    }
}
