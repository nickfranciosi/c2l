<?php

class Literature extends \Eloquent {
    protected $fillable = ['name'];

    public $table = 'literature';
    
    public function products()
    {
        return $this->belongsToMany('Product');
    }

    public function doctype()
    {
        return $this->belongsTo('Doctype');
    }

}