<?php 

class Specification extends \Eloquent {

    protected $fillable = [];

    public $table = 'specifications';

    public function specification_documents(){
        return $this->hasMany('SpecificationDocument');
    }
}
