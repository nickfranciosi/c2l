<?php

class ProductSubCategory extends \Eloquent {

    protected $fillable = ['name'];

    public $table = 'product_sub_category';

    public function products(){
        return $this->hasMany('Product');
    }


}