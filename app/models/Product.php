<?php 

class Product extends \Eloquent {
    protected $fillable = [];

   public $table = 'products_new';

   public function productSubCategory(){
    return $this->belongsTo('ProductSubCategory');
   }

   public function productType(){
    return $this->belongsTo('ProductType');
   }

    public function certifications(){
    return $this->belongsToMany('Certification');
   }

    public function literature(){
      return $this->belongsToMany('Literature');
    }

    public function similarProducts(){
    return $this->hasMany('SimilarProduct');
   }
}
