<?php

use C2l\Repositories\ApplicationsRepository as ApplicationsRepository;

class ApplicationsController extends \BaseController {


	protected $applicationsRepo;

	public function __construct(ApplicationsRepository $applicationsRepo)
	{
		$this->applicationsRepo = $applicationsRepo;
	}

	/**
	 * Display a listing of the resource.
	 * GET /products
	 *
	 * @return Response
	 */
	public function index()
	{
		$types = $this->applicationsRepo->getAll();
		$carouselItems = $this->applicationsRepo->getAll();
		return View::make('applications.index')->with(compact('types'))->with(compact('carouselItems'));
	}

	public function show($url)
	{
		$application = $this->applicationsRepo->getFromUrl($url);
		return View::make('applications.single')->with(compact('application'));
	}

}
