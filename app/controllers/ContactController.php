<?php 

use C2l\Mailers\FollowUpMailer;
use C2l\Mailers\AdminMailer;
use C2l\Mailers\TechSupportMailer;
use C2l\Forms\ContactPageFormValidator;
use C2l\Forms\FooterFormValidator;


class ContactController extends \BaseController {

    protected $followUpMailer;
    protected $adminMailer;
    protected $techSupportMailer;
    protected $fullValidator;
    protected $footerValidator;

    public function __construct(FollowUpMailer $followUpMailer, 
                                AdminMailer $adminMailer, 
                                TechSupportMailer $techSupportMailer, 
                                ContactPageFormValidator $fullValidator, 
                                FooterFormValidator $footerValidator){
        $this->followUpMailer = $followUpMailer;
        $this->adminMailer = $adminMailer;
        $this->techSupportMailer = $techSupportMailer;
        $this->fullValidator = $fullValidator;
        $this->footerValidator = $footerValidator;
    }


    public function show()
    {
        return View::make('contact');
    }

    public function send()
    {
        $this->fullValidator->validate(Input::all());
        $name = Input::get('name');
        $email = Input::get('email');
        $phone = Input::get('phone');
        $comment = Input::get('question');
        $this->adminMailer->sendEmailToAdmin($email, $comment, $phone);
        return Redirect::back()->with('message','Thanks for contacting us! We will get back with you shortly.');
    }

    public function footerSend()
    {
        $this->footerValidator->validate(Input::all());
        $email = Input::get('footerEmail');
        $comment = Input::get('question');
        $this->followUpMailer->followUpTo($email);
        $this->adminMailer->sendEmailToAdmin($email, $comment);
        return Redirect::back()->with('message',$this->followUpMailer->getFlashMessage());
    }


    public function techSend()
    {
        $this->fullValidator->validate(Input::all());
        $name = Input::get('name');
        $email = Input::get('email');
        $phone = Input::get('phone');
        $comment = Input::get('question');
        $this->techSupportMailer->sendEmailToAdmin($email, $comment, $name, $phone);
        return Redirect::back()->with('message','Thanks for contacting us! We will get back with you shortly.');
    }
}