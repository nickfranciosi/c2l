<?php


class RepController extends \BaseController {
  public function index()
  {
    $address = null;
    return View::make('replocator.index')->with(compact('address'));
  }

  public function autoloadLocation()
  {
    $address = Input::get('repZip');
    return View::make('replocator.index')->with(compact('address'));
  }

  public function getRepjson()
  {
    $locations = json_decode(RepLocations::all());

    if (isset($_GET['state'])) {
      $filteredLocations = array_filter($locations, function($k) {
        $state = $_GET['state'];

        if ($k->id == 103 || $k->id == 120) {
          $currentTime = strtotime('now');
          if ($currentTime < strtotime('2017-06-10 CDT')) {
            return false;
          }
        }

        return ($k->state == $state);
      });
    } elseif (isset($_GET['search'])) {
      $filteredLocations = array_filter($locations, function($k) {
        $query = strtolower($_GET['search']);

        if (strtolower($k->view_state) == $query) {
          return true;
        } elseif (strtolower($k->city) == $query) {
          return true;
        } elseif ($k->postal == $query) {
          return true;
        } else {
          return false;
        }
      });

    }
    return Response::json($filteredLocations);
  }
}
