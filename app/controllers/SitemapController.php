<?php 

use C2l\Repositories\ApplicationsRepository as ApplicationsRepository;

class SitemapController extends BaseController {

	protected $applicationsRepo;

    public function __construct(ApplicationsRepository $applicationsRepo)
    {
      $this->applicationsRepo = $applicationsRepo;
    }

    public function index() 
    {
        $products = Product::all();
        $product_types = ProductType::all();
        $application_types = $this->applicationsRepo->getAll();
        return View::make('sitemap')->with(compact('products'))->with(compact('product_types'))->with(compact('application_types'));
    }
}
