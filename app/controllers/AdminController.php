<?php



class AdminController extends \BaseController {

    public function index()
    {
        $users = User::all();

        return View::make('resourceCenter.admin')->with(compact('users'));;
    }

    public function update()
    {
        
        $editedUser = Input::all();
        $user = User::findOrFail($editedUser['id']);
        if(isset($editedUser['active'])){
            $user->isactive = 1;
            $user->save();
            return Redirect::back();
        }

        $user->isactive = 0;
        $user->save();
        return Redirect::back();
    }

}