<?php

use C2l\Repositories\SolutionsRepository as SolutionsRepository;

class SolutionsController extends \BaseController {

	protected $solutionsRepo;

	public function __construct(SolutionsRepository $solutionsRepo)
	{
		$this->solutionsRepo = $solutionsRepo;
	}

	/**
	 * Display a listing of the resource.
	 * GET /products
	 *
	 * @return Response
	 */
	public function index()
	{
		$productCategories = $this->solutionsRepo->getAllCategoriesWithPhotos();
		return View::make('solutions.index')->with(compact('productCategories'));
	}
}
