<?php 

use C2l\Forms\LoginFormValidator;

class ResourceCenterController extends \BaseController {

    protected $validator;

    public function __construct(LoginFormValidator $validator)
    {
        $this->validator = $validator;
    }

    public function index()
    {
        if(Auth::check())
        {
            return Redirect::to('/resource_center');
        }
        return View::make('resourceCenter.login');
    }

    public function login()
    {

        $this->validator->validate(Input::all());

        $email = Input::get('email');
        $password = Input::get('password');

        if (Auth::attempt(array('email' => $email, 'password' => $password)))
        {
            return Redirect::intended('/resource_center');
        }else{
            return Redirect::back()->withInput()->with('auth_fail_message', 'We were unable to log you in. Please double check your credentials.');
            // return "not logged in";
        }
    }

    public function logout()
    {
        Auth::logout();
        return View::make('resourceCenter.login');
    }

    public function dashboard()
    {
        return View::make('resourceCenter.dashboard');
    }

}