<?php

use C2l\Repositories\ProductsRepository as ProductsRepository;

class ProductsController extends \BaseController {


	protected $productsRepo;

	public function __construct(ProductsRepository $productsRepo)
	{
		$this->productsRepo = $productsRepo;
	}

	/**
	 * Display a listing of the resource.
	 * GET /products
	 *
	 * @return Response
	 */
	public function index($category_url)
	{
		$category = $this->productsRepo->getAllOfProductType($category_url);
    $products = $category->products;
		return View::make('products.filter_page')->with(compact('products'))->with(compact('category'));
		
	}

	
	public function show($url)
	{
		$product = $this->productsRepo->getFromUrl($url);
		$literature =  $product->literature;
		$doctypesIncluded = $this->productsRepo->checkforliteraturetypes($literature);
    $similarMappings = $product->similarProducts()->get();
    $carouselItems = $this->productsRepo->getSimilarProducts($similarMappings);
		return View::make('products.single')->with(compact('product'))->with(compact('carouselItems'))->with(compact('doctypesIncluded'));
	}

	

	public function search()
	{
		$query = Input::get('query');
        $productsFound = $this->productsRepo->getBySearch($query);
        $literatureFound = Literature::where('name', 'LIKE', '%' . $query .'%')->with('doctype')->get();
        return View::make('search.results')->with(compact('productsFound'))->with(compact('literatureFound'))->with(compact('query'));
		
	}


	public function zipFetcher($id)
	{
		
		$product = $this->productsRepo->getById($id);
		$literature =  $product->literature;
		$zippedFile = $this->productsRepo->constructZippedFiles($literature, $product->name);
		return $product->name;
		return Response::download($zippedFile);

	}
}
