<?php

use C2l\Repositories\ProductsRepository as ProductsRepository;
use C2l\Repositories\ApplicationsRepository as ApplicationsRepository;
use C2l\Repositories\MailingListRepository as MailingList;
use C2l\Forms\RegisterFormValidator;
use C2l\Mailers\RegisterMailer;
use C2l\Zip\ZipUtility;

class HomeController extends BaseController {

	protected $productsRepo;
	protected $applicationsRepo;
	protected $validator;
	protected $mailer;
	protected $mailingList;

	public function __construct(ProductsRepository $productsRepo, ApplicationsRepository $applicationsRepo, RegisterFormValidator $validator, RegisterMailer $mailer, MailingList $mailingList)
	{
		$this->productsRepo = $productsRepo;
		$this->applicationsRepo = $applicationsRepo;
		$this->validator = $validator;
		$this->mailer = $mailer;
		$this->mailingList = $mailingList;
	}


	public function showIndex()
	{
		$carouselItems = $this->applicationsRepo->getAll();
		return View::make('index')->with(compact('carouselItems'));
	}

	public function showAbout()
	{
		return View::make('about');
	}

	public function showPressreleases()
	{
		return View::make('pressreleases');
	}

	public function showSpotlights()
	{
		return View::make('spotlights');
	}

	public function showSupport()
	{
		return View::make('support');
	}

	public function showOneline()
	{
		return View::make('oneline');
	}

	public function showSpecifications()
	{
    $specifications = Specification::with('specification_documents')->get();
    // return $specifications[1]->specification_documents;
		return View::make('specifications')->with(compact('specifications'));
	}

	public function showTech()
	{
		return View::make('tech.support');
	}

	public function showWarranty()
	{
		return View::make('warranty.info');
	}


	public function register()
	{
		$newEntry = Input::all();
		$this->validator->validate($newEntry);
		// return $newEntry;
		$this->mailer->sendEmailToRegistrant($newEntry);
		$this->mailingList->addToMailingList($newEntry);
		return Redirect::back()->with('message','Thanks for registering for Douglas updates. We will be contacting you soon.');;

	}


}
