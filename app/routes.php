<?php

use C2l\Services\Zipper;

View::composer('*', function($view) {
  $view->with('headerItems', ProductType::all());
});

//remove after password protection is done
Route::get('getRepjson', ['as' => 'getjson', 'uses' => 'RepController@getRepjson']);

//route for redirect
Route::get('spaceplayer', function() {
  return Redirect::to('http://unvlt.com/products/space-player/');
});

//remove outer 'protected' group after password protection is done
Route::get('/', ['as' => 'home', 'uses' => 'HomeController@showIndex']);
Route::get('about', ['as' => 'about', 'uses' => 'HomeController@showAbout']);

Route::get('pressreleases', ['as' => 'pressreleases', 'uses' => 'HomeController@showPressreleases']);

Route::get('spotlights', ['as' => 'spotlights', 'uses' => 'HomeController@showSpotlights']);


Route::get('category/{category_url}', ['as' => 'filterPage', 'uses' => 'ProductsController@index']);

Route::group(['prefix' => 'products'], function() {
  Route::get('space-player',function() { // send to ult's spaceplayer page
    return Redirect::to('http://unvlt.com/products/space-player/');
  });

  Route::get('{product}', ['as' => 'single_product', 'uses' => 'ProductsController@show']);
});

Route::get('support', ['as' => 'support', 'uses' => 'HomeController@showSupport']);

Route::get('applications', ['as' => 'applications', 'uses' => 'ApplicationsController@index']);
Route::get('/applications/{application}', ['as' => 'single_application', 'uses' => 'ApplicationsController@show']);

Route::get('solutions', ['as' => 'solutions', 'uses' => 'SolutionsController@index']);

Route::get('search', ['as' => 'search', 'uses' => 'ProductsController@search']);

Route::get('oneline', ['as' => 'oneline', 'uses' => 'HomeController@showOneline']);

Route::get('specifications', ['as' => 'specifications', 'uses' => 'HomeController@showSpecifications']);

Route::get('sitemap', ['as' => 'sitemap', 'uses' => 'SitemapController@index']);

Route::get('contact', ['as' => 'contact', 'uses' => 'ContactController@show']);
Route::post('contact', ['before' => 'csrf','as' => 'contact', 'uses' => 'ContactController@send']);

Route::post('footersend', ['before' => 'csrf','as' => 'footerSend', 'uses' => 'ContactController@footerSend']);

Route::get('test', ['as' => 'test', 'uses' => 'ProductsController@test']);

Route::get('tech', ['as' => 'tech_support', 'uses' => 'HomeController@showTech']);
Route::post('tech', ['before' => 'csrf','as' => 'tech_support', 'uses' => 'ContactController@techSend']);

Route::get('warranty', ['as' => 'warranty_info', 'uses' => 'WarrantyController@index']);

Route::get('locator', ['as' => 'rep_locator', 'uses' => 'RepController@index']);
Route::post('locator', ['as' => 'rep_locator', 'uses' => 'RepController@autoloadLocation']);

Route::post('search', ['as' => 'search_results', 'uses' => 'ProductsController@search']);

Route::get('warranty', ['as' => 'warranty', 'uses' => 'HomeController@showWarranty']);


Route::group(['prefix' => 'resource_center', 'before' => 'auth'], function() {
  Route::get('/', ['as' => 'resource_dashboard', 'uses' => 'ResourceCenterController@dashboard']);
});


Route::get('login', ['as' => 'login', 'uses' => 'ResourceCenterController@index']);
Route::post('login', ['as' => 'login', 'uses' => 'ResourceCenterController@login']);
Route::get('logout', ['as' => 'logout', 'uses' => 'ResourceCenterController@logout']);

Route::post('register', ['as' => 'register', 'uses' => 'HomeController@register']);

Route::get('admin', ['as' => 'admin', 'before' => 'auth_admin', 'uses' => 'AdminController@index']);
Route::post('admin', ['as' => 'admin', 'uses' => 'AdminController@update']);

Route::get('zip/{id}', ['as' => 'zip', 'uses' => 'ProductsController@zipFetcher']);
