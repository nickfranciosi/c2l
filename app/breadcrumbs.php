<?php

Breadcrumbs::register('home', function($breadcrumbs) {
    $breadcrumbs->push('Home', route('home'));
});

Breadcrumbs::register('search', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Search', '/search');
});

Breadcrumbs::register('filterPage', function($breadcrumbs, $category) {
    $breadcrumbs->parent('solutions');
    $breadcrumbs->push($category->name);
});

Breadcrumbs::register('solutions', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Solutions', route('solutions'));
});

Breadcrumbs::register('single_product', function($breadcrumbs, $product) {
    $breadcrumbs->parent('solutions');

    if ($product->productType['attributes']['has_subcategory']) {
        $breadcrumbs->push($product->productType->name, route('filterPage', $product->productType->url));
        $breadcrumbs->push($product->name);
    } else {
        $breadcrumbs->push($product->productType->name);
    }
});

Breadcrumbs::register('applications', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Applications', route('applications'));
});

Breadcrumbs::register('single_application', function($breadcrumbs, $application) {
    $breadcrumbs->parent('applications');
    $breadcrumbs->push($application->name);
});
