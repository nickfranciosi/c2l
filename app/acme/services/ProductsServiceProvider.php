<?php namespace C2l\Services;


use Illuminate\Support\ServiceProvider;

class ProductsServiceProvider extends ServiceProvider {

    /**
     * Register the binding
     *
     * @return void
     */
    public function register()
    {
        $app = $this->app;

        $app->bind('C2l\Interfaces\ProductsRepositoryInterface', 'C2l\Repositories\ProductsRepository');
    }

}
