<?php namespace C2l\Services;



class Zipper {

    private $zip;

    public function __construct()
    {
        $this->zip = new \ZipArchive();
    }

    public function zipFiles(array $files, $destination)
    {
        if($this->zip->open(public_path() . '/resources/zips/'. $destination .'.zip' ,\ZipArchive::CREATE | \ZipArchive::OVERWRITE))
        {
            
            foreach ($files as $file) {
                if(file_exists($file)){
                    $this->zip->addFile($file, basename($file));
                }

            }
            
            $this->zip->close();
            return public_path() . '/resources/zips/'. $destination .'.zip';
        }

    }

   
}
