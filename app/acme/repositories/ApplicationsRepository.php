<?php namespace C2l\Repositories;


class ApplicationsRepository implements \C2l\Interfaces\ApplicationsRepositoryInterface
{
    protected $applicationModel;
    protected $typeModel;

    public function __construct(\Application $applicationModel, \ApplicationType $typeModel)
    {
       $this->applicationModel = $applicationModel;
       $this->typeModel = $typeModel;
    }


    public function getAll()
    {
        return $types = $this->typeModel->with('applications')->get();
    }

    public function getAllOfApplicationType($applicationType)
    {
        return $this->typeModel->with('applications')->where('name', $applicationType)->first();
    }

    public function getAllWith(array $with = array())
    {
        $query = $this->make($with);
        return $query->get();
    }

    public function getFromUrl($url)
    {
      // return $this->applicationModel->where('url', $url)->first();
      return $this->typeModel->with('applications')->where('url', $url)->first();
    }

    /**
     * Make a new instance of the entity to query on
     *
     * @param array $with
     */
    public function make(array $with = array())
    {
      return $this->applicationModel->with($with);
    }


    public function getById($id, array $with = array())
    {
      $query = $this->make($with);
     
      return $query->find($id);
    }
 
}
