<?php namespace C2l\Repositories;


use C2l\Services\Zipper;

class ProductsRepository implements \C2l\Interfaces\ProductsRepositoryInterface
{
    protected $productModel;
    protected $typeModel;

    public function __construct(\Product $productModel, \ProductType $typeModel)
    {
       $this->productModel = $productModel;
       $this->typeModel = $typeModel;
    }


    public function getAll()
    {
        return $this->productModel->all();
    }

    public function getAllOfProductType($productTypeUrl)
    {
        return $this->typeModel->with('products')->where('url', $productTypeUrl)->first();
    }

    public function getAllWith(array $with = array())
    {
        $query = $this->make($with);
        return $query->get();
    }

    public function getFromUrl($url)
    {
      return $this->productModel->with('literature.doctype','productType','certifications')->where('url', $url)->first();
    }

    public function getSimilarProducts($ids)
    {
      $returnObject = [];
      foreach($ids as $id){
        $returnObject[] = $id->similar_product_id;
      }
      return $this->productModel->find($returnObject);
    }

    public function checkforliteraturetypes($literature){
      $returnObject = [];
      foreach($literature as $lit){
        $returnObject[] = $lit->doctype->name;
      }

      return array_values(array_unique($returnObject, SORT_STRING));
    }

    public function constructZippedFiles($literature, $productName){
      $returnObject = [];
      foreach($literature as $lit){
        $returnObject[] = public_path() . '/resources/' . $lit->doctype->folder . '/'. $lit->path;
      }

      $zip = new Zipper();
      $pathToZip = $zip->zipFiles($returnObject, $productName);
    
      return $pathToZip;

    }

    /**
     * Make a new instance of the entity to query on
     *
     * @param array $with
     */
    public function make(array $with = array())
    {
      return $this->productModel->with($with);
    }


    public function getById($id, array $with = array())
    {
      $query = $this->make($with);
     
      return $query->find($id);
    }

    public function getBySearch($name)
    {
      return $this->productModel->where('name', 'LIKE', '%' . $name .'%')->get();
    }
}
