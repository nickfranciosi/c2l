<?php namespace C2l\Repositories;




class MailingListRepository
{
  protected $mailingList;

  public function __construct(\MailingList $mailingList)
    {
       $this->mailingList = $mailingList;
    }


     public function addToMailingList($user){
        $newUser = new \MailingList;
        $newUser->name = $user['name'];
        $newUser->title = $user['title'];
        $newUser->email = $user['email'];
        $newUser->company = $user['company'];
        return $newUser->save();

   }




}