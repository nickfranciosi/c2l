<?php namespace C2l\Repositories;

class SolutionsRepository extends ProductsRepository
{
	public function getAllCategories()
	{
		return $this->typeModel->all();
	}

	public function getAllCategoriesWithPhotos()
	{
		return $this->typeModel
		            ->leftJoin('products_new', 'product_type_new.id', '=', 'products_new.product_type_id')
		            ->groupBy('product_type_new.id')
		            ->distinct()
		            ->get(array('products_new.hero', 'products_new.secondary', 'products_new.thumbnail', 'product_type_new.*'));
	}
}
