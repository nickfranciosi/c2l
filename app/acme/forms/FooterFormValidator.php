<?php namespace C2l\Forms; 


class FooterFormValidator extends FormValidator{

    public $rules = [

        'footerEmail' => 'required|email',
        'question'  => 'required',
        'my_name_f'   => 'honeypot',
        'my_time_f'   => 'required|honeytime:5'
        
    ];
}