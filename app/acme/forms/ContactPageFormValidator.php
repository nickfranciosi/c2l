<?php namespace C2l\Forms; 


class ContactPageFormValidator extends FormValidator{

    public $rules = [

        'name' => 'required',
        'email' => 'required|email',
        'phone' => 'required',
        'my_name'   => 'honeypot',
        'my_time'   => 'required|honeytime:5'
        
    ];
}