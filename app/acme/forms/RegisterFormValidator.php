<?php namespace C2l\Forms; 


class RegisterFormValidator extends FormValidator{

    public $rules = [

        'name' => 'required',
        'email' => 'required|email',
        'title' => 'required',
        'company' => 'required',
        'my_name'   => 'honeypot',
        'my_time'   => 'required|honeytime:5'
        
    ];
}