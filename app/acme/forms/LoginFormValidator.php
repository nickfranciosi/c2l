<?php namespace C2l\Forms; 


class LoginFormValidator extends FormValidator{

    public $rules = [

        'email' => 'required|email',
        'password' => 'required'
        
    ];
}