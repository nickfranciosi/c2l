<?php namespace C2l\Interfaces;


interface ProductsRepositoryInterface{

    /**
     * returns all products
     */
    public function getAll();


       /**
     * returns all products
     */
    public function getAllWith();


    /**
     * returns instance of single product
     * @param integer
     * @return array of product info
     */
    public function getById($id, array $with);

}