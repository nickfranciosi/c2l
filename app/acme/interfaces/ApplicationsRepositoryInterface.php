<?php namespace C2l\Interfaces;


interface ApplicationsRepositoryInterface{

    /**
     * returns all application types
     */
    public function getAll();


       /**
     * returns all applications
     */
    public function getAllWith();


    /**
     * returns instance of single product
     * @param integer
     * @return array of product info
     */
    public function getById($id, array $with);

}
