<?php namespace C2l\Mailers;

class RegisterMailer extends Mailer{

    public function sendEmailToRegistrant(array $input)
    {
        $subject = "Thanks for Signing up for Douglas Updates";
        $view ="emails.register";
        $data = ['name' => $input['name']];
        $to = $input['email'];

        $this->sendTo($to, $subject, $view, $data);
    }

   
}
