<?php namespace C2l\Mailers;

class TechSupportMailer extends Mailer{

    public function sendEmailToAdmin($from, $comment, $name, $phone)
    {
        $subject = "Douglas customer : $from has a question/comment";
        $view ="emails.techsupport";
        $data = ['comment' => $comment, 'from' => $from, 'name' => $name, 'phone' => $phone];
        $to = 'customerservice@douglaslightingcontrols.com';

        $this->sendTo($to, $subject, $view, $data);
    }

   
}
