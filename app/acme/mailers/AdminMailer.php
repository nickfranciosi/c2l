<?php namespace C2l\Mailers;

class AdminMailer extends Mailer{

    public function sendEmailToAdmin($from, $comment, $phone = null)
    {
        $subject = "Douglas customer : $from has a question/comment";
        $view ="emails.contact";
        $data = ['comment' => $comment, 'from' => $from, 'phone' => $phone];
        $to = 'customerservice@douglaslightingcontrols.com';

        $this->sendTo($to, $subject, $view, $data);
    }

   
}
