<?php namespace C2l\Mailers;

class FollowUpMailer extends Mailer{

    protected $flashMessage = "Thanks for contacting us! We wll get back with you shortly.";

    public function followUpTo($to)
    {
        $subject = "Thanks for contacting Douglas";
        $view ="emails.follow";
        $data = [];
        
        $this->sendTo($to, $subject, $view, $data);
    }

    public function getFlashMessage()
    {
        return $this->flashMessage;
    }
}
