### Done
* Added some missing product images
* Resized secondary images to keep from being so blurry
* Added product description that was sent in spreadsheet; more probably needed on some tools/integration devices
* Removed Space Player from site
* Add footer copy
* Add image to Legacy solutions category
* Add Applications page copy
* Small copy changes on Support page
* Add logic for certifications without images

### Todo
* Add cutsheets for some products (esp. tools/integration devices)
* PDF detail grid

### Needed from Audey
* Would like feedback on new homepage slider
* Some copy for Legacy and Full Two Way category pages
* Category image for Full Two Way category page
* Hero image for About page
* Spreadsheet: N23 ?
