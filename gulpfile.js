var gulp = require('gulp')
  , source = require('vinyl-source-stream')
  , browserify = require('browserify')
  , gutil = require('gulp-util')
  , rename = require('gulp-rename')
  , notify = require('gulp-notify')
  , sass = require('gulp-sass')
  , autoprefix = require('gulp-autoprefixer')
  , minifyCss = require('gulp-minify-css')

// src and target directories
var sassDir = 'app/assets/scss'
  , jsDir = 'app/assets/js'
  , targetCSSDir = 'public/css'
  , targetJSDir = 'public/js'
  , bowerDir = 'bower_components/'

// Compile Sass, then autoprefix and minify CSS
gulp.task('css', function () {
    return gulp.src(sassDir + '/**/*.scss')
        .pipe(sass({ style: 'compressed' }).on('error', sass.logError))
        .pipe(autoprefix('last 10 version'))
        .pipe(minifyCss())
        .pipe(gulp.dest(targetCSSDir))
        .pipe(notify('CSS minified'))
})

gulp.task('browserify', function () {
    var bundle = browserify("./" + jsDir + '/main.js')

    return bundle
        .transform({ global: true }, 'uglifyify')
        .bundle()
        .pipe(source('main.js'))
        .pipe(rename(targetJSDir + '/bundle.js'))
        .pipe(gulp.dest('./'))
        .pipe(notify('js browserfied'))
})

gulp.task('copy', function() {
    return gulp.src([bowerDir + "jQuery-Store-Locator-Plugin/templates/*", bowerDir + "jQuery-Store-Locator-Plugin/js/*.min.js", bowerDir + "jvectormap/jquery-jvectormap.js"])
       .pipe(gulp.dest('./public/js'))
})

gulp.task('watch', function () {
    gulp.watch(sassDir + '/**/*.scss', ['css'])
    gulp.watch(jsDir + '/*.js', ['browserify'])
})

gulp.task('default', ['css','browserify', 'watch'])

gulp.task('build', ['css','browserify'])
